import smartpy as sp

class Verifier(sp.Contract) :
    def __init__(self,
        srs1,
        srs2,
        list_poly,
        list_k,
        generator,
        n,
        public_inputs,
        list_cm,
        list_eval,
        transcript
    ):
        srs1,
        srs2, 
        qM, qL, qR, qO, qC = list_poly
        k1, k2 = list_k
        generator = generator
        n = n
        public_inputs = public_inputs
        # proofs
        cm_a, cm_b, cm_c, cm_z, cm_tlo, cm_tmi, cm_thi, cm_w, cm_wg, cm_ssigma1, cm_ssigma2, cm_ssigma3 = list_cm
        # evals
        a, b, c, ssigma1, ssigma2, r, zg = list_eval
        
        self.init(
            srs1 = srs1,
            srs2 = srs2,
            qM = qM,
            qL = qL,
            qR = qR,
            qO = qO,
            qC = qC,
            k1 = k1,
            k2 = k2,
            generator = generator,
            n = n,
            public_inputs = public_inputs,
            # proofs
            cm_a = cm_a,
            cm_b = cm_b,
            cm_c = cm_c,
            cm_z = cm_z,
            cm_tlo = cm_tlo,
            cm_tmi = cm_tmi,
            cm_thi = cm_thi,
            cm_w = cm_w,
            cm_wg = cm_wg,
            cm_ssigma1 = cm_ssigma1,
            cm_ssigma2 = cm_ssigma2,
            cm_ssigma3 = cm_ssigma3,
            # evals
            a = a,
            b = b,
            c = c,
            ssigma1 = ssigma1,
            ssigma2 = ssigma2,
            r = r,
            zg = zg,
            transcript = transcript
        )

    #@sp.global_lambda
    def hash_to_fr(self, b):
        hash_b = sp.blake2b(b)
        # TODO fix me we throw too much entropy
        # We remove the last byte to be smaller than modulo r
        hash_trimmed = sp.concat([ sp.slice(hash_b, 0, 31).open_some(), sp.bytes("0x00")]) 
        # Adding the Fr Michelson prefix to the blake2b hash result.
        # Ending with 20 because the bytes are 32 bytes long (0x20 = 32)
        fr_bytes = sp.concat([
            sp.bytes("0x050a00000020"),
            hash_trimmed
        ])
        final_bytes = sp.unpack(fr_bytes, t = sp.TBls12_381_fr).open_some()
        #sp.trace(final_bytes)
        return final_bytes

    @sp.global_lambda
    def pow_fr(params):
        """
        Returns a^e
        """
        a, e = sp.match_pair(params)
        sp.set_type(e, sp.TNat)
        local_a = sp.local("local_a", a)
        result = sp.local("result", sp.bls12_381_fr("0x01"))
        local_e = sp.local("local_e", e)
        sp.while local_e.value > 0:
            div_res = sp.ediv(local_e.value, sp.nat(2)).open_some()
            s = sp.fst(div_res)
            r = sp.snd(div_res)
            sp.if r == 1:
                result.value = result.value * local_a.value
            local_a.value = sp.mul(local_a.value, local_a.value)
            local_e.value = s
        sp.result(result.value)

    #@sp.global_lambda
    def inv_fr(self, a):
        # Inverse by p - 2
        res = self.pow_fr((a, 52435875175126190479447740508185965837690552500527637822603658699938581184511)
        )
        return res

    #@sp.global_lambda
    def div_fr(self, b, a):
        return sp.mul(b, self.inv_fr(a))
        

    @sp.sub_entry_point
    def compute_challenges(self, t):
        # FIXME: we must split this function
        # pack adds a byte.
        transcript = sp.local("transcript", self.data.transcript)
        
        sp.verify_equal(transcript.value, sp.bytes("0x00"))
        transcript.value = sp.concat([
            transcript.value,
            sp.pack(self.data.cm_a),
            sp.pack(self.data.cm_b),
            sp.pack(self.data.cm_c)
        ])
        beta = sp.local("beta", self.hash_to_fr(transcript.value))
        # ??? IMPROVEME ?????
        gamma = sp.local("gamma", self.hash_to_fr(sp.blake2b(transcript.value)))
        
        # alpha
        transcript.value = sp.concat([
            transcript.value,
            sp.pack(self.data.cm_z)
        ])
        alpha = sp.local("alpha", self.hash_to_fr(transcript.value))
        
        transcript.value = sp.concat([
            transcript.value,
            sp.pack(self.data.cm_tlo),
            sp.pack(self.data.cm_tmi),
            sp.pack(self.data.cm_thi),
        ])
        z = sp.local("z", self.hash_to_fr(transcript.value))
        
        transcript.value = sp.concat([
            transcript.value,
            sp.pack(self.data.a),
            sp.pack(self.data.b),
            sp.pack(self.data.c),
            sp.pack(self.data.ssigma1),
            sp.pack(self.data.ssigma2),
            sp.pack(self.data.zg),
            sp.pack(self.data.r)
        ])
        v = sp.local("v", self.hash_to_fr(transcript.value))

        transcript.value = sp.concat([
            transcript.value,
            sp.pack(self.data.cm_w),
            sp.pack(self.data.cm_wg),
        ])
        u = sp.local("u", self.hash_to_fr(transcript.value))
        
        sp.result(
            sp.pair
                (sp.pair(
                    sp.pair(alpha.value, beta.value), 
                    sp.pair(gamma.value, z.value)
                ), sp.pair(v.value, u.value)
            )
        )


    @sp.entry_point
    def verify(self):
        one = sp.bls12_381_fr("0x01")

        # 4
        # FIXME the parameter is useless
        challenges = self.compute_challenges(sp.bls12_381_fr("0x00"))
        alphabetagammaz, vu = sp.match_pair(challenges)
        alphabeta, gammaz = sp.match_pair(alphabetagammaz)
        alpha, beta = sp.match_pair(alphabeta)
        gamma, z = sp.match_pair(gammaz)
        v, u = sp.match_pair(vu)
        
        
        a_beta_ssigma = self.data.a + \
            sp.mul(
                beta,
                self.data.ssigma1
            ) + gamma
        b_beta_ssigma = self.data.b + \
            sp.mul(
                beta,
                self.data.ssigma2
            ) + gamma

        z_n =  self.pow_fr((z, self.data.n))
        inv_z = self.inv_fr(z)

        # 5
        eval_Zh = z_n + ( - one)
        
        # 6
        inv_generator = self.inv_fr(self.data.generator)
        test_div = self.div_fr(eval_Zh, inv_generator)
        eval_L1 = self.div_fr(eval_Zh, sp.mul(self.data.n, (sp.mul(inv_generator, z) + (-one))))
        
        # 7
        sum_pi = sp.local("sum_pi", sp.bls12_381_fr("0x00"))
        inv_generator_i = sp.local("inv_generator_i", one)
        sp.for w in self.data.public_inputs:
            denom = inv_generator_i.value + ( - inv_z)
            sum_pi.value += self.div_fr(w, denom)
            inv_generator_i.value = sp.mul(inv_generator, inv_generator_i.value)
        eval_pi = -(sp.mul(self.div_fr(eval_Zh, sp.mul(self.data.n, z)), sum_pi.value))
        
        # 8
        alpha2 = sp.mul(alpha, alpha)
        intermediary_1 = sp.mul(
                    sp.mul(
                    a_beta_ssigma ,
                        b_beta_ssigma
                    ), sp.mul(self.data.c + gamma, self.data.zg)) 
        num = self.data.r + \
            eval_pi + (-( \
            sp.mul( intermediary_1
                , alpha) + sp.mul(eval_L1, alpha2)))

        eval_t = self.div_fr(num, eval_Zh)

        # 9
        av = self.data.a * v
        alphav = alpha * v
        first_term = sp.mul( self.data.qM, sp.mul(av, self.data.b)) + sp.mul( self.data.qL, av) + sp.mul( self.data.qR, sp.mul(self.data.b, v)) + sp.mul( self.data.qO, sp.mul(self.data.c, v)) + sp.mul( self.data.qC, v)
        second_term = sp.mul(
            self.data.cm_z, sp.mul(self.data.a + sp.mul(beta, z) + gamma,
                                   sp.mul(self.data.b + sp.mul(beta, sp.mul(self.data.k1, z)) + gamma,
                                          sp.mul(self.data.c + sp.mul(beta, sp.mul(self.data.k2, z)) + gamma,
                                                 alphav))) \
            + sp.mul(eval_L1, sp.mul(alpha2, v)) + u)
        third_term = (- sp.mul(self.data.cm_ssigma3,sp.mul(
                  b_beta_ssigma,
                      sp.mul(alphav, sp.mul(beta, sp.mul(self.data.zg, a_beta_ssigma))))))
        d = first_term   \
            +  second_term\
            + third_term

        # 10
        v2 = sp.mul(v, v)
        v3 = sp.mul(v, v2)
        v4 = sp.mul(v, v3)
        v5 = sp.mul(v, v4)
        v6 = sp.mul(v, v5)

        f = self.data.cm_tlo + \
            sp.mul( self.data.cm_tmi, z_n) + \
            sp.mul(self.data.cm_thi, sp.mul(z_n,z_n )) + \
            d + \
            sp.mul(self.data.cm_a, v2) + \
            sp.mul(self.data.cm_b, v3) + \
            sp.mul(self.data.cm_c, v4) + \
            sp.mul(self.data.cm_ssigma1, v5) + \
            sp.mul(self.data.cm_ssigma2, v6)

        # 11
        e = eval_t + \
            sp.mul(v, self.data.r) + \
            sp.mul(v2, self.data.a) + \
            sp.mul(v3, self.data.b) + \
            sp.mul(v4, self.data.c) + \
            sp.mul(v5, self.data.ssigma1) + \
            sp.mul(v6, self.data.ssigma2) + \
            sp.mul(u, self.data.zg)

        # 12
        with sp.match_cons(self.data.srs1) as x1:
            pairing_arg_1 = -(self.data.cm_w + sp.mul(self.data.cm_wg, u))
            intermediary = sp.mul(self.data.cm_wg, sp.mul(u,sp.mul(self.data.generator, z)))
            pairing_arg_2 = sp.mul(self.data.cm_w, z) + intermediary + f + (- sp.mul(x1.head, e))
            pair_list = [
                (pairing_arg_1, sp.snd(self.data.srs2)),
                (pairing_arg_2, sp.fst(self.data.srs2))
            ]
            sp.verify(sp.len(x1.tail) == 13)
            sp.verify(sp.pairing_check(pair_list))
        sp.else:
            sp.verify(False)
           



@sp.add_test(name = "Test verifier")
def test():
        
    n = 8
    generator = sp.bls12_381_fr("0x87c08b68cb02d78dfea4ea78408232a0225bca983ab233a76016a3255d40963f")
    srs1 = [sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"), sp.bls12_381_g1("0x0572cbea904d67468808c8eb50a9450c9721db309128012543902d0ac358a62ae28f75bb8f1c7c42c39a8c5529bf0f4e166a9d8cabc673a322fda673779d8e3822ba3ecb8670e461f73bb9021d5fd76a4c56d9d4cd16bd1bba86881979749d28"), sp.bls12_381_g1("0x0c9b60d5afcbd5663a8a44b7c5a02f19e9a77ab0a35bd65809bb5c67ec582c897feb04decc694b13e08587f3ff9b5b60143be6d078c2b79a7d4f1d1b21486a030ec93f56aa54e1de880db5a66dd833a652a95bee27c824084006cb5644cbd43f"), sp.bls12_381_g1("0x085ae765588126f5e860d019c0e26235f567a9c0c0b2d8ff30f3e8d436b1082596e5e7462d20f5be3764fd473e57f9cf19e7dfab8a794b6abb9f84e57739de172a63415273f460d1607fa6a74f0acd97d9671b801dd1fd4f18232dd1259359a1"), sp.bls12_381_g1("0x073eb991aa22cdb794da6fcde55a427f0a4df5a4a70de23a988b5e5fc8c4d844f66d990273267a54dd21579b7ba6a0861825bacd18f695351f843521ebeada20352c3c3965626f98bc4c68e6ff7c4eed38b48f328204bbb9cd461511d24ebfb3"), sp.bls12_381_g1("0x072841987e4f219d54f2b6a9eac5fe6e78704644753c3579e776a3691bc123743f8c63770ed0f72a71e9e964dbf58f430d6a2b27f2dc2f80885d7b3679d718c03978c664236ceae4bdb8a1a1f61d5c1433b538ddb43fef229cf4b0b03fad72e7"), sp.bls12_381_g1("0x111bb496153aa457e3302ea8e74427962c6eb57e97096f65cafe45a238f739b86d4b790debd5c7359f18f3642d7d774c05120bf6abdb2aa0b467ba483df88a0745bffeefd606b6126813bc54a2acd1a57612c32b3e7329a1e163f782a75a291d"), sp.bls12_381_g1("0x0b737f47d5b2794819b5dc01236895e684f1406f8b9f0d9aa06b5fb36dba6c185efec755b77d9424d09b8484681275590048ba1134d9ffeb5eb19f7006b3c7f64704674de5c9f2cbb52ad0278ff9d46e495b89dbb3189cd567d6fd160c676282"), sp.bls12_381_g1("0x0025cdadf2afc5906b2602574a799f4089d90f36d73f94c1cf317cfc1a207c57f232bca6057924dd34cff5bde87f193000d39b79486e185b221147b7d85eaf2238d756842bd84f57c6f37542a047a44d0c06b5de39f22721d14e54addecaea80"), sp.bls12_381_g1("0x09843dc6497bb8028cdbaea5c085b8e8c28bb5182ce691fa9ab5303b6792b2f5af0fc8fe52abb92c07ad5f4cf586c4001706760b494640dd33fa5a9a078727a04c402f1714071bf2ada62eb4131718fc34864aeb9c27eef1475de5dd810d57bf"), sp.bls12_381_g1("0x0e0031515253249cc68e8ff6381c85231781f9ba5c251f8d663d634b461bc6a35ecccd2938704d36cfd7eb7bcf843b8218a3f54484c881f70b94919888ab5546cfac6e9ce1d3959efac2b2f53a5da350016c10b1c3905cbe7a2da5aef1fe77b9"), sp.bls12_381_g1("0x0dec245e630cdf805c86f14258ca0e24c8016ec11defb065d70ad9a8c6dee38b26b2499a5cf1931ec65a2640ecd8424b0dd6493b2adf30a6b449da9da51b3d00164d8329dcd3033791604b44173656fe41dbdc0e3bd50cdc00fa379d0bbe1420"), sp.bls12_381_g1("0x156f2f510d8e6acf438600f0bbbf8b6c96e31183abadab8adb864d76dfb209bd3cedad07d188bc53ebcaef76eeb368b1041430e5a4fb1337d1e4ed56d68c874aedeabde84302be3d1d9e67f5cdefe0cfdd299ed3aaadae7c0d0d534ca8108a7f"), sp.bls12_381_g1("0x1664301dfcb940539b336e6b21c156cda3c8a63847727d37491a892db7f4f2bb9fc9730163254e8bd028b730c8b39309078660ad0dc67d15bae92e76cdff8b8262cf78c7180a7f1817faab792ee7a8b309150ddf84d53b9cc9301aa8b5ee76bd")]
    srs2 = (sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801"), sp.bls12_381_g2("0x0a4edef9c1ed7f729f520e47730a124fd70662a904ba1074728114d1031e1572c6c886f6b57ec72a6178288c47c335771638533957d540a9d2370f17cc7ed5863bc0b995b8825e0ee1ea1e1e4d00dbae81f14b0bf3611b78c952aacab827a0530f6d4552fa65dd2638b361543f887136a43253d9c66c411697003f7a13c308f5422e1aa0a59c8967acdefd8b6e36ccf30468fb440d82b0630aeb8dca2b5256789a66da69bf91009cbfe6bd221e47aa8ae88dece9764bf3bd999d95d71e4c9899"))

    qM = sp.bls12_381_g1("0x16fa8b1b95afca9ba02ab4561bd62e529cb3d4743cb36b60bb63f49078e7e6be50d2aa746fd598b9233faeb7501bc7bf0814b2bbfe956734c4103a7e8b9f4af2e67635738966ad33deb58b727ec00ba732bf7df0931db061da98154c349cadcf")
    qL = sp.bls12_381_g1("0x16d10debe1c924e8915e3c6b1033ba46f3ec13a8ab295a3faef14b613bf7448b0b4a7246a92aaf1e8681a490c8108b1d1731d698623bebff982022aabf0b98affcc4191d4cb0d34bec07ebca4240c982f7b9af81d9f37a83530e8434e64af67f")
    qR = sp.bls12_381_g1("0x0f90485cb8022a2edc80e9d6dd3df341bbf02b41fae8f2e6039d6b5cf29c42866d76c5cd39cc00c7b14e25b8991ddd7719f5278937f7925c4d8200a1f742bb8ae6334f73578a22034cf0a89e30119961c55525a5576ef922cfb694082524bf3f")
    qO = sp.bls12_381_g1("0x18a2b452524edb5fec82091a8db935050a2357858645612fc8bf3cd2f86047c01e6ebe2bdc62185fb0473d7fddec51120163b7538c15bd099b58f0eed70c88e9ec23a76dc8b8078ee996f44f35742f59b021fc06c0485a63e78d55806e76b0d3")
    qC = sp.bls12_381_g1("0x400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")

    k1 = sp.bls12_381_fr("0x0500000000000000000000000000000000000000000000000000000000000000")
    k2 = sp.bls12_381_fr("0x0700000000000000000000000000000000000000000000000000000000000000")

    cm_ssigma1 = sp.bls12_381_g1("0x04f5ae9541a9f7960cfbe97240209fdeac4606c453269dd0bfb8c7b712d902dec19b909493a118071c62794f76a9734110a1181107c8efc33168caaf8fb18fe77e9cd57d511eb0c0057e2272a8142a227004fe7b0d6baec2cc814f9cd2ce6260")
    cm_ssigma2 = sp.bls12_381_g1("0x02fb8bf4d843f8e622c326afd1a03f807e6967ef50133183d2461c100e37b03ea7f279e4b3a77439805a3be8c0afdb900141c5874b58abcbc76d7fd381ac6e67a0d4c0d0c7807d27aa19b89c480ef3af605475a53f8b4d760c207a7a25184522")
    cm_ssigma3 = sp.bls12_381_g1("0x0ae4e8204371d245676ab9a101648d142e5d9b8f93038eaa01f4c0d58edfe7cb9992896cb525b6fdab082702af37b2f919db5ea6a194160abd38bfed71507acc6905621a0f3b47005374faa5863739778b954829901bee853bbc044a71220544")
    cm_a = sp.bls12_381_g1("0x059fdee94b64a4d1e7436bd55ba3597bec09273f96f694b5d17e6960fbc1b6ba2e7f84b2d91a027f1dec853d699ead620576ce7976d8af08be48f3d13d260e86227fdd7f389c62b2a5154787277a212acc3003a35659ab86d5a74efdfa001de9")
    cm_b = sp.bls12_381_g1("0x05e40c8a40b4f3e3a95bba7f12c5b2e2c42eddf7bf0c67bb4f225ba158f5eb21c68929f8820d6c7a8c2d561ce75033de1620f4568e8aef14cdc36805049facdd365f913d91b5b6c9a5dee3042e9076d87f221c31766e6e0d7ab6637b74a1d5c6")
    cm_c = sp.bls12_381_g1("0x14bc0e05c5510af04974e7c50cc547097cfd99806296f7bf83f80565f315c2877d5c9a835c996c67fa7e5673f0b31a5d0f73bcf09ba81aa379d5933eece076ac876376e6b3c5be612c4893d1dba9c196863b8d0a92ef03590162ad5d2921ad1f")
    cm_z_poly = sp.bls12_381_g1("0x11b55eb7e331786cf04724ca8bca3d852e7309a3a570fad9dbdb6091ff16d86a0253b4d666a544eabf5bb283a7f2557611523eba22acfa312653351c4feeb242c006fb2ab5658a021193f8b9aa5bc6533a362111131b26cd0026c5b89201c7f5")
    cm_tlo = sp.bls12_381_g1("0x0193fc528310717ab406d475d29be89b2d3c50ba950fd68091ccda6c2f1856c1757de182009f4ad82e1ac6b1ddc5b6450e0296a45850133d655d7b507b0e6fd8a32e42b99b54828b368a76dda0c3922364fa5cfb8263cd6741ad3485784ce15a")
    cm_tmi = sp.bls12_381_g1("0x0465c90efa90b9359e282a97cac7fae8bfc0f8fe8c9544e55b85bf3b9f8b25391ca4e371f51a1544f791a8b592203d491102d23bc38a4ec593f6200c9eed309021347df2e8a16311882e68094b39e8324445f1d34e66bd06c5a77082e01aa37f")
    cm_thi = sp.bls12_381_g1("0x04511568bf556dcd74f9825337328185457f646261d87274f70208fcc5b20681a68a3b39943b1df4445e362f742f132a0ffa77fd807c3ef1ded0b881668a25d7e63214732e58433462df872e8c45fe1e461d12420910de22cf1c28de6a22f7fc")
    cm_w = sp.bls12_381_g1("0x0617cf6defde76534a19895035a5c7e258f3d39781c3a7c78dab3c0513b1a006edb73481afb42e0ec64deeb41b09f33a0e7e47028f2600403e8f37d4762c1b0b1ed5ee844576d8968f45919c12cb07047030f96a4be570dfbdc4cbfb24db7365")
    cm_wg = sp.bls12_381_g1("0x0ce1a21bad27ef4b49bebc183c87b7bcb5e458e3e93096a3b0609bd3dcc9ffc7bc5cbcb6fd31c2d8eb5e2ec0bea2b4c60b9dca73275e7b0ee2a70596f02a15f81904a740b11d93c55ad3e954c417f43f9ac5aeaaa807066e6f434f717b78fbf5")

    eval_a = sp.bls12_381_fr("0x7386112a59c7030a74a30216232e29aa07f501ca5c851ffc2d817a2fd8505a6a")
    eval_b = sp.bls12_381_fr("0x2a2d8aeacda761e4e266c45db357ab5b8ac32b4dd218458b0678d099e67e4c13")
    eval_c = sp.bls12_381_fr("0xf07610d5698fe15e5593b6cb5e3656253b32e7e157f21c743c675c0717a57c6e")
    eval_ssigma1 = sp.bls12_381_fr("0x671a92a4e672b42bd87ff79f2ae53eb3b403991402ea72041080b706736c362d")
    eval_ssigma2 = sp.bls12_381_fr("0x12665c6ed75e9f9f35bde243afda7f90b180d266bb69c6f1622b07d98bfccf12")
    eval_r = sp.bls12_381_fr("0xe090e7b65e3fd2014112ab5eb476807e3f35ed90298d6648a9369df3c6db021c")
    eval_zg = sp.bls12_381_fr("0xad7f977ba9d7a7f997a3a4c64b5c67df7192e140491ac579fab0b5a74b992734")

    public_inputs = [sp.bls12_381_fr("0x0100000000000000000000000000000000000000000000000000000000000000"), sp.bls12_381_fr("0x0200000000000000000000000000000000000000000000000000000000000000"), sp.bls12_381_fr("0x0300000000000000000000000000000000000000000000000000000000000000")]
    
    transcript=sp.bytes("0x00")
 
    list_poly = [qM, qL, qR, qO, qC]
    list_cm = [cm_a, cm_b, cm_c, cm_z_poly, cm_tlo, cm_tmi, cm_thi, cm_w, cm_wg, cm_ssigma1, cm_ssigma2, cm_ssigma3]
    list_eval = [eval_a,eval_b,eval_c,eval_ssigma1,eval_ssigma2,eval_r,eval_zg]
    list_k = [k1, k2]
        
    verifier_ok = Verifier(
            srs1 = srs1,
            srs2 = srs2,
            list_poly = list_poly,
            list_k = list_k,
            generator = generator,
            n = n,
            public_inputs = public_inputs,
            list_cm = list_cm,
            list_eval = list_eval,
            transcript = transcript
        )

    scenario = sp.test_scenario()
    scenario.h1("Verifier : test OK")
    scenario += verifier_ok
    scenario += verifier_ok.verify()
    
    
    # Wrong proof
    
    qM = sp.bls12_381_g1("0x16fa8b1b95afca9ba02ab4561bd62e529cb3d4743cb36b60bb63f49078e7e6be50d2aa746fd598b9233faeb7501bc7bf0814b2bbfe956734c4103a7e8b9f4af2e67635738966ad33deb58b727ec00ba732bf7df0931db061da98154c349cadcf")
    qL = sp.bls12_381_g1("0x16d10debe1c924e8915e3c6b1033ba46f3ec13a8ab295a3faef14b613bf7448b0b4a7246a92aaf1e8681a490c8108b1d1731d698623bebff982022aabf0b98affcc4191d4cb0d34bec07ebca4240c982f7b9af81d9f37a83530e8434e64af67f")
    qR = sp.bls12_381_g1("0x0f90485cb8022a2edc80e9d6dd3df341bbf02b41fae8f2e6039d6b5cf29c42866d76c5cd39cc00c7b14e25b8991ddd7719f5278937f7925c4d8200a1f742bb8ae6334f73578a22034cf0a89e30119961c55525a5576ef922cfb694082524bf3f")
    qO = sp.bls12_381_g1("0x18a2b452524edb5fec82091a8db935050a2357858645612fc8bf3cd2f86047c01e6ebe2bdc62185fb0473d7fddec51120163b7538c15bd099b58f0eed70c88e9ec23a76dc8b8078ee996f44f35742f59b021fc06c0485a63e78d55806e76b0d3")
    qC = sp.bls12_381_g1("0x400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
    
    k1 = sp.bls12_381_fr("0x0500000000000000000000000000000000000000000000000000000000000000")
    k2 = sp.bls12_381_fr("0x0700000000000000000000000000000000000000000000000000000000000000")
    
    cm_ssigma1 = sp.bls12_381_g1("0x04f5ae9541a9f7960cfbe97240209fdeac4606c453269dd0bfb8c7b712d902dec19b909493a118071c62794f76a9734110a1181107c8efc33168caaf8fb18fe77e9cd57d511eb0c0057e2272a8142a227004fe7b0d6baec2cc814f9cd2ce6260")
    cm_ssigma2 = sp.bls12_381_g1("0x02fb8bf4d843f8e622c326afd1a03f807e6967ef50133183d2461c100e37b03ea7f279e4b3a77439805a3be8c0afdb900141c5874b58abcbc76d7fd381ac6e67a0d4c0d0c7807d27aa19b89c480ef3af605475a53f8b4d760c207a7a25184522")
    cm_ssigma3 = sp.bls12_381_g1("0x0ae4e8204371d245676ab9a101648d142e5d9b8f93038eaa01f4c0d58edfe7cb9992896cb525b6fdab082702af37b2f919db5ea6a194160abd38bfed71507acc6905621a0f3b47005374faa5863739778b954829901bee853bbc044a71220544")
    
    cm_a = sp.bls12_381_g1("0x11c769493ba2dda97accafdf21ac6a06159d7ed9bafd2008e8b7d738cc09beb058823391d94066316e9708e7a9302ae9088109c34d6abb37bd07c9cd93de6d533bc79bdda40de91223b7d37692a32cd185b5ec3f7effe8c79e59ec42ffe85c2e")
    cm_b = sp.bls12_381_g1("0x0afcacbff7785a02fd377671290ca214243148078df6b80032cf50a0dc1047c1160ccca5c067114614f5df0311e2cf3809df6ce2efa025fe9ef6ef2717e93aeec123c748436492c856fd320a66152a7a0e6005ebc07aac05b555e6902fe41098")
    cm_c = sp.bls12_381_g1("0x08f86d644f21bcf44684ab9eb3ca91d58899403a36c2ade6f8c627941255f9938e2e2966f5a1da555778a1bddf84f8cb0abbf0a1fd9588d5f38595fe13c3b50c7c678ca03510a847d1658ff846c3f03de9960ccc11624d691883b156d11c119a")
    cm_z_poly = sp.bls12_381_g1("0x0caef3c98085197e27d0923116909851d3f445e09075fe898ac858d6c479a19f89d0fb212ff1106489302df89e70147f015a172854b26ac970f06fdf7ee2a43d0908cfb0ec4fcfb9c8ada37e92f5231234f5359d3109607aaddc8ca4c42770f1")
    cm_tlo = sp.bls12_381_g1("0x1484182a9e6a56338622857383b58679b295099e6055f2a2324b145687f066f992156f32c48707f40d24ee0aaf104fef130b9286241146f631453b46b462e83af04c7fdd6e4c0ed538fe015375560879f442ed350c09857b6684f378bcd76b2c")
    cm_tmi = sp.bls12_381_g1("0x0e5fb68da68ea86ebd57a2e627581a8770510a208443553599e61c8a090509a015ed37fe42197305b17b2a865b30c90f1694bbceea83ad6b7803a825c07b2c021d4b0e04591e57f1af9c9033d4b0eb4e4cddac895c90a77593413e6602e0fec5")
    cm_thi = sp.bls12_381_g1("0x12edde2d3c20bdbeffb2a77e455d4653f600a167ec0317ae29803ea16036f6029a790bbf74beabbdb0bf1c8dd3296d3b190d6b29e6f31a175dd6e8335d352ac5f731e84b27f8047c694b59de184148a246069ffc9685b4fe904b65e824bc9486")
    cm_w = sp.bls12_381_g1("0x118eab8ac712700c2038a0e3aa704f0c414c3c1c85c9a02f5b6bf97b331b70eca6d9996458dd71e399c91c86a8489d7100a43ec6ce3f4b6876a50cf68f5eacb8ff59b293944081afe8783dc9a17078f0b39714d2c80cf14f8588d3aa89b84df4")
    cm_wg = sp.bls12_381_g1("0x15efc24381d1319e5c67184afa1deea76d78d127b8aa0b8e07b8d67c77d8eed0267352e05bfc6748a70db73272a10cb614a2b0f564d84115bb4f804c9b520bca05309f4f7928dd6ae793986ba32436b369629b2a95a10ef08f81b10db9c85a60")
    
    eval_a = sp.bls12_381_fr("0xf88ad6229adddb2730871115bdf13c70414eb4a18df6b1933a223d51c2682f42")
    eval_b = sp.bls12_381_fr("0x2df54c3de0d9ce4bef38f919a4e90d43750a9c0025151360cf63d4ca2e135a5b")
    eval_c = sp.bls12_381_fr("0x00816d57df222419ac24ea54286471362b9519077b1d9b66de3026f5c2d31019")
    eval_ssigma1 = sp.bls12_381_fr("0x15eb5411e0c23d11d0c6f405232a169c730753a2e018a1215e7fc4221d990068")
    eval_ssigma2 = sp.bls12_381_fr("0xb9a602fd88b9e1b5653ac4af06d4678245b42cde853db8599d94bbe3c5113573")
    eval_r = sp.bls12_381_fr("0x736958624aff116349a2d4e91b3d0182fb5791010c6c81f8e1a27a9757cdaa13")
    eval_zg = sp.bls12_381_fr("0x1782ea86d096f8f8566350e918529b0e814c612fe2f09174750c078d725e412c")
    
    list_poly = [qM, qL, qR, qO, qC]
    list_cm = [cm_a, cm_b, cm_c, cm_z_poly, cm_tlo, cm_tmi, cm_thi, cm_w, cm_wg, cm_ssigma1, cm_ssigma2, cm_ssigma3]
    list_eval = [eval_a,eval_b,eval_c,eval_ssigma1,eval_ssigma2,eval_r,eval_zg]
    list_k = [k1, k2]
    
    verifier_nok = Verifier(
            srs1 = srs1,
            srs2 = srs2,
            list_poly = list_poly,
            list_k = list_k,
            generator = generator,
            n = n,
            public_inputs = public_inputs,
            list_cm = list_cm,
            list_eval = list_eval,
            transcript = transcript
        )
        
    
    scenario.h1("Verifier : test NOK")
    scenario += verifier_nok
    scenario += verifier_nok.verify().run(valid = False)
    

sp.add_compilation_target("Verifier", Verifier)
