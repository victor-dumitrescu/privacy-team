import smartpy as sp

class FrCalculator(sp.Contract):
    def __init__(self):
        self.init(result=sp.bls12_381_fr("0x01"))

    @sp.global_lambda
    def pow_fr(params):
        """
        Returns a^e
        """
        a, e = sp.match_pair(params)
        local_a = sp.local("local_a", sp.to_int(a))
        result = sp.local("result", sp.bls12_381_fr("0x01"))
        local_e = sp.local("local_e", e)
        sp.while local_e.value > 0:
            div_res = sp.ediv(local_e.value, 2).open_some()
            s = sp.fst(div_res)
            r = sp.snd(div_res)
            sp.if r == 1:
                result.value = sp.mul(result.value, local_a.value)
            local_a.value = sp.mul(local_a.value, local_a.value)
            local_e.value = s
        sp.result(result.value)

    @sp.entry_point
    def pow(self, a, e):
        sp.set_type(a, sp.TBls12_381_fr)
        sp.set_type(e, sp.TInt)
        self.data.result = self.pow_fr((a, e))

    # @sp.global_lambda
    # def inv_fr(a):
    #     # Inverse by p - 2
    #     res = self.pow_fr(
    #         a,
    #         52435875175126190479447740508185965837690552500527637822603658699938581184511
    #     )
    #     sp.result(res)

    # @sp.global_lambda
    # def div_fr(a, b):
    #     sp.result(sp.mul(a, self.inv_fr(b)))

    # @sp.entry_point
    # def div(self, params):
    #     self.data.result = self.div_fr(a=sp.fst(params), b=sp.snd(params))


@sp.add_test(name="Random values - positive")
def test_random_values():
    scenario = sp.test_scenario()
    c1 = FrCalculator()
    scenario += c1
    a1 = sp.bls12_381_fr("0x977f1a7ec8d5154621afa68c2882f391c975b6768947579d1ac0ba3744e20c67")
    e1 = sp.int(29834593285034)
    res1 = sp.bls12_381_fr("0x67c1d7164caf2a21fbcc59aa1fb5bb295de62d43aa8ec0b9314db0738fc3dd5e")
    scenario += c1.pow(a=a1, e=e1)
    scenario.verify(sp.pack(c1.data.result) == sp.pack(res1))

@sp.add_test(name="Random values - negative")
def test_random_values_unequal():
    scenario = sp.test_scenario()
    c1 = FrCalculator()
    scenario += c1
    a1 = sp.bls12_381_fr("0x977f1a7ec8d5154621afa68c2882f391c975b6768947579d1ac0ba3744e20c67")
    e1 = sp.int(29834593285034-1)
    res1 = sp.bls12_381_fr("0x67c1d7164caf2a21fbcc59aa1fb5bb295de62d43aa8ec0b9314db0738fc3dd5e")
    scenario += c1.pow(a=a1, e=e1)
    scenario.verify(sp.pack(c1.data.result) != sp.pack(res1))

sp.add_compilation_target("calc_fr", FrCalculator())