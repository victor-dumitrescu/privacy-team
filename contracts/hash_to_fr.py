import smartpy as sp

class HashToFrContract(sp.Contract):
    def __init__(self):
        self.init(result=sp.bls12_381_fr("0x01"),a=sp.bytes("0x00"),mynat=sp.int(1))
    @sp.global_lambda
    def hash_to_fr(b):
        hash_b = sp.blake2b(b)
        # Adding the Fr Michelson prefix to the blake2b hash result.
        # Ending with 20 because the bytes are 32 bytes long (0x20 = 32)
        hash_trimmed = sp.concat([ sp.slice(hash_b, 0, 31).open_some(), sp.bytes("0x00")]) 
        fr_bytes = sp.concat([
            sp.bytes("0x050a00000020"),
            hash_trimmed
        ])
        res_fr = sp.unpack(fr_bytes, t = sp.TBls12_381_fr).open_some()
        sp.result(res_fr)

    @sp.entry_point
    def hash(self, b):
        sp.set_type(b, sp.TBytes)
        self.data.result = self.hash_to_fr(b)

    @sp.entry_point
    def unpack(self, b):
        sp.set_type(b, sp.TBytes)
        b2 = sp.unpack(b, sp.TInt).open_some()
        self.data.mynat=b2
        
    @sp.entry_point
    def pack(self, nat):
        sp.set_type(nat,sp.TInt)
        packed = sp.pack(nat)
        self.data.a=packed

        
sp.add_compilation_target("hash-to-fr", HashToFrContract())
