open Plonk_generate_proof.Generate_proof

let print_list oc print l =
  Printf.fprintf oc "[";
  let rec aux li =
    match li with
    | [] -> Printf.fprintf oc "]"
    | [ x ] ->
        print x;
        Printf.fprintf oc "]"
    | x :: r ->
        print x;
        Printf.fprintf oc ", ";
        aux r
  in
  aux l

let print_cm oc s x =
  Printf.fprintf oc "\n%s = sp.bls12_381_g1(\"0x%s\")" s
    (Hex.show (Hex.of_bytes (G1.to_bytes x)))

let print_ev oc s x =
  Printf.fprintf oc "\n%s = sp.bls12_381_fr(\"0x%s\")" s
    (Hex.show (Hex.of_bytes (Scalar.to_bytes x)))

let print_proof oc
    ( cm_a,
      cm_b,
      cm_c,
      cm_z_poly,
      cm_tlo,
      cm_tmi,
      cm_thi,
      cm_w,
      cm_wg,
      eval_a,
      eval_b,
      eval_c,
      eval_ssigma1,
      eval_ssigma2,
      eval_r,
      eval_zg ) =
  print_cm oc "cm_a" cm_a;
  print_cm oc "cm_b" cm_b;
  print_cm oc "cm_c" cm_c;
  print_cm oc "cm_z_poly" cm_z_poly;
  print_cm oc "cm_tlo" cm_tlo;
  print_cm oc "cm_tmi" cm_tmi;
  print_cm oc "cm_thi" cm_thi;
  print_cm oc "cm_w" cm_w;
  print_cm oc "cm_wg" cm_wg;
  print_ev oc "\neval_a" eval_a;
  print_ev oc "eval_b" eval_b;
  print_ev oc "eval_c" eval_c;
  print_ev oc "eval_ssigma1" eval_ssigma1;
  print_ev oc "eval_ssigma2" eval_ssigma2;
  print_ev oc "eval_r" eval_r;
  print_ev oc "eval_zg" eval_zg

let print_preprocessed oc
    ( srs,
      n,
      _permutation,
      _circuit_wires,
      _selectors,
      (cm_ql, cm_qr, cm_qo, cm_qm, cm_qc),
      _ssigmas,
      (cm_ssigma1, cm_ssigma2, cm_ssigma3) ) =
  (* n *)
  Printf.fprintf oc "\nn = %d" n;
  (* g *)
  let generator =
    let module Fr_gen = Bls12_381_polynomial__Fr_generation.Make (Scalar) in
    Fr_gen.root_of_unity Z.(log2up (of_int n))
  in
  print_ev oc "\ngenerator" generator;
  (* SRS *)
  Printf.fprintf oc "\nsrs1 = ";
  let srs1 =
    Polynomial.Srs.to_array Kzg.Public_parameters.(srs.srs1) |> Array.to_list
  in
  print_list oc
    (fun s ->
      Printf.fprintf oc "sp.bls12_381_g1(\"0x%s\")"
        (Hex.show (Hex.of_bytes (G1.to_bytes s))))
    srs1;
  Printf.fprintf oc "\nsrs2 = ";
  Printf.fprintf oc "(sp.bls12_381_g2(\"0x%s\"), sp.bls12_381_g2(\"0x%s\"))"
    (Hex.show
       (Hex.of_bytes (G2.to_bytes Kzg.Public_parameters.(srs.encoding_1))))
    (Hex.show
       (Hex.of_bytes (G2.to_bytes Kzg.Public_parameters.(srs.encoding_x))));
  (* q *)
  print_cm oc "\nqM" cm_qm;
  print_cm oc "qL" cm_ql;
  print_cm oc "qR" cm_qr;
  print_cm oc "qO" cm_qo;
  print_cm oc "qC" cm_qc;
  (* k1, k oc2 *)
  print_ev oc "\nk1" (PM.get_k 1);
  print_ev oc "k2" (PM.get_k 2);
  print_cm oc "\ncm_ssigma1" cm_ssigma1;
  print_cm oc "cm_ssigma2" cm_ssigma2;
  print_cm oc "cm_ssigma3" cm_ssigma3

let prove_verify ?(verbose = false) expected_result (w, circuit) =
  let w_copy = Array.copy w in
  let l = Plonk.Circuit.(circuit.public_input_size) in
  let public_inputs = Array.sub w_copy 0 l in
  let transcript = Hex.to_bytes (`Hex "00") in
  let preprocessed = preprocess circuit in
  try
    let proof =
      generate_proof_for_smartpy_verifier transcript preprocessed w
        public_inputs
    in
    assert (
      verifier transcript preprocessed proof public_inputs = expected_result);
    if verbose then (
      let file = "output" in
      let oc = open_out file in
      try
        print_preprocessed oc preprocessed;
        let proof =
          generate_proof_for_smartpy_verifier transcript preprocessed w
            public_inputs
        in
        print_proof oc proof;
        Printf.fprintf oc "\n\npublic_inputs = ";
        print_list oc
          (fun s ->
            Printf.fprintf oc "sp.bls12_381_fr(\"0x%s\")"
              (Hex.show (Hex.of_bytes (Scalar.to_bytes s))))
          (List.init l (fun i -> w.(i)));
        assert (
          verifier transcript preprocessed proof public_inputs = expected_result);
        close_out oc
      with e ->
        close_out_noerr oc;
        raise e)
    else ()
  with Poly.Rest_not_null _ ->
    if expected_result then
      failwith "Prove shouldn’t fail, expected result is true."
    else ()

let default_circuit =
  (* this circuit does x₀ + x₁ * (x₂ + x₃ * (x₄ + x₅)) *)
  let l = 2 in
  let wires =
    Plonk.Circuit.make_wires ~a:[ 0; 0; 1; 2; 3; 4 ] ~b:[ 0; 9; 8; 7; 6; 5 ]
      ~c:[ 0; 10; 9; 8; 7; 6 ] ()
  in
  (* We added a dummy constraint to use all selectors for the test to pass. *)
  let gates =
    Plonk.Circuit.make_gates
      ~ql:[ zero; one; zero; one; zero; one ]
      ~qr:[ zero; one; zero; one; zero; one ]
      ~qo:[ min_one; min_one; min_one; min_one; min_one; min_one ]
      ~qm:[ zero; zero; one; zero; one; zero ]
      ~qc:[ one; zero; zero; zero; zero; zero ]
      ()
  in
  let circuit = Plonk.Circuit.make ~wires ~gates ~public_input_size:l () in
  circuit

let dummy_circuit i =
  let n = Int.shift_left 1 i in
  let m = 3 + (2 * (n - 1)) in
  let l = 0 in
  let x = Array.init m (fun _ -> Scalar.zero) in
  let wires =
    Plonk.Circuit.make_wires
      ~a:(0 :: List.init (n - 1) (fun i -> i))
      ~b:(0 :: List.init (n - 1) (fun i -> m - 2 - i))
      ~c:(0 :: List.init (n - 1) (fun i -> m - 1 - i))
      ()
  in
  let gates =
    Plonk.Circuit.make_gates
      ~ql:(one :: List.init (n - 1) (fun _ -> zero))
      ~qr:(one :: List.init (n - 1) (fun _ -> zero))
      ~qm:(zero :: List.init (n - 1) (fun _ -> one))
      ~qo:(zero :: List.init (n - 1) (fun _ -> min_one))
      ~qc:(Scalar.of_string "-2" :: List.init (n - 1) (fun _ -> zero))
      ()
  in
  let circuit = Plonk.Circuit.make ~wires ~gates ~public_input_size:l () in
  (x, circuit)

let fr_of_int_safe = Fr_generation.fr_of_int_safe

let w_for_default_circuit =
  [|
    fr_of_int_safe 1;
    fr_of_int_safe 2;
    fr_of_int_safe 3;
    fr_of_int_safe 4;
    fr_of_int_safe 5;
    fr_of_int_safe 6;
    fr_of_int_safe 11;
    fr_of_int_safe 44;
    fr_of_int_safe 47;
    fr_of_int_safe 94;
    fr_of_int_safe 95;
  |]

let wrong_w_for_default_circuit =
  [|
    fr_of_int_safe 1;
    fr_of_int_safe 2;
    fr_of_int_safe 3;
    fr_of_int_safe 4;
    fr_of_int_safe 5;
    fr_of_int_safe 6;
    fr_of_int_safe 11;
    fr_of_int_safe 44;
    fr_of_int_safe 47;
    fr_of_int_safe 94;
    fr_of_int_safe 94;
  |]

let () =
  prove_verify ~verbose:true true (w_for_default_circuit, default_circuit);
  prove_verify false (wrong_w_for_default_circuit, default_circuit)
