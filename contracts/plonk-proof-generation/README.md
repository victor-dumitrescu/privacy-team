Use this to generate proofs for PlonK’s verifier SmartPy contract.
The printed proof can be directly copied and pasted into the verifier.py tests.

To print proof elements on terminal, run:
```
dune build @contracts/plonk/plonk-proof-generation/runtest
```

Default proofs are generated for a default circuit.

To generate and test proofs for other circuits :
- In test/plonk-generate-proof, comment out everything from line 175
- In test/plonk-generate-proof, write circuit as `(i, a, b, c, qL, qR, qO, qM, qC, l)` with i = log₂(number of constraints), l = number of public inputs, a, b, c, qL, qR, qO, qM, qC as lists
- In test/plonk-generate-proof, write witness as an array w
- In test/plonk-generate-proof, `let verify_result = prove_verify (w, circuit)`
- Use `dune build @contracts/plonk/plonk-proof-generation/runtest` and proofs will appear on the terminal
