module Scalar = Bls12_381.Fr
module G1 = Bls12_381.G1
module G2 = Bls12_381.G2
module GT = Bls12_381.GT
module Fr_generation = Plonk.Fr_generation.Make (Scalar)
module Kzg = Plonk.Kzg.Kzg_impl
module PP = Plonk.Polynomial_protocol.Make (Kzg)
module PM = Plonk.Permutation_gate.Permutation_gate_impl (PP)
module Main = Plonk.Main_protocol.Make (PP)
module Domain = Kzg.Polynomial.Domain
module Poly = Kzg.Poly
module Evaluations = Plonk.Evaluations_map.Make (PP.PC.Polynomial.Evaluations)

let one = Scalar.one
let two = Scalar.add one one
let zero = Scalar.zero
let min_one = Scalar.negate one

let k0, k1, k2 =
  ( Fr_generation.fr_of_int_safe 1,
    Fr_generation.fr_of_int_safe 5,
    Fr_generation.fr_of_int_safe 7 )

let build_permutation _n circuit_wires =
  let partition = Main.Partition.build_partition circuit_wires in
  Main.Partition.partition_to_permutation partition

(* ---------------------------- *)

(* returns h(gX) *)
let composition_g h g =
  (* optimisations for regular cases in plonk *)
  if Poly.is_zero h then Poly.zero
  else
    let z_list = Poly.to_dense_coefficients h in
    let h_list = Array.(mapi (fun i e -> (e, i)) z_list) in
    let f (acc, gk, k) (a, i) =
      if Scalar.is_zero a then (acc, gk, k)
      else
        let k = i - k in
        let gi = Scalar.(mul gk (pow g (Z.of_int k))) in
        ((Scalar.mul a gi, i) :: acc, gi, i)
    in
    let ret_list, _, _ = Array.fold_left f ([], Scalar.one, 0) h_list in
    Poly.of_coefficients ret_list

(* Compute public inputs *)
let compute_PI public_inputs domain n =
  if n = 0 then Poly.zero
  else
    let l = Array.length public_inputs in
    let scalar_list =
      Array.(append public_inputs (init (n - l) (fun _ -> Scalar.zero)))
    in
    Poly.(opposite (Evaluations.interpolation_fft2 domain scalar_list))

(* ------------------------------------------------------------------ *)
(* ------------------------------------------------------------------ *)
(* ------------------------------------------------------------------ *)

let hash_to_fr b =
  let hash_b = Plonk.Utils.Hash.hash_bytes [ b ] in
  let hash_trimed =
    Bytes.concat Bytes.empty [ Bytes.sub hash_b 0 31; Hex.to_bytes (`Hex "00") ]
  in
  Scalar.of_bytes_exn hash_trimed

(* generate scalar from transcript in the same way as we do in smartpy *)
let generate_from_transcript (pre, to_bytes) transcript l =
  let transcript =
    Bytes.concat (Hex.to_bytes (`Hex pre)) (transcript :: List.map to_bytes l)
  in
  (transcript, hash_to_fr transcript)

(* returns challenges form the proof *)
let compute_challenges transcript
    ( cm_a,
      cm_b,
      cm_c,
      cm_z_poly,
      cm_tlo,
      cm_tmi,
      cm_thi,
      cm_w,
      cm_wg,
      eval_a,
      eval_b,
      eval_c,
      eval_ssigma1,
      eval_ssigma2,
      eval_zg,
      eval_r ) =
  let transcript, beta =
    generate_from_transcript
      ("050a00000060", G1.to_bytes)
      transcript [ cm_a; cm_b; cm_c ]
  in
  let gamma = hash_to_fr (Plonk.Utils.Hash.hash_bytes [ transcript ]) in
  let transcript, alpha =
    generate_from_transcript
      ("050a00000060", G1.to_bytes)
      transcript [ cm_z_poly ]
  in
  let transcript, z =
    generate_from_transcript
      ("050a00000060", G1.to_bytes)
      transcript [ cm_tlo; cm_tmi; cm_thi ]
  in
  let transcript, v =
    generate_from_transcript
      ("050a00000020", Scalar.to_bytes)
      transcript
      [ eval_a; eval_b; eval_c; eval_ssigma1; eval_ssigma2; eval_zg; eval_r ]
  in
  let _transcript, u =
    generate_from_transcript
      ("050a00000060", G1.to_bytes)
      transcript [ cm_w; cm_wg ]
  in
  (alpha, beta, gamma, z, v, u)

let preprocess
    Plonk.Circuit.
      { circuit_size; wires; gates; public_input_size; table_size; nb_wires; _ }
    =
  let n = circuit_size + public_input_size in
  let domain = Domain.build ~log:Z.(log2up (of_int n)) in
  let srs = fst (Kzg.Public_parameters.setup (n + 5 + 1, 0)) in
  let gates, _gates, wires, _tables =
    let tables = [] in
    Main.Preprocess.preprocessing domain gates wires tables n public_input_size
      circuit_size table_size nb_wires ~ultra:false
  in
  let permutation = build_permutation n wires in
  let ssigmas =
    let map =
      PM.Preprocessing.ssigma_map_non_quadratic_residues ~prefix:"" permutation
        domain (Main.SMap.cardinal wires)
    in
    Main.SMap.bindings map
  in

  let cms_ssigmas =
    List.map (fun (_, p) -> Kzg.Commitment.commit_single srs p) ssigmas
  in
  let gates =
    Main.SMap.
      [
        find "ql" gates;
        find "qr" gates;
        find "qo" gates;
        find "qm" gates;
        find "qc" gates;
      ]
  in
  let cms_selectors = List.map (Kzg.Commitment.commit_single srs) gates in
  let list_to_tuple3 = function
    | [ e1; e2; e3 ] -> (e1, e2, e3)
    | _ -> assert false
  in
  let list_to_tuple5 = function
    | [ e1; e2; e3; e4; e5 ] -> (e1, e2, e3, e4, e5)
    | _ -> assert false
  in
  let gates = list_to_tuple5 gates in
  let cms_selectors = list_to_tuple5 cms_selectors in
  let ssigmas = list_to_tuple3 (List.map snd ssigmas) in
  let cms_ssigmas = list_to_tuple3 cms_ssigmas in
  (srs, n, permutation, wires, gates, cms_selectors, ssigmas, cms_ssigmas)

let generate_proof_for_smartpy_verifier transcript
    ( srs,
      n,
      permutation,
      circuit_wires,
      (ql, qr, qo, qm, qc),
      _cms_selectors,
      (ssigma1, ssigma2, ssigma3),
      _cms_ssigmas ) witness public_inputs =
  let log = Z.(log2up (of_int n)) in
  let generator =
    let module Fr_gen = Bls12_381_polynomial__Fr_generation.Make (Scalar) in
    Fr_gen.root_of_unity log
  in
  let domain = Domain.build ~log in
  (* Round 1 *)
  let f_map =
    let enforced_wires =
      Main.Prover.enforce_wire_values circuit_wires witness
    in
    fst
    @@ Main.Prover.compute_wire_polynomials ~zero_knowledge:false
         ~module_list:[] n domain enforced_wires
  in
  let a = Main.SMap.find "a" f_map in
  let b = Main.SMap.find "b" f_map in
  let c = Main.SMap.find "c" f_map in
  let cm_a = Kzg.Commitment.commit_single srs a in
  let cm_b = Kzg.Commitment.commit_single srs b in
  let cm_c = Kzg.Commitment.commit_single srs c in
  (* Round 2 *)
  let l1 =
    Evaluations.interpolation_fft2 domain
      (zero :: one :: List.init (n - 2) (fun _ -> zero) |> Array.of_list)
  in
  let transcript, beta =
    generate_from_transcript
      ("050a00000060", G1.to_bytes)
      transcript [ cm_a; cm_b; cm_c ]
  in
  let gamma = hash_to_fr (Plonk.Utils.Hash.hash_bytes [ transcript ]) in
  let z_poly =
    let witness_c =
      Evaluations.make_evaluation (Array.length witness - 1, witness)
    in
    PM.Permutation_poly.compute_Z permutation domain beta gamma witness_c
      circuit_wires
    (* TODO: blind these polynomials if we want zero_knowledge *)
  in
  let cm_z_poly = Kzg.Commitment.commit_single srs z_poly in
  (* Round 3 *)
  let pi = compute_PI public_inputs domain n in
  let transcript, alpha =
    generate_from_transcript
      ("050a00000060", G1.to_bytes)
      transcript [ cm_z_poly ]
  in
  let x_poly = Poly.of_coefficients [ (one, 1) ] in
  let y = Poly.of_coefficients [ (gamma, 0) ] in
  let zg = composition_g z_poly generator in
  let f' =
    Poly.(
      (a + mul_by_scalar beta x_poly + y)
      * (b + mul_by_scalar (Scalar.mul beta k1) x_poly + y)
      * (c + mul_by_scalar (Scalar.mul beta k2) x_poly + y)
      * z_poly)
  in
  let g' =
    Poly.(
      (a + mul_by_scalar beta ssigma1 + y)
      * (b + mul_by_scalar beta ssigma2 + y)
      * (c + mul_by_scalar beta ssigma3 + y)
      * zg)
  in
  let t_numerator =
    Poly.(
      (a * b * qm)
      + (a * ql) + (b * qr) + (c * qo) + pi + qc
      + mul_by_scalar alpha (f' + opposite g')
      + mul_by_scalar (Scalar.mul alpha alpha) ((z_poly * l1) + opposite l1))
  in
  let minus_one = Scalar.(negate one) in
  let t = fst @@ Poly.division_xn t_numerator n minus_one in
  let tlo, tmi, thi =
    let rec aux l (acc_tlo, acc_tmi, acc_thi) =
      match l with
      | [] ->
          ( Poly.of_coefficients acc_tlo,
            Poly.of_coefficients acc_tmi,
            Poly.of_coefficients acc_thi )
      | (e, i) :: h ->
          if i > 2 * n then aux h (acc_tlo, acc_tmi, (e, i - (2 * n)) :: acc_thi)
          else if i > n then aux h (acc_tlo, (e, i - n) :: acc_tmi, acc_thi)
          else aux h ((e, i) :: acc_tlo, acc_tmi, acc_thi)
    in
    let t =
      let t = Poly.to_dense_coefficients t |> Array.to_list in
      List.(mapi (fun i e -> (e, i)) t)
    in
    aux t ([], [], [])
  in
  let cm_tlo = Kzg.Commitment.commit_single srs tlo in
  let cm_tmi = Kzg.Commitment.commit_single srs tmi in
  let cm_thi = Kzg.Commitment.commit_single srs thi in
  (* Round 4 *)
  let transcript, z =
    generate_from_transcript
      ("050a00000060", G1.to_bytes)
      transcript [ cm_tlo; cm_tmi; cm_thi ]
  in
  let eval_a = Poly.evaluate a z in
  let eval_b = Poly.evaluate b z in
  let eval_c = Poly.evaluate c z in
  let eval_ssigma1 = Poly.evaluate ssigma1 z in
  let eval_ssigma2 = Poly.evaluate ssigma2 z in
  let eval_t = Poly.evaluate t z in
  let eval_zg = Poly.evaluate zg z in
  let eval_l1 = Poly.evaluate l1 z in
  let m_scal = Poly.mul_by_scalar in
  let r2 =
    Scalar.(
      Poly.add
        (m_scal
           (alpha
           * (eval_a + (beta * z) + gamma)
           * (eval_b + (beta * k1 * z) + gamma)
           * (eval_c + (beta * k2 * z) + gamma))
           z_poly)
        (Poly.opposite
           (m_scal
              (alpha
              * (eval_a + (beta * eval_ssigma1) + gamma)
              * (eval_b + (beta * eval_ssigma2) + gamma)
              * beta * eval_zg)
              ssigma3)))
  in
  let r3 = Scalar.(m_scal (eval_l1 * alpha * alpha) z_poly) in
  let r =
    let p1 = m_scal (Scalar.mul eval_a eval_b) qm in
    let p2 = m_scal eval_a ql in
    let p3 = m_scal eval_b qr in
    let p4 = m_scal eval_c qo in
    let r1 = Poly.(p1 + p2 + p3 + p4 + qc) in
    Poly.(r1 + r2 + r3)
  in
  let eval_r = Poly.evaluate r z in
  (* Round 5 *)
  let _transcript, v =
    generate_from_transcript
      ("050a00000020", Scalar.to_bytes)
      transcript
      [ eval_a; eval_b; eval_c; eval_ssigma1; eval_ssigma2; eval_zg; eval_r ]
  in
  let v2 = Scalar.mul v v in
  let v3 = Scalar.mul v v2 in
  let v4 = Scalar.mul v v3 in
  let v5 = Scalar.mul v v4 in
  let v6 = Scalar.mul v v5 in
  let z_n = Scalar.pow z (Z.of_int n) in
  let z_2n = Scalar.pow z (Z.of_int (2 * n)) in
  let constants c = Poly.constant c in
  let w_poly_num =
    Poly.(
      tlo + mul_by_scalar z_n tmi + mul_by_scalar z_2n thi
      + opposite (constants eval_t)
      + mul_by_scalar v (r + opposite (constants eval_r))
      + mul_by_scalar v2 (a + opposite (constants eval_a))
      + mul_by_scalar v3 (b + opposite (constants eval_b))
      + mul_by_scalar v4 (c + opposite (constants eval_c))
      + mul_by_scalar v5 (ssigma1 + opposite (constants eval_ssigma1))
      + mul_by_scalar v6 (ssigma2 + opposite (constants eval_ssigma2)))
  in
  let w_poly = fst @@ Poly.division_xn w_poly_num 1 (Scalar.negate z) in
  let w_poly_zw =
    fst
    @@ Poly.(
         division_xn
           (z_poly + opposite (constants eval_zg))
           1
           Scalar.(negate @@ mul z generator))
  in
  let cm_w = Kzg.Commitment.commit_single srs w_poly in
  let cm_wg = Kzg.Commitment.commit_single srs w_poly_zw in
  ( cm_a,
    cm_b,
    cm_c,
    cm_z_poly,
    cm_tlo,
    cm_tmi,
    cm_thi,
    cm_w,
    cm_wg,
    eval_a,
    eval_b,
    eval_c,
    eval_ssigma1,
    eval_ssigma2,
    eval_r,
    eval_zg )

let verifier transcript
    ( srs,
      n,
      _permutation,
      _circuit_wires,
      _selectors,
      (cm_ql, cm_qr, cm_qo, cm_qm, cm_qc),
      _ssigmas,
      (cm_ssigma1, cm_ssigma2, cm_ssigma3) )
    ( cm_a,
      cm_b,
      cm_c,
      cm_z_poly,
      cm_tlo,
      cm_tmi,
      cm_thi,
      cm_w,
      cm_wg,
      eval_a,
      eval_b,
      eval_c,
      eval_ssigma1,
      eval_ssigma2,
      eval_r,
      eval_zg ) public_inputs =
  let alpha, beta, gamma, z, v, u =
    compute_challenges transcript
      ( cm_a,
        cm_b,
        cm_c,
        cm_z_poly,
        cm_tlo,
        cm_tmi,
        cm_thi,
        cm_w,
        cm_wg,
        eval_a,
        eval_b,
        eval_c,
        eval_ssigma1,
        eval_ssigma2,
        eval_zg,
        eval_r )
  in
  let generator =
    let module Fr_gen = Bls12_381_polynomial__Fr_generation.Make (Scalar) in
    Fr_gen.root_of_unity Z.(log2up (of_int n))
  in
  let inv = Scalar.inverse_exn in
  (* 5 *)
  let zn = Scalar.pow z (Z.of_int n) in
  let eval_zh = Scalar.(zn + negate one) in
  let inv_g = inv generator in
  let n_fr = Fr_generation.fr_of_int_safe n in
  let eval_l1 = Scalar.(eval_zh * inv (n_fr * ((inv_g * z) + negate one))) in
  let eval_pi =
    let inv_z = inv z in
    let f (acc, gi) wi =
      (* let deno = Scalar.((gi * z) + negate one) in *)
      let deno = Scalar.(gi + negate inv_z) in
      (Scalar.(acc + (wi / deno)), Scalar.mul inv_g gi)
    in
    let res, _ = Array.fold_left f (Scalar.zero, Scalar.one) public_inputs in
    let xn_min_1_div_n = Scalar.(eval_zh / (n_fr * z)) in
    Scalar.(negate (mul xn_min_1_div_n res))
  in
  (* 8 *)
  let eval_t =
    let num =
      Scalar.(
        eval_r + eval_pi
        + negate
            ((eval_a + (beta * eval_ssigma1) + gamma)
            * (eval_b + (beta * eval_ssigma2) + gamma)
            * (eval_c + gamma) * eval_zg * alpha)
        + negate (alpha * alpha * eval_l1))
    in
    Scalar.(num * inv eval_zh)
  in
  (* 9 *)
  let add_g1 = G1.add in
  let mul_g1 x c = G1.mul c x in
  let min_g1 = G1.negate in
  let d =
    let i1 =
      Scalar.(
        add_g1
          (add_g1
             (add_g1
                (mul_g1 (eval_a * eval_b * v) cm_qm)
                (mul_g1 (eval_a * v) cm_ql))
             (add_g1 (mul_g1 (eval_b * v) cm_qr) (mul_g1 (eval_c * v) cm_qo)))
          (mul_g1 v cm_qc))
    in
    let i2 =
      Scalar.(
        add_g1
          (mul_g1
             ((eval_a + (beta * z) + gamma)
              * (eval_b + (beta * k1 * z) + gamma)
              * (eval_c + (beta * k2 * z) + gamma)
              * alpha * v
             + (eval_l1 * alpha * alpha * v)
             + u)
             cm_z_poly)
          (min_g1
             (mul_g1
                ((eval_a + (beta * eval_ssigma1) + gamma)
                * (eval_b + (beta * eval_ssigma2) + gamma)
                * alpha * v * beta * eval_zg)
                cm_ssigma3)))
    in
    add_g1 i1 i2
  in
  (* 10 *)
  let v2 = Scalar.mul v v in
  let v3 = Scalar.mul v v2 in
  let v4 = Scalar.mul v v3 in
  let v5 = Scalar.mul v v4 in
  let v6 = Scalar.mul v v5 in
  let f =
    add_g1
      (add_g1
         (add_g1
            (add_g1
               (add_g1
                  (add_g1
                     (add_g1
                        (add_g1 cm_tlo (mul_g1 zn cm_tmi))
                        (mul_g1 (Scalar.mul zn zn) cm_thi))
                     d)
                  (mul_g1 v2 cm_a))
               (mul_g1 v3 cm_b))
            (mul_g1 v4 cm_c))
         (mul_g1 v5 cm_ssigma1))
      (mul_g1 v6 cm_ssigma2)
  in
  let e =
    Scalar.(
      eval_t + (v * eval_r) + (v2 * eval_a) + (v3 * eval_b) + (v4 * eval_c)
      + (v5 * eval_ssigma1) + (v6 * eval_ssigma2) + (u * eval_zg))
  in
  let ps =
    let uzg = Scalar.(u * z * generator) in
    let srs1 = Polynomial.Srs.to_array Kzg.Public_parameters.(srs.srs1) in
    let p1 =
      add_g1
        (add_g1 (add_g1 (mul_g1 z cm_w) (mul_g1 uzg cm_wg)) f)
        (min_g1 (mul_g1 e srs1.(0)))
    in
    let p2 = min_g1 (add_g1 cm_w (mul_g1 u cm_wg)) in
    [
      (p1, Kzg.Public_parameters.(srs.encoding_1));
      (p2, Kzg.Public_parameters.(srs.encoding_x));
    ]
  in
  Bls12_381.Pairing.pairing_check ps
