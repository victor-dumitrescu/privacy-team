 ## How to get the Michelson contract from the SmartPy contract
 ### From the [SmartPy online editor](https://smartpy.io/)
 This only works if your contract have tests
 - Open or copy-paste your SmartPy contract in the [SmartPy online editor](https://smartpy.io/)
 - Click on the `Run` button : it will run the tests and generate the Michelson code 
 - In output, click on `Deploy Michelson Contract` on the same line as `SmartPy` and `Types`, and then on the `Deploy Michelson Contract` button to reach [origination page](https://smartpy.io/origination) ; click on `CODE` at the end of the page to get the Michelson code
 
### By installing SmartPy
- Install [SmartPy](https://gitlab.com/SmartPy/smartpy/) locally by following the instructions
- Run `~/SmartPy/smartpy-cli/SmartPy.sh compile contracts/plonk/verifier.py contracts/plonk/_build/` : it will create the contract `step_000_cont_0_contract.tz` in `_build`
- The generated contract may have a lot of comments that can make the filesize too big to be used in Tezos. The following command can be run to get the code without comments in `verifier.tz` : `cat contracts/plonk/_build/Verifier/step_000_cont_0_contract.tz | cut -d\# -f1 > contracts/plonk/_build/Verifier/verifier.tz`