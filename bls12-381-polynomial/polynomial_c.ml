(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Fr = Bls12_381.Fr

module Stubs = struct
  type fr = Fr.t
  type fr_array = Carray.Stubs.fr_array

  external degree : fr_array -> int -> int = "caml_polynomial_degree_stubs"
    [@@noalloc]
  (** [degree p n] returns the degree of a polynomial [p]

  - requires: [size p = n] *)

  external of_sparse : fr_array -> (fr * int) array -> int -> unit
    = "caml_polynomial_of_sparse_stubs"
    [@@noalloc]
  (** [of_sparse res p n] converts the sparse representation of a polynomial [p] to
  the dense representation, from an OCaml array [p] of size [n] to a C array [res] of
  size [degree p + 1]

  requires:
  - degree of each coeff [d_i >= 0] and [d_i] are unique
  - the result must be initialized with zero (as done by {!Carray.allocate})
  - [size res = degree p + 1]
  - [size p = n] *)

  external add : fr_array -> fr_array -> fr_array -> int -> int -> unit
    = "caml_polynomial_add_stubs"
    [@@noalloc]
  (** [add res a b size_a size_b] writes the result of polynomial addition of [a] and [b]
  in [res]

  requires:
  - [size a = size_a]
  - [size b = size_b]
  - [size res = max (size_a, size_b)]
  - [res], [a] and [b] are either pairwise disjoint or equal *)

  external sub : fr_array -> fr_array -> fr_array -> int -> int -> unit
    = "caml_polynomial_sub_stubs"
    [@@noalloc]
  (** [sub res a b size_a size_b] writes the result of polynomial subtraction of [b] from [a]
  in [res]

  requires:
  - [size a = size_a]
  - [size b = size_b]
  - [size res = max (size_a, size_b)]
  - [res], [a] and [b] are either pairwise disjoint or equal *)

  external mul : fr_array -> fr_array -> fr_array -> int -> int -> unit
    = "caml_polynomial_mul_stubs"
    [@@noalloc]
  (** [mul res a b size_a size_b] writes the result of polynomial multiplication of [a] by [b]
  in [res]

  requires:
  - the result must be initialized with zero (as done by {!Carray.allocate})
  - [size a = size_a]
  - [size b = size_b]
  - [size res = size_a + size_b - 1] *)

  external mul_by_scalar : fr_array -> fr -> fr_array -> int -> unit
    = "caml_polynomial_mul_by_scalar_stubs"
    [@@noalloc]
  (** [mul_by_scalar res b a size_a] writes the result of multiplying a polynomial [a]
  by a blst_fr element [b] in [res]

  requires:
  - [size a = size_a]
  - [size res = size_a]
  - [res] and [a] either disjoint or equal *)

  external linear : fr_array -> (fr_array * int * fr) array -> int -> unit
    = "caml_polynomial_linear_stubs"
    [@@noalloc]
  (** [linear res poly_polylen_coeff nb_polys] writes the result of
      computing [λ₁·p₁(x) + λ₂·p₂(x) + … + λₖ·pₖ(x)] in [res], where
      - [poly_polylen_coeff.[i] = (pᵢ, size_p_i, λᵢ)]
      - [nb_polys] is a number of polynomials, i.e., [i = 1..nb_polys]

   requires:
   - the result must be initialized with zero (as done by {!Carray.allocate})
   - [size res = max (size_p_i)]
   - [size poly_polylen_coeff = nb_polys]
   - [size p_i = size_p_i] *)

  external linear_with_powers :
    fr_array -> fr -> (fr_array * int) array -> int -> unit
    = "caml_polynomial_linear_with_powers_stubs"
    [@@noalloc]
  (** [linear_with_powers res c poly_polylen nb_polys] writes the result of
      computing [c⁰·p₀(x) + c¹·p₁(x) + … + cᵏ·pₖ(x)] in [res], where
      - [poly_polylen.[i] = (pᵢ, size_p_i)]
      - [nb_polys] is a number of polynomials

   requires:
   - the result must be initialized with zero (as done by {!Carray.allocate})
   - [size res = max (size_p_i)]
   - [size poly_polylen = nb_polys]
   - [size p_i = size_p_i] *)

  external negate : fr_array -> fr_array -> int -> unit
    = "caml_polynomial_negate_stubs"
    [@@noalloc]
  (** [negate res p n] writes the result of negating a polynomial [p] in [res]

  requires:
  - [size p = n]
  - [size res = n]
  - [res] and [p] either disjoint or equal *)

  external is_zero : fr_array -> int -> bool = "caml_polynomial_is_zero_stubs"
    [@@noalloc]
  (** [is_zero p n] checks whether a polynomial [p] is the zero polynomial

  - requires: [size p = n] *)

  external evaluate : fr -> fr_array -> int -> fr -> unit
    = "caml_polynomial_evaluate_stubs"
    [@@noalloc]
  (** [evaluate res p n x] writes the result of evaluating a polynomial [p] at [x]
  in [res]

  - requires: [size p = n] and [n > 0] *)

  external division_xn :
    fr_array -> fr_array -> fr_array -> int -> int * fr -> unit
    = "caml_polynomial_division_xn_stubs"
    [@@noalloc]
  (** [division_xn res_q res_r p size_p (n, c)] writes the quotient and remainder of
      the division of a polynomial [p] by [(X^n + c)] in [res]

  requires:
  - [size p = size_p] and [size_p > n]
  - [size res_q = size_p - n]
  - [size res_r = n] *)

  external mul_xn : fr_array -> fr_array -> int -> int -> fr -> unit
    = "caml_polynomial_mul_xn_stubs"
    [@@noalloc]
  (** [mul_xn res p size_p n c] writes the result of multiplying a polynomial [p]
      by [(X^n + c)] in [res]

  requires:
  - [res] is initialized with bls-fr zeros
  - [size p = size_p]
  - [size res = size_p + n] *)

  external derivative : fr_array -> fr_array -> int -> unit
    = "caml_polynomial_derivative_stubs"
    [@@noalloc]
end

module Polynomial_impl = struct
  type scalar = Fr.t
  type t = Carray.t

  let of_carray : Carray.t -> t = fun x -> x
  let to_carray : t -> Carray.t = fun x -> x
  let allocate n = (Carray.allocate n, n)
  let erase p = Carray.erase p
  let length (_, n) = n
  let copy = Carray.copy
  let to_string = Carray.to_string
  let get = Carray.get
  let degree (p, n) = Stubs.degree p n

  (* ?of_sparse_coefficients *)
  let of_coefficients coefficients =
    let coefficients = Array.of_list coefficients in
    let degree =
      Array.fold_left
        (fun max_degree (_coeff, d) ->
          assert (d >= 0);
          max d max_degree)
        0 coefficients
    in
    let polynomial = Carray.allocate (degree + 1) in
    Stubs.of_sparse polynomial coefficients (Array.length coefficients);
    (polynomial, degree + 1)

  let of_dense = Carray.of_array

  (* zero = [0] and not [] *)
  let zero = of_coefficients []
  let one = of_coefficients [ (Fr.one, 0) ]

  let generate_biased_random_polynomial n =
    if Random.int 10 = 0 then zero
    else
      let poly =
        List.init n (fun i ->
            if Random.bool () then (Fr.random (), i) else (Fr.copy Fr.zero, i))
      in
      of_coefficients poly

  let random n = List.init n (fun i -> (Fr.random (), i)) |> of_coefficients

  let to_dense_coefficients (polynomial, n) =
    (* the polynomial could be padded with zero, so we instead of using [n] and
       wasting some space we recompute the size of the minimal representation *)
    let size = 1 + max 0 (Stubs.degree polynomial n) in
    Carray.to_array (polynomial, size)

  (* ensures: no coefficient in the result is zero *)
  let to_sparse_coefficients poly =
    let poly = to_dense_coefficients poly in
    let res = ref [] in
    for deg = Array.length poly - 1 downto 0 do
      let coef = poly.(deg) in
      if not (Fr.is_zero coef) then res := (Fr.copy coef, deg) :: !res
    done;
    !res

  let encoding : t Data_encoding.t =
    Data_encoding.(
      let fr_encoding = conv Fr.to_bytes Fr.of_bytes_exn bytes in
      conv to_dense_coefficients of_dense (array fr_encoding))

  let equal = Carray.equal

  let add (poly_1, size_1) (poly_2, size_2) =
    let res_size = max size_1 size_2 in
    let res = Carray.allocate res_size in
    Stubs.add res poly_1 poly_2 size_1 size_2;
    (res, res_size)

  let add_inplace (poly_res, size_res) (poly_1, size_1) (poly_2, size_2) =
    assert (size_res = max size_1 size_2);
    Stubs.add poly_res poly_1 poly_2 size_1 size_2

  let sub (poly_1, size_1) (poly_2, size_2) =
    let max_size = max size_1 size_2 in
    let res = Carray.allocate max_size in
    Stubs.sub res poly_1 poly_2 size_1 size_2;
    (res, max_size)

  let sub_inplace (poly_res, size_res) (poly_1, size_1) (poly_2, size_2) =
    assert (size_res >= max size_1 size_2);
    Stubs.sub poly_res poly_1 poly_2 size_1 size_2

  let mul (poly_1, size_1) (poly_2, size_2) =
    let res_size = size_1 + size_2 - 1 in
    let res = Carray.allocate res_size in
    Stubs.mul res poly_1 poly_2 size_1 size_2;
    (res, res_size)

  let mul_by_scalar scalar (poly, size) =
    let res = Carray.allocate size in
    Stubs.mul_by_scalar res scalar poly size;
    (res, size)

  let mul_by_scalar_inplace (poly_res, size_res) scalar (poly, size) =
    assert (size_res >= size);
    Stubs.mul_by_scalar poly_res scalar poly size

  let linear polys coeffs =
    let nb_polys = List.length polys in
    assert (nb_polys = List.length coeffs);
    let res_size =
      List.fold_left (fun res_size (_, size) -> max size res_size) 0 polys
    in
    let res = Carray.allocate res_size in
    let poly_polylen_coeff =
      List.map2 (fun (p, size) coeff -> (p, size, coeff)) polys coeffs
    in
    Stubs.linear res (Array.of_list poly_polylen_coeff) nb_polys;
    (res, res_size)

  let linear_with_powers polys coeff =
    let nb_polys = List.length polys in
    let res_size =
      List.fold_left (fun res_size (_, size) -> max size res_size) 0 polys
    in
    let res = Carray.allocate res_size in
    Stubs.linear_with_powers res coeff (Array.of_list polys) nb_polys;
    (res, res_size)

  let opposite (poly, size) =
    let res = Carray.allocate size in
    Stubs.negate res poly size;
    (res, size)

  let opposite_inplace (poly, size) = Stubs.negate poly poly size
  let is_zero (poly, size) = Stubs.is_zero poly size

  let evaluate (p, s) scalar =
    let res = Fr.copy scalar in
    Stubs.evaluate res p s scalar;
    res

  exception Rest_not_null of string

  let division_xn (poly, size) n c =
    assert (n > 0);
    let poly_degree = degree (poly, size) in
    let poly_size = poly_degree + 1 in
    if poly_degree = -1 || poly_degree < n then (zero, (poly, size))
    else
      let res_q = Carray.allocate (poly_size - n) in
      let res_r = Carray.allocate n in
      Stubs.division_xn res_q res_r poly poly_size (n, c);
      let poly_q = (res_q, poly_size - n) in
      let poly_r = (res_r, n) in
      (poly_q, poly_r)

  let mul_xn (poly, size) n c =
    let res = Carray.allocate (size + n) in
    Stubs.mul_xn res poly size n c;
    (res, size + n)

  let derivative (poly, size) =
    if is_zero (poly, size) || size = 1 then zero
    else
      let res = Carray.allocate (size - 1) in
      Stubs.derivative res poly size;
      (res, size - 1)

  (* let p_chunks = [p_1; p_2; ...; p_k; p_last], where
     - k = nb_coeff_P / size_chunk
     - p_i = slice p (i * size_chunk) ((i + 1) * size_chunk)
     - p_last = slice p (k * size_chunk) nb_coeff_P
     - p_i has size [size_chunk] and p_last has size [nb_coeff_P mod size_chunk]

     if p_k and p_last are not empty and their total size is <= d then
     p_k and p_last are merged, i.e., split returns [p_1; p_2; ...; p_k || p_last],
     Otherwise, split returns p_chunks *)
  let split d size_chunk p =
    let poly_degree = degree p in
    let nb_coeff_P = 1 + poly_degree in
    let size_last_chunk = nb_coeff_P mod size_chunk in
    let size_two_last_blocks = size_chunk + size_last_chunk in
    (* TODO: could we merge more than two blocks when they fit d? *)
    let nb_chunks, is_merged =
      let k = nb_coeff_P / size_chunk in
      if nb_coeff_P = size_last_chunk then (1, false)
      else if size_last_chunk = 0 then (k, false)
      else if size_two_last_blocks <= nb_coeff_P && size_two_last_blocks <= d
      then (k, true)
      else (k + 1, false)
    in

    if poly_degree = -1 then [ zero ]
    else
      List.init nb_chunks (fun i ->
          let len =
            if i = nb_chunks - 1 then
              if is_merged then size_two_last_blocks
              else
                (* Note: this can be equal to size_chunk when size_last_chunk = 0 *)
                nb_coeff_P - (i * size_chunk)
            else size_chunk
          in
          copy ~offset:(i * size_chunk) ~len p)

  (* TODO : Marina is building a C implementation of this
     function using Horner's method *)
  let batch s ps =
    let ss =
      List.fold_left
        (fun (ss, s_k) _ -> (s_k :: ss, Fr.mul s s_k))
        ([], Fr.one) ps
      |> fst
    in
    linear ps ss

  let blind ~nb_blinds n p =
    let blinding_factor = random nb_blinds in
    let zh = of_coefficients [ (Fr.one, n); (Fr.negate Fr.one, 0) ] in
    (add p (mul zh blinding_factor), blinding_factor)

  let ( = ) = equal
  let ( + ) = add
  let ( - ) = sub
  let ( * ) = mul
  let constant c = of_coefficients [ (c, 0) ]
end

module type Polynomial_sig = sig
  (**
   This library implements polynomials of Bls12_381.Fr as arrays of contiguous
   memory in C, allowing much better performances for algorithms that scan the
   polynomials.

   An array [a] of size [n] represents the polynomial $\sum_i^(n-1) a[i] X^i$
   The length of [a] is always greater or equal than the degree+1 of its
   corresponding polynomial, if greater it padded with zeros. As a consequence a
   polynomial has many representations, namely all arrays with trailing zeros.
 *)

  type scalar
  type t

  val encoding : t Data_encoding.t

  val allocate : int -> t
  (** [allocate len] creates a zero polynomial of size [len] *)

  val erase : t -> unit
  (** [erase p] overwrites a polynomial [p] with a zero polynomial of
      the same size as the polynomial [p] *)

  val length : t -> int
  (** [length p] returns the length of a C array where a polynomial [p] is stored *)

  val generate_biased_random_polynomial : int -> t
  (** [generate_biased_random_polynomial n] generates a random polynomial of
       degree strictly lower than [n], the distribution is NOT uniform, it is
       biased towards sparse polynomials and particularly towards the zero
       polynomial *)

  val random : int -> t
  (** [random n] generates a uniformly sampled polynomial among the set of all
      polynomials of degree strictly lower than [n] *)

  val degree : t -> int
  (** [degree p] returns the degree of a polynomial [p]. Returns [-1] for the
  zero polynomial *)

  val get : t -> int -> scalar
  (** [get p i] returns the [i]-th element of a given array [p], a coefficient of [X^i]
  in [p] *)

  val to_string : t -> string
  (** [to_string p] returns the string representation of a polynomial [p] *)

  val copy : ?offset:int -> ?len:int -> t -> t
  (** [copy p] returns a copy of a polynomial [p] *)

  val to_dense_coefficients : t -> scalar array
  (** [to_dense_coefficients p] returns the dense representation of
  a polynomial [p], i.e., it converts a C array to an OCaml array *)

  val of_dense : scalar array -> t
  (** [of_dense p] creates a value of type [t] from the dense representation of
  a polynomial [p], i.e., it converts an OCaml array to a C array *)

  val of_coefficients : (scalar * int) list -> t
  (** [of_coefficients p] creates a value of type [t] from the sparse representation of
  a polynomial [p], i.e., it converts an OCaml array to a C array *)

  val equal : t -> t -> bool
  (** [equal a b] checks whether a polynomial [a] is equal to a polynomial [b] *)

  val is_zero : t -> bool
  (** [is_zero p] checks whether a polynomial [p] is the zero polynomial *)

  val zero : t
  (** [zero] is the zero polynomial, the neutral element for polynomial addition *)

  val one : t
  (** [one] is the constant polynomial one, the neutral element for polynomial
  multiplication *)

  val add : t -> t -> t
  (** [add] computes polynomial addition *)

  val add_inplace : t -> t -> t -> unit
  (** [add_inplace res a b] computes polynomial addition of [a] and [b] and
      writes the result in [res]

  Note: [res] can be equal to either [a] or [b] *)

  val sub : t -> t -> t
  (** [sub] computes polynomial subtraction *)

  val sub_inplace : t -> t -> t -> unit
  (** [sub_inplace res a b] computes polynomial subtraction of [a] and [b] and
      writes the result in [res]

  Note: [res] can be equal to either [a] or [b] *)

  val mul : t -> t -> t
  (** [mul] computes polynomial multiplication

  Note: naive quadratic algorithm, result's size is the sum of arguments' size *)

  val mul_by_scalar : scalar -> t -> t
  (** [mul_by_scalar] computes multiplication of a polynomial by a blst_fr element *)

  val mul_by_scalar_inplace : t -> scalar -> t -> unit
  (** [mul_by_scalar_inplace res s p] computes multiplication of a polynomial [p]
  by a blst_fr element [s] and stores it in [res] *)

  val linear : t list -> scalar list -> t
  (** [linear p s] computes [∑ᵢ s.(i)·p.(i)] *)

  val linear_with_powers : t list -> scalar -> t
  (** [linear_with_powers p s] computes [∑ᵢ sⁱ·p.(i)]. This function is more efficient
      than [linear] + [powers] if all polynomials are approximately of the same size *)

  val opposite : t -> t
  (** [opposite] computes polynomial negation *)

  val opposite_inplace : t -> unit
  (** [opposite_inplace p] computes polynomial negation

  Note: The argument [p] is overwritten *)

  val evaluate : t -> scalar -> scalar
  (** [evaluate p x] evaluates a polynomial [p] at [x] *)

  exception Rest_not_null of string

  val division_xn : t -> int -> scalar -> t * t
  (** [division_xn p n c] returns the quotient and remainder of the division of
      [p] by [(X^n + c)] *)

  val mul_xn : t -> int -> scalar -> t
  (** [mul_xn p n c] returns the product of [p] and [(X^n + c)] *)

  val derivative : t -> t
  (** [derivative p] returns the formal derivative of [p] *)

  val split : int -> int -> t -> t list

  val batch : scalar -> t list -> t
  (** [batch s ps] returns [\sum_i s^i * ps_i] where [i] ranges from [0] to
      [List.length ps - 1] *)

  val blind : nb_blinds:int -> int -> t -> t * t
  (** [blind ~nb_blinds n p] adds to polynomial [p] a random multiple of
      polynomial [(X^n - 1)], chosen by uniformly sampling a polynomial [b]
      of degree strictly lower than [nb_blinds] and multiplying it by
      [(X^n - 1])], [b] is returned as the second argument *)

  val ( = ) : t -> t -> bool
  (** Infix operator for {!equal} *)

  val ( + ) : t -> t -> t
  (** Infix operator for {!add} *)

  val ( - ) : t -> t -> t
  (** Infix operator for {!sub} *)

  val ( * ) : t -> t -> t
  (** Infix operator for {!mul} *)

  val constant : scalar -> t
  (** [constant s] creates a value of type [t] from a blst_fr element [s] *)
end

module type Polynomial_unsafe_sig = sig
  include Polynomial_sig

  val to_carray : t -> Carray.t
  (** [to_carray p] converts [p] from type {!t} to type {!Carray.t}

      Note: [to_carray p] doesn't create a copy of [p] *)

  val of_carray : Carray.t -> t
  (** [of_carray p] converts [p] from type {!Carray.t} to type {!t}

      Note: [of_carray p] doesn't create a copy of [p] *)
end

module Polynomial_unsafe :
  Polynomial_unsafe_sig with type scalar = Bls12_381.Fr.t =
  Polynomial_impl

include (
  Polynomial_unsafe :
    Polynomial_sig
      with type scalar = Polynomial_unsafe.scalar
       and type t = Polynomial_unsafe.t)
