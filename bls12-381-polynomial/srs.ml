(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Stubs = struct
  type t

  external allocate : int -> t = "caml_polynomial_srs_allocate_stubs"
  (** [allocate n] allocates a custom block to save a SRS of size [n] *)

  external load_from_file : t -> string -> int -> int -> bool
    = "caml_polynomial_srs_load_from_file_stubs"

  external of_array : t -> Bls12_381.G1.t array -> int -> unit
    = "caml_polynomial_srs_of_array_stubs"
    [@@noalloc]
  (** [of_array srs jacobian_pts length] feeds the SRS
      [srs] with the SRS given in jacobian coordinates *)

  external to_array : Bls12_381.G1.t array -> t -> int -> unit
    = "caml_polynomial_srs_to_array_stubs"
    [@@noalloc]
  (** [to_array jacobian_pts_array srs length]
      extracts the SRS points from [srs] and feed it in
      [jacobian_pts_array]
  *)

  external get : Bls12_381.G1.t -> t -> int -> unit
    = "caml_polynomial_srs_get_stubs"
    [@@noalloc]
  (** [get res srs i] writes [i]-th element of [srs] in [res]

  - requires: [0 <= i < size srs]
  - ensures: [res = srs[i]] *)

  external pippenger :
    Bls12_381.G1.t -> t -> Carray.Stubs.fr_array -> int -> int -> int -> unit
    = "caml_polynomial_pippenger_stubs_bytecode" "caml_polynomial_pippenger_stubs"
  (** [pippenger res srs fr_array start len] uses pippenger to compute multi
      scalar exponentiation *)
end

module type S = sig
  type polynomial

  type t
  (** A context to be used with {!pippenger} *)

  val encoding : t Data_encoding.t
  (** Simply encodes the Srs as the filename where
      it was read from and its size. *)

  val encoding_full : t Data_encoding.t
  (** Encodes the whole Srs as an OCaml array *)

  val get : t -> int -> Bls12_381.G1.t
  (** [get srs i] returns the [i]-th element of [srs] *)

  val size : t -> int
  (** Returns the pippenger ctxt size, i.e. the number of elements the context
      is supposed to be called with *)

  val load_from_file : string -> int -> t
  (** [srs_load_from_file path ~offset size] reads [size] points of g1 in affine
      compressed format from the file at [path] starting at [offset]. *)

  val pippenger : ?start:int -> ?len:int -> t -> polynomial -> Bls12_381.G1.t
  (** [pippenger ctxt poly] computes the multiscalar exponentiation using the
      SRS saved in the context and the coefficients of the given polynomial *)

  (** For testing only  *)

  val of_array : Bls12_381.G1.t array -> t
  val to_array : t -> Bls12_381.G1.t array
end

module M = struct
  type polynomial = Polynomial_c.Polynomial_unsafe.t
  type t = { path : string; data : Stubs.t * int }

  let size { data = _, n; _ } = n

  let get { data = srs, n; _ } i =
    if i < 0 || i >= n then raise @@ Invalid_argument "get: index out of bounds";
    let res = Bls12_381.G1.(copy zero) in
    Stubs.get res srs i;
    res

  let load_from_file path n =
    let srs = Stubs.allocate n in
    let success = Stubs.load_from_file srs path n 0 in
    if success then { path; data = (srs, n) }
    else
      raise
      @@ Invalid_argument
           (Format.sprintf
              "The path %s is either invalid or it does not contain %d elements"
              path n)

  let encoding =
    let open Data_encoding in
    conv
      (fun { path; data = _, size } -> (path, size))
      (fun (path, size) -> load_from_file path size)
      (tup2 string int31)

  let of_array a =
    let length = Array.length a in
    let srs = Stubs.allocate length in
    Stubs.of_array srs a length;
    { path = ""; data = (srs, length) }

  let to_array { data = srs, n; _ } =
    let pts = Array.init n (fun _ -> Bls12_381.G1.(copy one)) in
    Stubs.to_array pts srs n;
    pts

  let encoding_full =
    let open Data_encoding in
    let g1_encoding =
      let open Bls12_381 in
      conv G1.to_bytes G1.of_bytes_exn (Fixed.bytes G1.size_in_bytes)
    in
    conv to_array of_array (array g1_encoding)

  let pippenger ?(start = 0) ?len { data = srs, ctxt_size; _ } poly =
    let coefficients, n = Polynomial_c.Polynomial_unsafe.to_carray poly in
    let l = min ctxt_size n in
    let len = Option.value ~default:(l - start) len in
    if start < 0 || len < 1 || start + len > l then
      raise
      @@ Invalid_argument
           (Format.sprintf "start %d len %d poly size %d ctxt size %d" start len
              n ctxt_size);
    let res = Bls12_381.G1.(copy zero) in
    Stubs.pippenger res srs coefficients start len ctxt_size;
    res
end
