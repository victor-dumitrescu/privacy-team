(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Fr = Bls12_381.Fr

module Stubs = struct
  type fr = Fr.t
  type fr_array = Carray.Stubs.fr_array

  external compute_domain : fr_array -> int -> fr -> unit
    = "caml_polynomial_compute_domain_stubs"
    [@@noalloc]
  (** [compute_domain res n g] computes [[one; g; ..; g^{n-1}]] for a given
  blst_fr element [g]

  - requires: [n] is even and [0 < n <= size res]
  - ensures: [res[i] = g^i] for [i = 0..(n-1)] *)

  external rescale : fr_array -> fr_array -> int -> int -> unit
    = "caml_polynomial_evaluations_rescale_stubs"
    [@@noalloc]
  (** [rescale res a size_res size_a] writes the result of rescaling the evaluation
      representation of a polynomial [a] from [domain_a] of size [size_a] to
      [domain_res] of size [size_res] in [res]

   requires:
   - [size res = size_res]
   - [size a = size_a]
   - [size_res <= size_a]
   - [res] and [a] are disjoint
   - [size_res mod size_a = 0] *)
end

module Domain_impl = struct
  type scalar = Bls12_381.Fr.t
  type t = Carray.t

  let encoding = Carray.encoding
  let of_carray : Carray.t -> t = fun x -> x
  let to_carray : t -> Carray.t = fun x -> x
  let of_array = Carray.of_array
  let to_array = Carray.to_array
  let length = Carray.length
  let get = Carray.get

  let create log root_of_unity =
    let n = 1 lsl log in
    let domain = Carray.allocate n in
    Stubs.compute_domain domain n root_of_unity;
    (domain, n)

  let build ~log =
    let module Fr_g = Fr_generation.Make (Fr) in
    let root_u = Fr_g.root_of_unity log in
    create log root_u

  let subgroup ~log (d, l) =
    let n = 1 lsl log in
    if n > l || log <= 0 then raise @@ Invalid_argument "subgroup: wrong order"
    else
      let dom = Carray.allocate n in
      Stubs.rescale dom d n l;
      (dom, n)

  let inverse domain =
    let n = Carray.length domain in
    Array.init n (fun i ->
        if i = 0 then Fr.(copy one) else Carray.get domain (n - i))
end

module type Domain_sig = sig
  type scalar
  type t

  val encoding : t Data_encoding.t

  val length : t -> int
  (** [length p] returns the length of a given array [p] *)

  val get : t -> int -> scalar
  (** [get p i] returns the [i]-th element of a given array [p] *)

  val build : log:int -> t
  (** [build log] computes [[one; g; ..; g^{n-1}]] where [g] is a primitive
    [n]-th root of unity and [n = 2^log] *)

  val subgroup : log:int -> t -> t
  (** [subgroup log d] returns a subgroup of [d] of order [2^log] *)

  val inverse : t -> scalar array
  (** [inverse d] returns for a domain [wⁱᵢ] its inverse domain [w⁻ⁱᵢ] *)
end

module type Domain_unsafe_sig = sig
  include Domain_sig

  val to_carray : t -> Carray.t
  (** [to_carray d] converts [d] from type {!t} to type {!Carray.t}

      Note: [to_carray d] doesn't create a copy of [d] *)

  val of_carray : Carray.t -> t
  (** [of_carray d] converts [d] from type {!Carray.t} to type {!t}

      Note: [of_carray d] doesn't create a copy of [d] *)

  val to_array : t -> scalar array
  (** [to_array d] converts a C array [d] to an OCaml array *)

  val of_array : scalar array -> t
  (** [of_array d] converts an OCaml array [d] to a C array *)
end

module Domain_unsafe : Domain_unsafe_sig with type scalar = Bls12_381.Fr.t =
  Domain_impl

include (
  Domain_unsafe :
    Domain_sig
      with type t = Domain_unsafe.t
       and type scalar = Domain_unsafe.scalar)
