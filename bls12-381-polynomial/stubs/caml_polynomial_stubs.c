/* MIT License
 * Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "caml_bls12_381_stubs.h"
#include "ocaml_integers.h"
#include "polynomial.h"
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>

#define Blst_fr_array_val(v) (*(blst_fr **)Data_custom_val(v))

static void finalize_blst_fr_array(value v) { free(Blst_fr_array_val(v)); }
static struct custom_operations blst_fr_array_ops = {
    "blst_fr_array",
    finalize_blst_fr_array,
    custom_compare_default,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default,
    custom_compare_ext_default,
    custom_fixed_length_default};

CAMLprim value caml_polynomial_allocate_fr_array_stubs(value n) {
  CAMLparam1(n);
  CAMLlocal1(block);
  int n_c = Int_val(n);
  // Allocating the polynomial in the C heap to avoid too big values in the Caml
  // heap
  size_t size = sizeof(blst_fr) * n_c;
  block = caml_alloc_custom_mem(&blst_fr_array_ops, sizeof(blst_fr *), size);
  void *p = calloc(1, size);
  if (p == NULL) {
    caml_raise_out_of_memory();
  }
  blst_fr **block_fr = (blst_fr **)(Data_custom_val(block));
  *block_fr = (blst_fr *)p;
  CAMLreturn(block);
}

CAMLprim value caml_polynomial_erase_fr_array_stubs(value buffer, value n) {
  CAMLparam2(buffer, n);
  int n_c = Int_val(n);
  void *buffer_c = (Blst_fr_array_val(buffer));
  memset(buffer_c, 0, sizeof(blst_fr) * n_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_compute_domain_stubs(value buffer, value n,
                                                    value root_of_unity) {
  CAMLparam3(buffer, n, root_of_unity);
  blst_fr *root_of_unity_c = Blst_fr_val(root_of_unity);
  int n_c = Int_val(n);
  blst_fr *buffer_c = Blst_fr_array_val(buffer);

  blst_fr_set_to_one(buffer_c);
  memcpy(buffer_c + 1, root_of_unity_c, sizeof(blst_fr));

  for (int i = 1; i < n_c / 2; i++) {
    blst_fr_sqr(buffer_c + 2 * i, buffer_c + i);
    blst_fr_mul(buffer_c + 2 * i + 1, buffer_c + 2 * i, root_of_unity_c);
  }

  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_of_fr_array_stubs(value buffer, value vs,
                                                 value n) {
  CAMLparam3(buffer, vs, n);

  blst_fr *vs_c = Blst_fr_array_val(vs);
  int buffer_size = Int_val(n);
  for (int i = 0; i < buffer_size; i++) {
    memcpy(Fr_val_k(buffer, i), vs_c + i, sizeof(blst_fr));
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_degree_stubs(value arg, value n) {
  CAMLparam2(arg, n);

  blst_fr *arg_c = Blst_fr_array_val(arg);
  int n_c = Int_val(n);
  int res = polynomial_degree(arg_c, n_c);
  CAMLreturn(Val_int(res));
}

CAMLprim value caml_polynomial_get_stubs(value res, value arg, value idx) {
  CAMLparam3(res, arg, idx);
  blst_fr *res_c = Blst_fr_val(res);
  blst_fr *arg_c = Blst_fr_array_val(arg);
  int idx_c = Int_val(idx);

  memcpy(res_c, arg_c + idx_c, sizeof(blst_fr));
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_copy_stubs(value buffer, value vs, value offset,
                                          value len) {
  CAMLparam4(buffer, vs, offset, len);

  blst_fr *vs_c = Blst_fr_array_val(vs);
  blst_fr *buffer_c = Blst_fr_array_val(buffer);
  int offset_c = Int_val(offset);
  int len_c = Int_val(len);
  for (int i = 0; i < len_c; i++) {
    memcpy(buffer_c + i, vs_c + offset_c + i, sizeof(blst_fr));
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_fft_inplace_on_stubs(value coefficients,
                                                    value domain,
                                                    value log_domain_size,
                                                    value degree,
                                                    value log_degree) {
  CAMLparam5(coefficients, domain, log_domain_size, degree, log_degree);

  int log_domain_size_c = Int_val(log_domain_size);
  int degree_c = Int_val(degree);
  int log_degree_c = Int_val(log_degree);
  blst_fr *domain_c = Blst_fr_array_val(domain);
  blst_fr *coefficients_c = Blst_fr_array_val(coefficients);
  polynomial_fft_inplace(coefficients_c, domain_c, log_domain_size_c, degree_c,
                         log_degree_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_ifft_inplace_on_stubs(value coefficients,
                                                     value domain,
                                                     value log_domain_size) {
  CAMLparam3(coefficients, domain, log_domain_size);

  int log_domain_size_c = Int_val(log_domain_size);
  blst_fr *domain_c = Blst_fr_array_val(domain);
  blst_fr *coefficients_c = Blst_fr_array_val(coefficients);
  polynomial_ifft_inplace(coefficients_c, domain_c, log_domain_size_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_of_sparse_stubs(value polynomial,
                                               value coefficients,
                                               value nb_coefficients) {
  CAMLparam3(polynomial, coefficients, nb_coefficients);

  blst_fr *polynomial_c = Blst_fr_array_val(polynomial);
  int nb_coefficients_c = Int_val(nb_coefficients);

  value idx_i;
  blst_fr *c;
  int d;
  for (int i = 0; i < nb_coefficients_c; i++) {
    idx_i = Field(coefficients, i);
    c = Blst_fr_val(Field(idx_i, 0));
    d = Int_val(Field(idx_i, 1));
    memcpy(polynomial_c + d, c, sizeof(blst_fr));
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_of_dense_stubs(value res, value coefficients,
                                              value nb_coefficients) {
  CAMLparam3(res, coefficients, nb_coefficients);

  blst_fr *res_c = Blst_fr_array_val(res);
  int nb_coefficients_c = Int_val(nb_coefficients);

  blst_fr *c;
  for (int i = 0; i < nb_coefficients_c; i++) {
    c = Blst_fr_val(Field(coefficients, i));
    memcpy(res_c + i, c, sizeof(blst_fr));
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_eq_stubs(value polynomial_1, value polynomial_2,
                                        value size_1, value size_2) {
  CAMLparam4(polynomial_1, polynomial_2, size_1, size_2);

  blst_fr *poly_1_c = Blst_fr_array_val(polynomial_1);
  int size_1_c = Int_val(size_1);
  blst_fr *poly_2_c = Blst_fr_array_val(polynomial_2);
  int size_2_c = Int_val(size_2);
  bool is_equal = polynomial_eq(poly_1_c, poly_2_c, size_1_c, size_2_c);
  CAMLreturn(Val_bool(is_equal));
}

CAMLprim value caml_polynomial_add_stubs(value res, value arg_1, value arg_2,
                                         value size_1, value size_2) {
  CAMLparam5(res, arg_1, arg_2, size_1, size_2);
  blst_fr *arg_1_c = Blst_fr_array_val(arg_1);
  blst_fr *arg_2_c = Blst_fr_array_val(arg_2);
  blst_fr *res_c = Blst_fr_array_val(res);
  int size_1_c = Int_val(size_1);
  int size_2_c = Int_val(size_2);

  polynomial_add(res_c, arg_1_c, arg_2_c, size_1_c, size_2_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_sub_stubs(value res, value arg_1, value arg_2,
                                         value size_1, value size_2) {
  CAMLparam5(res, arg_1, arg_2, size_1, size_2);
  blst_fr *arg_1_c = Blst_fr_array_val(arg_1);
  blst_fr *arg_2_c = Blst_fr_array_val(arg_2);
  blst_fr *res_c = Blst_fr_array_val(res);
  int size_1_c = Int_val(size_1);
  int size_2_c = Int_val(size_2);

  polynomial_sub(res_c, arg_1_c, arg_2_c, size_1_c, size_2_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_mul_stubs(value res, value arg_1, value arg_2,
                                         value size_1, value size_2) {
  CAMLparam5(res, arg_1, arg_2, size_1, size_2);
  blst_fr *arg_1_c = Blst_fr_array_val(arg_1);
  blst_fr *arg_2_c = Blst_fr_array_val(arg_2);
  blst_fr *res_c = Blst_fr_array_val(res);
  int size_1_c = Int_val(size_1);
  int size_2_c = Int_val(size_2);

  polynomial_mul(res_c, arg_1_c, arg_2_c, size_1_c, size_2_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_mul_by_scalar_stubs(value res, value scalar,
                                                   value arg, value size) {
  CAMLparam4(res, scalar, arg, size);
  blst_fr *res_c = Blst_fr_array_val(res);
  blst_fr *scalar_c = Blst_fr_val(scalar);
  blst_fr *arg_c = Blst_fr_array_val(arg);
  int size_c = Int_val(size);

  polynomial_mul_by_scalar(res_c, scalar_c, arg_c, size_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_linear_stubs(value res, value poly_polylen_coeff,
                                            value nb_polys) {
  CAMLparam3(res, poly_polylen_coeff, nb_polys);
  blst_fr *res_c = Blst_fr_array_val(res);
  int nb_polys_c = Int_val(nb_polys);

  blst_fr *polys_c[nb_polys_c];
  int polys_len_c[nb_polys_c];
  blst_fr linear_coeffs_c[nb_polys_c];

  value idx_i;
  blst_fr *c;
  for (int i = 0; i < nb_polys_c; i++) {
    idx_i = Field(poly_polylen_coeff, i);
    polys_c[i] = Blst_fr_array_val(Field(idx_i, 0));
    polys_len_c[i] = Int_val(Field(idx_i, 1));
    c = Blst_fr_val(Field(idx_i, 2));
    memcpy(linear_coeffs_c + i, c, sizeof(blst_fr));
  }

  polynomial_linear(res_c, polys_c, polys_len_c, linear_coeffs_c, nb_polys_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_linear_with_powers_stubs(value res, value coeff,
                                                        value poly_polylen,
                                                        value nb_polys) {
  CAMLparam4(res, coeff, poly_polylen, nb_polys);
  blst_fr *res_c = Blst_fr_array_val(res);
  int nb_polys_c = Int_val(nb_polys);
  blst_fr *coeff_c = Blst_fr_val(coeff);

  blst_fr *polys_c[nb_polys_c];
  int polys_len_c[nb_polys_c];

  value idx_i;
  for (int i = 0; i < nb_polys_c; i++) {
    idx_i = Field(poly_polylen, i);
    polys_c[i] = Blst_fr_array_val(Field(idx_i, 0));
    polys_len_c[i] = Int_val(Field(idx_i, 1));
  }

  polynomial_linear_with_powers(res_c, polys_c, polys_len_c, coeff_c,
                                nb_polys_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_negate_stubs(value res, value arg, value size) {
  CAMLparam3(res, arg, size);
  blst_fr *res_c = Blst_fr_array_val(res);
  blst_fr *arg_c = Blst_fr_array_val(arg);
  int size_c = Int_val(size);

  polynomial_negate(res_c, arg_c, size_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_evaluate_stubs(value res, value arg, value size,
                                              value scalar) {
  CAMLparam4(res, arg, size, scalar);
  blst_fr *res_c = Blst_fr_val(res);
  blst_fr *arg_c = Blst_fr_array_val(arg);
  int size_c = Int_val(size);
  blst_fr *scalar_c = Blst_fr_val(scalar);

  polynomial_evaluate(res_c, arg_c, size_c, scalar_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_is_zero_stubs(value poly, value size) {
  CAMLparam2(poly, size);
  blst_fr *poly_c = Blst_fr_array_val(poly);
  int size_c = Int_val(size);
  bool res = polynomial_is_zero(poly_c, size_c);
  CAMLreturn(Val_bool(res));
}

CAMLprim value caml_polynomial_division_xn_stubs(value res_q, value res_r,
                                                 value poly, value size,
                                                 value n_and_scalar) {
  CAMLparam5(res_q, res_r, poly, size, n_and_scalar);
  blst_fr *poly_c = Blst_fr_array_val(poly);
  blst_fr *res_q_c = Blst_fr_array_val(res_q);
  blst_fr *res_r_c = Blst_fr_array_val(res_r);
  int size_c = Int_val(size);
  int n_c = Int_val(Field(n_and_scalar, 0));
  blst_fr *scalar_c = Blst_fr_val(Field(n_and_scalar, 1));
  polynomial_division_xn(res_q_c, res_r_c, poly_c, size_c, n_c, scalar_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_mul_xn_stubs(value res, value poly, value size,
                                            value n, value scalar) {
  CAMLparam4(res, poly, size, n);
  blst_fr *poly_c = Blst_fr_array_val(poly);
  blst_fr *res_c = Blst_fr_array_val(res);
  int size_c = Int_val(size);
  int n_c = Int_val(n);
  blst_fr *scalar_c = Blst_fr_val(scalar);
  polynomial_mul_xn(res_c, poly_c, size_c, n_c, scalar_c);
  CAMLreturn(Val_unit);
}

// Bindings for evaluations.ml

CAMLprim value caml_polynomial_evaluations_add_stubs(value res, value eval_1,
                                                     value eval_2, value size_1,
                                                     value size_2) {
  CAMLparam5(res, eval_1, eval_2, size_1, size_2);
  blst_fr *res_c = Blst_fr_array_val(res);
  blst_fr *eval_1_c = Blst_fr_array_val(eval_1);
  blst_fr *eval_2_c = Blst_fr_array_val(eval_2);
  int size_1_c = Int_val(size_1);
  int size_2_c = Int_val(size_2);

  polynomial_evaluations_add(res_c, eval_1_c, eval_2_c, size_1_c, size_2_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_evaluations_rescale_stubs(value res, value eval,
                                                         value size_res,
                                                         value size_eval) {
  CAMLparam4(res, eval, size_res, size_eval);
  blst_fr *res_c = Blst_fr_array_val(res);
  blst_fr *eval_c = Blst_fr_array_val(eval);
  int size_res_c = Int_val(size_res);
  int size_eval_c = Int_val(size_eval);

  polynomial_evaluations_rescale(res_c, eval_c, size_res_c, size_eval_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_evaluations_mul_arrays_stubs(
    value res, value eval_evallen_comp_power_powlen, value size_res,
    value nb_evals) {
  CAMLparam4(res, eval_evallen_comp_power_powlen, size_res, nb_evals);
  blst_fr *res_c = Blst_fr_array_val(res);
  int size_res_c = Int_val(size_res);
  int nb_evals_c = Int_val(nb_evals);

  blst_fr *evaluations_c[nb_evals_c];
  int evaluations_len_c[nb_evals_c];
  int composition_gx_c[nb_evals_c];
  byte *powers_c[nb_evals_c];
  int powers_len_c[nb_evals_c];

  value idx_i;
  for (int i = 0; i < nb_evals_c; i++) {
    idx_i = Field(eval_evallen_comp_power_powlen, i);
    evaluations_c[i] = Blst_fr_array_val(Field(idx_i, 0));
    evaluations_len_c[i] = Int_val(Field(idx_i, 1));
    composition_gx_c[i] = Int_val(Field(idx_i, 2));
    powers_c[i] = Bytes_val(Field(idx_i, 3));
    powers_len_c[i] = Int_val(Field(idx_i, 4));
  }

  polynomial_evaluations_mul_arrays(res_c, evaluations_c, evaluations_len_c,
                                    composition_gx_c, powers_c, powers_len_c,
                                    size_res_c, nb_evals_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_evaluations_linear_arrays_stubs(
    value res, value eval_evallen_coeff_comp, value add_constant,
    value size_res, value nb_evals) {
  CAMLparam5(res, eval_evallen_coeff_comp, add_constant, size_res, nb_evals);
  blst_fr *res_c = Blst_fr_array_val(res);
  blst_fr *add_constant_c = Blst_fr_val(add_constant);
  int size_res_c = Int_val(size_res);
  int nb_evals_c = Int_val(nb_evals);

  blst_fr *evaluations_c[nb_evals_c];
  int evaluations_len_c[nb_evals_c];
  blst_fr linear_coeffs_c[nb_evals_c];
  int composition_gx_c[nb_evals_c];

  value idx_i;
  blst_fr *c;
  for (int i = 0; i < nb_evals_c; i++) {
    idx_i = Field(eval_evallen_coeff_comp, i);
    evaluations_c[i] = Blst_fr_array_val(Field(idx_i, 0));
    evaluations_len_c[i] = Int_val(Field(idx_i, 1));
    c = Blst_fr_val(Field(idx_i, 2));
    memcpy(linear_coeffs_c + i, c, sizeof(blst_fr));
    composition_gx_c[i] = Int_val(Field(idx_i, 3));
  }

  polynomial_evaluations_linear_arrays(res_c, evaluations_c, evaluations_len_c,
                                       linear_coeffs_c, composition_gx_c,
                                       add_constant_c, size_res_c, nb_evals_c);
  CAMLreturn(Val_unit);
}

#define Srs_val(v) (*(blst_p1_affine **)Data_custom_val(v))

static void finalize_srs(value v) { free(Srs_val(v)); }

static struct custom_operations srs_ops = {"srs",
                                           finalize_srs,
                                           custom_compare_default,
                                           custom_hash_default,
                                           custom_serialize_default,
                                           custom_deserialize_default,
                                           custom_compare_ext_default,
                                           custom_fixed_length_default};

CAMLprim value caml_polynomial_srs_allocate_stubs(value n) {
  CAMLparam1(n);
  CAMLlocal1(block);
  int n_c = Int_val(n);
  size_t size = sizeof(blst_p1_affine) * n_c;
  block = caml_alloc_custom_mem(&srs_ops, sizeof(blst_p1_affine *), size);
  blst_p1_affine *srs = (blst_p1_affine *)malloc(size);
  if (srs == NULL) {
    caml_raise_out_of_memory();
  }
  blst_p1_affine **caml_srs = (blst_p1_affine **)(Data_custom_val(block));
  *caml_srs = srs;
  CAMLreturn(block);
}

CAMLprim value caml_polynomial_srs_load_from_file_stubs(value srs, value path,
                                                        value n, value offset) {
  CAMLparam4(srs, path, n, offset);

  blst_p1_affine *srs_c = Srs_val(srs);
  const char *path_c = String_val(path);
  const int n_c = Int_val(n);
  // Must be the number of bytes we must skip, i.e. a multiple of 48. Not the
  // number of points we want to offset
  const int offset_c = Int_val(offset);

  // Fill the SRS from file
  FILE *stream;
  stream = fopen(path_c, "r");
  if (stream == NULL) {
    CAMLreturn(Val_int(0));
  }; /* false */
  fseek(stream, offset_c, SEEK_SET);
  byte tmp[48];
  int count;
  for (int i = 0; i < n_c; i++) {
    count = fread(&tmp, 48, 1, stream);
    if (count != 1) {
      CAMLreturn(Val_int(0));
    }; /* false */
    blst_p1_uncompress(srs_c + i, tmp);
  }
  fclose(stream);

  CAMLreturn(Val_int(1)); /* true */
}

CAMLprim value caml_polynomial_srs_of_array_stubs(value srs, value caml_array,
                                                  value size) {
  CAMLparam3(srs, caml_array, size);
  int size_c = Int_val(size);
  blst_p1_affine *srs_c = Srs_val(srs);
  for (int i = 0; i < size_c; i++) {
    blst_p1 *src = G1_val_k(caml_array, i);
    blst_p1_to_affine(srs_c + i, src);
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_srs_to_array_stubs(value caml_array, value srs,
                                                  value size) {
  CAMLparam3(caml_array, srs, size);
  int size_c = Int_val(size);
  blst_p1_affine *srs_c = Srs_val(srs);
  for (int i = 0; i < size_c; i++) {
    blst_p1_from_affine(G1_val_k(caml_array, i), srs_c + i);
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_srs_get_stubs(value res, value srs, value ind) {
  CAMLparam3(res, srs, ind);
  int ind_c = Int_val(ind);
  blst_p1_affine *srs_c = Srs_val(srs);
  blst_p1_from_affine(Blst_p1_val(res), srs_c + ind_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_pippenger_stubs(value res, value srs, value poly,
                                               value start, value len) {
  CAMLparam5(res, srs, poly, start, len);
  int start_c = Int_val(start);
  int len_c = Int_val(len);
  blst_fr *poly_c = Blst_fr_array_val(poly) + start_c;
  const blst_p1_affine *srs_c = Srs_val(srs) + start_c;

  byte *bs_c = (byte *)malloc(sizeof(byte[32]) * len_c);
  if (bs_c == NULL) {
    caml_raise_out_of_memory();
  }
  for (int i = 0; i < len_c; i++) {
    // bs_c[i] points to the ith element of the contiguous C array of bytes of
    // size ctxt_size. See caml_polynomial_allocate_pippenger_ctxt_stubs
    blst_lendian_from_fr(bs_c + (32 * i), poly_c + i);
  }

  // blst does not support pippenger for 1 element
  if (len_c == 1) {
    blst_p1 tmp;
    blst_p1_from_affine(&tmp, &srs_c[0]);
    blst_p1_mult(Blst_p1_val(res), &tmp, &bs_c[0], 256);
  } else {
    limb_t *scratch = calloc(1, blst_p1s_mult_pippenger_scratch_sizeof(len_c));
    if (scratch == NULL) {
      free(bs_c);
      caml_raise_out_of_memory();
    }

    blst_p1s_mult_pippenger_cont(Blst_p1_val(res), srs_c, len_c,
                                 (const byte *)bs_c, 256, scratch);
    free(scratch);
  }
  free(bs_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_polynomial_pippenger_stubs_bytecode(value *argv, int argn) {
  return caml_polynomial_pippenger_stubs(argv[0], argv[1], argv[2], argv[3],
                                         argv[4]);
}

CAMLprim value caml_polynomial_derivative_stubs(value res, value poly,
                                                value size) {
  CAMLparam3(res, poly, size);
  blst_fr *poly_c = Blst_fr_array_val(poly);
  blst_fr *res_c = Blst_fr_array_val(res);
  int size_c = Int_val(size);
  polynomial_derivative(res_c, poly_c, size_c);
  CAMLreturn(Val_unit);
}
