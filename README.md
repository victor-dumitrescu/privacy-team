To setup correctly your local environment run the script `./scripts/install_build_deps.sh` which will take care of creating a local opam directory `_opam` and installing:
- the right Rust version
- the right OCaml version
- the system dependencies
- opam build and test dependencies.

The CI uses the same script so it's easier to make sure that it matches your local setup.
The CI's first job launches the installation script and then caches the resulting `_opam`. Following jobs will then download the cache and run.
The first job exists immediately if it detects that the cache is already populated.

The cache can be cleared clicking "Clear runner caches" under pipelines, and it's important to do so if any `*.opam` files or `.gitlab-ci.yml` have changed.
A pipeline w/o cache takes 18 minutes vs 5 minutes for one with cache.

The cache contains also the dune directory `_build` so that it is shared between branches and MRs to avoid re-building code or re-running tests that haven't changed.
