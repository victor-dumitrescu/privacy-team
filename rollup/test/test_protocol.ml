open Rollup
open Rollup.Protocol
module T = Tx_rollup.P
module HashPV = Plompiler.Poseidon128
module SchnorrPV = Plompiler.Schnorr (HashPV)
module Schnorr = SchnorrPV.P

let accounts = Array.init 5 (fun _ -> Protocol.Sig.gen ())
let sks = Array.map (fun (_, _, sk) -> sk) accounts
let pks = Array.map (fun (_, pk, _) -> pk) accounts
let pkhs = Array.map (fun (pkh, _, _) -> pkh) accounts
let _ = Array.iter (fun pkh -> Printf.printf "pkh: %d\n" pkh) pkhs

module type User = sig
  val id : int

  (* val pos : Z.t *)

  val next_cnt : unit -> Z.t
  val l1_pk : Protocol.Sig.pk
  val l1_pkh : Protocol.Sig.pkh
  val l1_sk : Protocol.Sig.sk
  val l2_pk : Schnorr.pk
  val l2_sk : Schnorr.sk
end

module UserMk (Id : sig
  val id : int
end) : User = struct
  let cnt = ref 0
  let id = Id.id
  let l1_pk = pks.(id)
  let l1_pkh = pkhs.(id)
  let l1_sk = sks.(id)
  let l2_sk = T.sks.(id)
  let l2_pk = Schnorr.neuterize l2_sk

  (* let pos = Z.of_int id *)

  let next_cnt () =
    cnt := !cnt + 1;
    Z.of_int !cnt
end

module type DefaultUsers = sig
  module UserOp : User
  module User1 : User
  module User2 : User
  module User3 : User
end

module MakeUsers () : DefaultUsers = struct
  module UserOp = UserMk (struct
    let id = 0
  end)

  module User1 = UserMk (struct
    let id = 1
  end)

  module User2 = UserMk (struct
    let id = 2
  end)

  module User3 = UserMk (struct
    let id = 3
  end)
end

module type TICKET = sig
  val c_id : Bls12_381.Fr.t
  val p_id : bytes

  val c_of_bounded :
    'a Tx_rollup.Types.P.Bounded.t -> 'a Tx_rollup.Types.P.ticket

  val c_amount : int -> Tx_rollup.Constants.amount Tx_rollup.Types.P.ticket
  val c_bal : int -> Tx_rollup.Constants.balance Tx_rollup.Types.P.ticket
  val p : int -> ticket
end

module TicketMk (Id : sig
  val c_id : Bls12_381.Fr.t
end) : TICKET = struct
  include Id

  let p_id =
    Data_encoding.Binary.to_bytes_exn Protocol.scalar_data_encoding c_id

  let c_of_bounded amount : 'a Tx_rollup.Types.P.ticket = { id = c_id; amount }

  let c_amount amount : 'a Tx_rollup.Types.P.ticket =
    {
      id = c_id;
      amount =
        Tx_rollup.(
          Types.P.Bounded.make ~unsafe:true ~bound:Constants.Bound.max_amount
            (Z.of_int amount));
    }

  let c_bal amount : 'a Tx_rollup.Types.P.ticket =
    {
      id = c_id;
      amount =
        Tx_rollup.(
          Types.P.Bounded.make ~unsafe:true ~bound:Constants.Bound.max_balance
            (Z.of_int amount));
    }

  let p x : ticket = { id = p_id; amount = Int32.of_int x }
end

module Tez = TicketMk (struct
  let c_id = Tx_rollup.Constants.tez_id
end)

module Blue = TicketMk (struct
  let c_id = Bls12_381.Fr.one
end)

module Red = TicketMk (struct
  let c_id = Bls12_381.Fr.of_int 2
end)

let pos_of_index ?(offset = 0) index =
  Z.of_int @@ ((Tx_rollup.Constants.max_nb_tickets * index) + offset)

let dummy_pk = Protocol.Sig.dummy_pk
(* let open Data_encoding in
   (* let open Tezos_crypto.Ed25519 in *)
   let size = Binary.fixed_length Public_key.encoding |> Option.get in
   Binary.of_bytes_exn Public_key.encoding (Bytes.init size (fun _ -> '\x00')) *)

let make_transfer ?(unsafe = false) ~(src : (module User))
    ~(dst : (module User)) ~amount ~fee ~rollup_id () =
  let module Src = (val src) in
  let module Dst = (val dst) in
  let module Bounded = Tx_rollup.Types.P.Bounded in
  let open Tx_rollup.Constants.Bound in
  let open Tx_rollup.Types.P in
  let offset = Z.to_int @@ Bls12_381.Fr.to_z amount.id in
  let src_pos =
    Bounded.make ~unsafe ~bound:max_nb_leaves (pos_of_index ~offset Src.id)
  in
  let dst_pos =
    Bounded.make ~unsafe ~bound:max_nb_leaves (pos_of_index ~offset Dst.id)
  in
  let fee = Bounded.make ~unsafe ~bound:max_fee fee in
  let cnt = Bounded.make ~unsafe ~bound:max_counter (Src.next_cnt ()) in
  let unsigned_payload : unsigned_transfer_payload =
    { cnt; src = src_pos; dst = dst_pos; amount; fee }
  in
  let header =
    {
      op_code = Bounded.make ~unsafe ~bound:max_op_code Z.zero;
      price = Tez.c_of_bounded @@ Bounded.make ~unsafe ~bound:max_balance Z.zero;
      l1_dst =
        Data_encoding.Binary.to_bytes_exn Protocol.Sig.pkh_encoding Src.l1_pkh;
      rollup_id;
    }
  in
  let unsigned_op : unsigned_transfer =
    { header; payload = unsigned_payload }
  in
  Operator.(to_protocol_tx (sign_op Src.l2_sk (Transfer unsigned_op)))

let make_create ?(unsafe = false) ~(dst : (module User)) ~fee ~rollup_id () =
  let module Dst = (val dst) in
  let module Bounded = Tx_rollup.Types.P.Bounded in
  let open Tx_rollup.Constants.Bound in
  let open Tx_rollup.Types.P in
  let fee = Bounded.make ~unsafe ~bound:max_fee fee in
  let unsigned_payload : unsigned_create_payload = { pk = Dst.l2_pk; fee } in
  let header =
    {
      op_code = Bounded.make ~unsafe ~bound:max_op_code Z.one;
      price = Tez.c_of_bounded Bounded.(make ~bound:max_balance (v fee));
      l1_dst =
        Data_encoding.Binary.to_bytes_exn Protocol.Sig.pkh_encoding Dst.l1_pkh;
      rollup_id;
    }
  in
  let unsigned_op : unsigned_create = { header; payload = unsigned_payload } in
  Operator.(to_protocol_tx (sign_op Dst.l2_sk (Create unsigned_op)))

let make_credit ?(unsafe = false) ~(dst : (module User)) ~amount ~rollup_id () =
  let module Dst = (val dst) in
  let module Bounded = Tx_rollup.Types.P.Bounded in
  let open Tx_rollup.Constants.Bound in
  let open Tx_rollup.Types.P in
  let offset = Z.to_int @@ Bls12_381.Fr.to_z amount.id in
  let dst_pos =
    Bounded.make ~unsafe ~bound:max_nb_leaves (pos_of_index ~offset Dst.id)
  in
  let cnt = Bounded.make ~unsafe ~bound:max_counter (Dst.next_cnt ()) in
  let payload : credit_payload = { cnt; dst = dst_pos; amount } in
  let header =
    {
      op_code = Bounded.make ~unsafe ~bound:max_op_code Z.(of_int 2);
      price =
        {
          id = amount.id;
          amount = Bounded.(make ~unsafe ~bound:max_balance (v amount.amount));
        };
      l1_dst =
        Data_encoding.Binary.to_bytes_exn Protocol.Sig.pkh_encoding Dst.l1_pkh;
      rollup_id;
    }
  in
  let op : credit = { header; payload } in
  Operator.(to_protocol_tx (Credit op))

let make_debit ?(unsafe = false) ~(src : (module User)) ~amount ~rollup_id () =
  let module Src = (val src) in
  let module Bounded = Tx_rollup.Types.P.Bounded in
  let open Tx_rollup.Constants.Bound in
  let open Tx_rollup.Types.P in
  let offset = Z.to_int @@ Bls12_381.Fr.to_z amount.id in
  let src_pos =
    Bounded.make ~unsafe ~bound:max_nb_leaves (pos_of_index ~offset Src.id)
  in
  let cnt = Bounded.make ~unsafe ~bound:max_counter (Src.next_cnt ()) in
  let unsigned_payload : unsigned_debit_payload =
    { cnt; src = src_pos; amount }
  in
  let header =
    {
      op_code = Bounded.make ~unsafe ~bound:max_op_code Z.(of_int 3);
      price =
        {
          id = amount.id;
          amount = Bounded.(make ~unsafe ~bound:max_balance (v amount.amount));
        };
      l1_dst =
        Data_encoding.Binary.to_bytes_exn Protocol.Sig.pkh_encoding Src.l1_pkh;
      rollup_id;
    }
  in
  let unsigned_op : unsigned_debit = { header; payload = unsigned_payload } in
  Operator.(to_protocol_tx (sign_op Src.l2_sk (Debit unsigned_op)))

let empty_ctx =
  let open Protocol in
  let accounts = Accounts.empty in
  let rollup = None in
  { accounts; rollup }

let empty_ctx_state =
  let open Operator in
  let ctx = Protocol.create empty_ctx public_parameters ~init_state in
  let state = Operator.init in
  (ctx, state)

(*
   Tez:
   ctx = {pki -> 1000} for i in [0,3]
   state = {pk0 -> 0, pk1 -> 100, pk2 -> 100}

   Tickets:
   ctx = {pk1 -> 100 blue; pk2 -> 100 red}
*)
let initial_ctx_state : Protocol.ctx * Operator.state =
  let module Users = MakeUsers () in
  let open Users in
  let open Operator in
  let ctx =
    Option.get
    @@ Protocol.create empty_ctx public_parameters ~circuits:Operator.circuits
         ~init_state ~fee:Z.zero ~nb_ops:4 UserOp.l1_pk
  in
  let ctx =
    List.fold_left
      (fun ctx usr ->
        let module U = (val usr : User) in
        Protocol.credit U.l1_pk (Z.of_int 1000) ctx)
      ctx
      [
        (module UserOp : User);
        (module User1 : User);
        (module User2 : User);
        (module User3 : User);
      ]
  in
  let ctx = Protocol.credit User1.l1_pk ~id:Blue.p_id (Z.of_int 100) ctx in
  let ctx = Protocol.credit User2.l1_pk ~id:Red.p_id (Z.of_int 100) ctx in

  let state =
    T.make_state Z.[ (of_int 0, [||]); (of_int 100, [||]); (of_int 100, [||]) ]
  in
  let root = Operator.Merkle.root state.accounts_tree in
  let proto_state =
    Protocol.{ root; next_position = 3 * Tx_rollup.Constants.max_nb_tickets }
  in
  let proto_state_bytes =
    Data_encoding.Binary.to_bytes_exn Protocol.state_data_encoding proto_state
  in
  let ctx =
    let rollup = Option.get ctx.rollup in
    Protocol.
      { ctx with rollup = Some { rollup with state = proto_state_bytes } }
  in
  (ctx, state)

let get_ru ctx = Option.get ctx.rollup

type c_ticket = Tx_rollup.Constants.balance Tx_rollup.Types.P.ticket

let assert_ctx_s (ctx : ctx) (s : Tx_rollup.Types.P.state)
    (users :
      ((module User) * int * ticket list * (int * c_ticket list) option) list) =
  let to_proto_state s : Protocol.state =
    let Tx_rollup.Types.P.{ accounts_tree; next_position; _ } = s in
    let root = Operator.Merkle.root accounts_tree in
    Protocol.{ root; next_position }
  in
  assert (
    (get_ru ctx).state
    = Data_encoding.Binary.to_bytes_exn Protocol.state_data_encoding
        (to_proto_state s));
  let module Bounded = Tx_rollup.Types.P.Bounded in
  let check_one ((usr : (module User)), l1_tez_bal, l1_tickets, l2_bals) =
    let module U = (val usr) in
    let check_l1_bal id expected =
      let l1acc = Protocol.get_balance ~id U.l1_pk ctx in
      if Z.(not (l1acc = of_int32 expected)) then
        failwith
        @@ Printf.sprintf
             "User %d has an L1 balance for token %s of %d, expected %d" U.id
             (Bls12_381.Fr.to_string
             @@ Data_encoding.Binary.of_bytes_exn Protocol.scalar_data_encoding
                  id)
             (Z.to_int @@ l1acc) (Int32.to_int expected)
    in
    check_l1_bal Tez.p_id (Int32.of_int l1_tez_bal);
    List.iter (fun t -> check_l1_bal t.id t.amount) l1_tickets;
    match (Tx_rollup.Types.P.IMap.find_opt U.id s.accounts, l2_bals) with
    | Some (l2acc, _, _), None when l2acc.pk <> Schnorr.g ->
        failwith
        @@ Printf.sprintf
             "User %d has an L2 account with a tez balance of %d, but it \
              shouldn't have an L2 account"
             U.id
             (Z.to_int @@ Bounded.v l2acc.tez_balance)
    | None, Some (l2_bal, _) ->
        failwith
        @@ Printf.sprintf
             "User %d does not have an L2 account, but it should have one with \
              a tez balance of %d"
             U.id l2_bal
    | Some (l2acc, leaves, _), Some (l2_tez_bal, l2_tickets) ->
        if Z.(not (Bounded.v l2acc.tez_balance = of_int l2_tez_bal)) then
          failwith
          @@ Printf.sprintf "User %d has an L2 tez balance of %d, expected %d"
               U.id
               (Z.to_int @@ Bounded.v l2acc.tez_balance)
               l2_tez_bal;
        let check_l2_ticket (t : c_ticket) =
          let i = Z.to_int @@ Bls12_381.Fr.to_z t.id in
          let bal = leaves.(i) in
          if not (Bounded.v t.amount = Bounded.v bal.ticket.amount) then
            failwith
            @@ Printf.sprintf
                 "User %d has an L2 tez balance for ticketd %d of %d, expected \
                  %d"
                 U.id i
                 (Z.to_int @@ Bounded.v bal.ticket.amount)
                 (Z.to_int @@ Bounded.v t.amount)
        in
        List.iter check_l2_ticket l2_tickets
    | _ -> ()
  in
  List.fold_left (fun () -> check_one) () users

let hash_pk pk =
  Protocol.Sig.(Data_encoding.Binary.to_bytes_exn pkh_encoding @@ hash_pk pk)

let assert_initial ctx s =
  let module Users = MakeUsers () in
  let open Users in
  assert_ctx_s ctx s
    [
      ((module User1), 1000, [ Blue.p 100 ], Some (100, []));
      ((module User2), 1000, [ Red.p 100 ], Some (100, []));
      ((module User3), 1000, [], None);
    ]

let () =
  let module Users = MakeUsers () in
  let ctx, s = initial_ctx_state in
  assert_initial ctx s

let test_create () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in

  let tx1 =
    make_create
      ~dst:(module User3)
      ~fee:Z.(of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 5) ~op:tx1 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (5, []));
      ((module User1), 1000, [], Some (100, []));
      ((module User2), 1000, [], Some (100, []));
      ((module User3), 995, [], Some (0, []));
    ];
  Some ()

let test_transfer () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in

  let tx1 =
    make_transfer
      ~src:(module User1)
      ~dst:(module User2)
      ~amount:(Tez.c_amount 50) ~fee:(Z.of_int 10)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let tx2 =
    make_transfer
      ~src:(module User2)
      ~dst:(module User1)
      ~amount:(Tez.c_amount 20) ~fee:(Z.of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User1.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx1 ~fee:Z.zero
  in
  let* ctx =
    Protocol.append ~ctx ~src:User2.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx2 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk (get_ru ctx)
  in

  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (15, []));
      ((module User1), 1000, [], Some (60, []));
      ((module User2), 1000, [], Some (125, []));
      ((module User3), 1000, [], None);
    ];
  Some ()

let test_inval_transfer_fees () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in

  let tx1 =
    make_transfer
      ~src:(module User1)
      ~dst:(module User2)
      ~amount:(Tez.c_amount 100) ~fee:(Z.of_int 10)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User1.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx1 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_initial ctx s;
  Some ()

let test_credit () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in

  let tx1 =
    make_credit
      ~dst:(module User2)
      ~amount:(Tez.c_amount 50)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User2.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 50) ~op:tx1 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (0, []));
      ((module User1), 1000, [], Some (100, []));
      ((module User2), 950, [], Some (150, []));
      ((module User3), 1000, [], None);
    ];
  Some ()

let test_credit_ticket () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in

  let tx1 =
    make_credit
      ~dst:(module User1)
      ~amount:(Blue.c_amount 50)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User1.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Blue.p 50) ~op:tx1 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (0, []));
      ((module User1), 1000, [ Blue.p 50 ], Some (100, [ Blue.c_bal 50 ]));
      ((module User2), 1000, [ Red.p 100 ], Some (100, []));
      ((module User3), 1000, [], None);
    ];
  Some ()

let test_debit () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in
  let tx1 =
    make_debit
      ~src:(module User2)
      ~amount:(Tez.c_amount 50)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User2.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx1 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (0, []));
      ((module User1), 1000, [], Some (100, []));
      ((module User2), 1050, [], Some (50, []));
      ((module User3), 1000, [], None);
    ];
  Some ()

let test_create_credit_transfer () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in

  let tx1 =
    make_create
      ~dst:(module User3)
      ~fee:Z.(of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let tx2 =
    make_credit
      ~dst:(module User3)
      ~amount:(Tez.c_amount 50)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let tx3 =
    make_transfer
      ~src:(module User3)
      ~dst:(module User1)
      ~amount:(Tez.c_amount 20) ~fee:(Z.of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 5) ~op:tx1 ~fee:Z.zero
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 50) ~op:tx2 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk ~private_ops:[ [ tx3 ] ] (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (10, []));
      ((module User1), 1000, [], Some (120, []));
      ((module User2), 1000, [], Some (100, []));
      ((module User3), 945, [], Some (25, []));
    ];
  Some ()

let test_all_ticket () =
  (*
  Initial ctx state
  Tez:
    ctx = {pki -> 1000} for i in [0,3]
    state = {pk0 -> 0, pk1 -> 100, pk2 -> 100}

  Tickets:
    ctx = {pk1 -> 100 blue; pk2 -> 100 red}
  *)
  (*
     Ops:
     - create U3
     - credit U2 with 50 red
     - transfer form U2 to U3 50 red
     - debit from U3 50 red
  *)
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in
  let rollup_id = hash_pk (get_ru ctx).address in

  let tx1 = make_create ~dst:(module User3) ~fee:Z.(of_int 5) ~rollup_id () in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 5) ~op:tx1 ~fee:Z.zero
  in
  let tx2 =
    make_credit ~dst:(module User2) ~amount:(Red.c_amount 50) ~rollup_id ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User2.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Red.p 50) ~op:tx2 ~fee:Z.zero
  in
  (* This will be published to preserve order, so the fee is 0 *)
  let tx3 =
    make_transfer
      ~src:(module User2)
      ~dst:(module User3)
      ~amount:(Red.c_amount 50) ~fee:Z.zero ~rollup_id ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User2.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Red.p 0) ~op:tx3 ~fee:Z.zero
  in
  let tx4 =
    make_debit ~src:(module User3) ~amount:(Red.c_amount 50) ~rollup_id ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Red.p 0) ~op:tx4 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk ~private_ops:[] (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (5, []));
      ((module User1), 1000, [ Blue.p 100 ], Some (100, []));
      ((module User2), 1000, [ Red.p 50 ], Some (100, [ Red.c_bal 0 ]));
      ((module User3), 995, [ Red.p 50 ], Some (0, [ Red.c_bal 0 ]));
    ];
  Some ()

let test_genesis () =
  let ( let* ) = Option.bind in
  let open MakeUsers () in
  let open Operator in
  let ctx =
    Option.get
    @@ Protocol.create empty_ctx public_parameters ~circuits:Operator.circuits
         ~init_state ~fee:Z.zero ~nb_ops:4 UserOp.l1_pk
  in

  let ctx = Protocol.credit UserOp.l1_pk (Z.of_int 5) ctx in
  let ctx = Protocol.credit User1.l1_pk (Z.of_int 1000) ctx in
  let ctx = Protocol.credit User2.l1_pk (Z.of_int 5) ctx in
  let s = T.empty_state () in
  let tx1 =
    make_create
      ~dst:(module UserOp)
      ~fee:(Z.of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:UserOp.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 5) ~op:tx1 ~fee:Z.zero
  in
  let tx2 =
    make_create
      ~dst:(module User1)
      ~fee:(Z.of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User1.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 5) ~op:tx2 ~fee:Z.zero
  in
  let tx3 =
    make_create
      ~dst:(module User2)
      ~fee:(Z.of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User2.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 5) ~op:tx3 ~fee:Z.zero
  in
  let tx4 =
    make_credit
      ~dst:(module User1)
      ~amount:(Tez.c_amount 50)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User1.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 50) ~op:tx4 ~fee:Z.zero
  in
  let tx5 =
    make_transfer
      ~src:(module User1)
      ~dst:(module User2)
      ~amount:(Tez.c_amount 20) ~fee:(Z.of_int 5)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User1.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx5 ~fee:Z.zero
  in
  let tx6 =
    make_debit
      ~src:(module User2)
      ~amount:(Tez.c_amount 20)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User2.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx6 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 0, [], Some (20, []));
      ((module User1), 945, [], Some (25, []));
      ((module User2), 20, [], Some (0, []));
      ((module User3), 0, [], None);
    ];
  Some ()

let test_ill_formed () =
  let open Tx_rollup.Constants.Bound in
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in
  let ctx = Protocol.credit User3.l1_pk Z.(v max_balance * of_int 5) ctx in

  let tx1 =
    make_create ~unsafe:true
      ~dst:(module User3)
      ~fee:(v max_fee)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let tx2 =
    make_credit ~unsafe:true
      ~dst:(module User3)
      ~amount:(Tez.c_amount @@ Z.to_int @@ v max_amount)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let tx3 =
    make_transfer ~unsafe:true
      ~src:(module User3)
      ~dst:(module User1)
      ~amount:(Tez.c_amount 20) ~fee:(v max_fee)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let tx4 =
    make_debit ~unsafe:true
      ~src:(module User3)
      ~amount:(Tez.c_amount @@ Z.to_int @@ v max_amount)
      ~rollup_id:(hash_pk (get_ru ctx).address)
      ()
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p @@ Z.to_int @@ v max_fee)
      ~op:tx1 ~fee:Z.zero
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p @@ Z.to_int @@ v max_amount)
      ~op:tx2 ~fee:Z.zero
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx3 ~fee:Z.zero
  in
  let* ctx =
    Protocol.append ~ctx ~src:User3.l1_pk ~ru:(get_ru ctx).address
      ~ticket:(Tez.p 0) ~op:tx4 ~fee:Z.zero
  in
  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk ~private_ops:[] (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (0, []));
      ((module User1), 1000, [], Some (100, []));
      ((module User2), 1000, [], Some (100, []));
      ((module User3), (Z.(to_int @@ v max_balance) * 5) + 1000, [], None);
    ];
  Some ()

let test_inner_batch () =
  let module Users = MakeUsers () in
  let open Users in
  let ( let* ) = Option.bind in
  let ctx, s = initial_ctx_state in

  (* Number of ops in an inner batch *)
  let n = 1 in
  (* Number of inner batches in the block *)
  let m = 1 in

  let txs =
    List.init m (fun _ ->
        List.init n (fun _ ->
            make_transfer
              ~src:(module User1)
              ~dst:(module User2)
              ~amount:(Tez.c_amount 1) ~fee:(Z.of_int 0)
              ~rollup_id:(hash_pk (get_ru ctx).address)
              ()))
  in

  let data, s =
    Operator.craft_update s ~operator_index:UserOp.id
      ~operator_l1pk:UserOp.l1_pk ~private_ops:txs (get_ru ctx)
  in
  let* ctx = Protocol.update ctx data ~fee:Z.zero in
  assert_ctx_s ctx s
    [
      ((module UserOp), 1000, [], Some (0, []));
      ((module User1), 1000, [], Some (100 - (n * m), []));
      ((module User2), 1000, [], Some (100 + (n * m), []));
      ((module User3), 1000, [], None);
    ];
  Some ()

(*

let test_create_valid () =
  let ( let* ) = Option.bind in
  let open MakeUsers () in
  (* We start with a context where the user 1 (pks.(1)) has 10 tokens *)
  let (ctx, s) = initial_ctx_state in
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 10) ;
  (* And a state where the validator has 0 tokens *)
  assert ((Circuit.Context.get state 0).el.balance = 0) ;
  (* User 1 enters the RU through an append *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 1 (User1.enter 1)
  >>= fun ctx ->
  (* After the append, the user one is left with 9 tokens,
     while the RU now has 1 *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  (* We craft and perform the update for the create op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged, as the operation should
     have been valid *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  (* In the RU's state, the validator gets the fee, and user 1 has
     entered with a balance of 0 *)
  assert ((Circuit.Context.get state 0).el.balance = 1) ;
  assert ((Circuit.Context.get state 1).el.balance = 0) ;
  Some ()


let should_fail = function Some _ -> None | None -> Some ()

(* Create with insufficient tokens for the fee *)
let test_create_invalid1 () =
  let ( >>= ) = Option.bind in
  initial_ctx_state >>= fun (ctx, _state) ->
  (* We start with a context where the user 1 (pks.(1)) has 10 tokens *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 10) ;
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 11 (User1.enter 11)
  >>= fun _ctx -> Some ()

(* Create with invalid signature *)
let test_create_invalid2 () =
  let ( >>= ) = Option.bind in
  initial_ctx_state >>= fun (ctx, state) ->
  (* We start with a context where the user 1 (pks.(1)) has 10 tokens *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 10) ;
  let op = Circuit.Clear.Create {pk = pks.(1); fee = 1} in
  let signature =
    Crypto.Signature.sign
      sks.(0) (* Use incorrect secret key to sign. *)
      ~msg:(Circuit.Clear.unsigned_operation_to_bytes op)
  in
  let c = Circuit.Clear.operation_to_bytes (op, signature) in
  (* User 1 enters the RU through an append but with an
     invalid signature. *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 1 c >>= fun ctx ->
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  (* We craft and perform the update for the create op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* As the operation is invalid, the exits should be executed.
     We check that the context is reverted and the state is unchanged. *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 10) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 0) ;
  assert ((Circuit.Context.get state 0).el.balance = 0) ;
  Some ()

(* Context-state pair where user 1 already entered the RU *)
let entered_ctx_state =
  let ( >>= ) = Option.bind in
  initial_ctx_state >>= fun (ctx, state) ->
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 1 (User1.enter 1)
  >>= fun ctx ->
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  Some (ctx, state)

let test_credit_valid () =
  let ( >>= ) = Option.bind in
  entered_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  (* We append a credit operation to user 1 of 5 tokens *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 5 (User1.credit 5)
  >>= fun ctx ->
  (* After the append, user 1 is left with 5 tokens on chain and the
     RU now has 6 *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  (* We craft and perform the update for the credit op *)
  let (pi, proof, _state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged, as the operation should
     have been valid *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  Some ()

(* Invalid credit operation: not enough tokens on chain *)
let test_credit_invalid1 () =
  let ( >>= ) = Option.bind in
  entered_ctx_state >>= fun (ctx, _state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  (* We append a credit operation to user 1 of 11 tokens *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 11 (User1.credit 11)
  >>= fun _ctx ->
  (* Should fail *)
  Some ()

(* Credit invalid: replay *)
let test_credit_invalid2 () =
  let ( >>= ) = Option.bind in
  entered_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  (* We append a credit operation to user 1 of 2 tokens *)
  let c = User1.credit 2 in
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 2 c >>= fun ctx ->
  (* After the append, user 1 is left with 7 tokens on chain and the
     RU now has 3 *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 7) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 3) ;
  (* We craft and perform the update for the credit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged, as the operation should
     have been valid *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 7) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 2) ;
  (* Start of replay *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 2 c >>= fun ctx ->
  (* After the append, user 1 is left with 6 tokens on chain and the
     RU now has 5, this should be reverted in the update *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 5) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 5) ;
  (* We craft and perform the update for the credit op *)
  let (pi, proof, _state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is reverted, as the operation should
     have been invalid *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 7) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 2) ;
  Some ()

(* Valid credit from another user *)
let test_credit_valid3 () =
  let ( >>= ) = Option.bind in
  entered_ctx_state >>= fun (ctx, state) ->
  (* We start with a context where the user 1 (pks.(1)) has 9 tokens *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 10) ;
  let op = Circuit.Clear.Credit {cnt = 1; dst = 1; amount = 2} in
  let signature =
    Crypto.Signature.sign
      sks.(2) (* Use incorrect secret key to sign. *)
      ~msg:(Circuit.Clear.unsigned_operation_to_bytes op)
  in
  let c = Circuit.Clear.operation_to_bytes (op, signature) in
  (* User 2 appends a credit op for user 1 *)
  Protocol.append ctx pks.(2) ctx.(get_ru ctx).address 2 c >>= fun ctx ->
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 8) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 3) ;
  (* We craft and perform the update for the create op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* As the operation is invalid, the exits should be executed.
     Thus, the context is reverted *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 8) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 2) ;
  Some ()

(* Credit invalid: dst doesn't exist *)
let test_credit_invalid4 () =
  let ( >>= ) = Option.bind in
  entered_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 10) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  (* We append a credit operation to user 2 of 5 tokens *)
  (* User 2 hasn't entered the RU, so it should fail *)
  Protocol.append ctx pks.(2) ctx.(get_ru ctx).address 5 (User2.credit 5)
  >>= fun ctx ->
  (* After the append, user 2 has 5 tokens in the context.Data_encoding
     This should be reverted by the update. *)
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 5) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  (* We craft and perform the update for the credit op *)
  let (pi, proof, _state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is back to the original state, as the
     operation was invalid. *)
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 10) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 1) ;
  Some ()

(* Context-state pair where user 1 already entered the RU and has 5 tokens *)
let credited_ctx_state =
  let ( >>= ) = Option.bind in
  initial_ctx_state >>= fun (ctx, state) ->
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 1 (User1.enter 1)
  >>= fun ctx ->
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 5 (User1.credit 5)
  >>= fun ctx ->
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  Some (ctx, state)

let test_debit_valid () =
  let ( >>= ) = Option.bind in
  credited_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  (* We append a debit operation from user 1 of 2 tokens *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 (User1.debit 2)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the debit is performed
     after the update *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  (* We craft and perform the update for the debit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the debit has been performed *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 6) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 4) ;
  assert ((Circuit.Context.get state 1).el.balance = 3) ;
  Some ()

(* Invalid debit operation: not enough tokens on RU *)
let test_debit_invalid1 () =
  let ( >>= ) = Option.bind in
  credited_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  (* We append a debit operation from user 1 of 6 tokens *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 (User1.debit 6)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the debit is performed
     after the update *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  (* We craft and perform the update for the debit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged, as the debit should be invalid *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  Some ()

(* Debit invalid: replay *)
let test_debit_invalid2 () =
  let ( >>= ) = Option.bind in
  credited_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  (* We append a credit operation to user 1 of 2 tokens *)
  let d = User1.debit 2 in
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 d >>= fun ctx ->
  (* After the append, ctx is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  (* We craft and perform the update for the debit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the debit is reflected in the context after the update *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 6) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 4) ;
  assert ((Circuit.Context.get state 1).el.balance = 3) ;
  (* Start of replay *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 d >>= fun ctx ->
  (* After the append, ctx is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 6) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 4) ;
  (* We craft and perform the update for the debit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged, as the operation should
     have been invalid *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 6) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 4) ;
  assert ((Circuit.Context.get state 1).el.balance = 3) ;
  Some ()

(* Debit with invalid signature *)
let test_debit_invalid3 () =
  let ( >>= ) = Option.bind in
  credited_ctx_state >>= fun (ctx, state) ->
  (* We start with a context where the user 1 (pks.(1)) has 4 tokens *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  let op = Circuit.Clear.Debit {cnt = 1; src = 1; amount = 3} in
  let signature =
    Crypto.Signature.sign
      sks.(0) (* Use incorrect secret key to sign *)
      ~msg:(Circuit.Clear.unsigned_operation_to_bytes op)
  in
  let c = Circuit.Clear.operation_to_bytes (op, signature) in
  (* User 1 append a debit op with an invalid signature *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 c >>= fun ctx ->
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  (* We craft and perform the update for the debit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* As the operation is invalid, the exits should NOT be executed.
     Thus, the context is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  Some ()

(* Invalid debit: src doesn't exist *)
let test_debit_invalid4 () =
  let ( >>= ) = Option.bind in
  credited_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 10) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  (* We append a debit operation from user 2 of 2 tokens *)
  Protocol.append ctx pks.(2) ctx.(get_ru ctx).address 0 (User2.debit 2)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the debit is performed
     after the update *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 10) ;
  (* We craft and perform the update for the debit op *)
  let (pi, proof, _state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that context is unchanged, as the debit was invalid *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 6) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 10) ;
  Some ()

(* Context-state pair where 2 users already entered the RU and user 1
   has 5 tokens *)
let transfer_ctx_state =
  let ( >>= ) = Option.bind in
  initial_ctx_state >>= fun (ctx, state) ->
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 1 (User1.enter 1)
  >>= fun ctx ->
  Protocol.append ctx pks.(2) ctx.(get_ru ctx).address 1 (User2.enter 1)
  >>= fun ctx ->
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 5 (User1.credit 5)
  >>= fun ctx ->
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  Some (ctx, state)

let test_transfer_valid () =
  let ( >>= ) = Option.bind in
  transfer_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* Whe check the initial state *)
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  (* We append a transfer operation from user 1 to user 2 of 2 tokens with
     fee of 1 token *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 (User1.transfer 2 2 1)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the transfer only
     modifies the RU's state *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We craft and perform the update for the credit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We check that the transfer has been performed *)
  assert ((Circuit.Context.get state 0).el.balance = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 2) ;
  assert ((Circuit.Context.get state 2).el.balance = 2) ;
  Some ()

(* Invalid transfer: insufficient tokens *)
let test_transfer_invalid1 () =
  let ( >>= ) = Option.bind in
  transfer_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* Whe check the initial state *)
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  (* We append a transfer operation from user 1 to user 2 of 5 tokens with
     fee of 1 token *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 (User1.transfer 2 5 1)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the transfer only modifies
     the RU's state *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We craft and perform the update for the credit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We check that the only modification in the state is the payment of the fee
     as the transfer should be invalid *)
  assert ((Circuit.Context.get state 0).el.balance = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 4) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  Some ()

(* Invalid transfer: invalid signature *)
let test_transfer_invalid2 () =
  let ( >>= ) = Option.bind in
  transfer_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* Whe check the initial state *)
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  let op =
    Circuit.Clear.Transfer {cnt = 2; src = 1; dst = 2; amount = 2; fee = 1}
  in
  let signature =
    Crypto.Signature.sign
      sks.(0) (* Use incorrect secret key to sign *)
      ~msg:(Circuit.Clear.unsigned_operation_to_bytes op)
  in
  let c = Circuit.Clear.operation_to_bytes (op, signature) in
  (* We append an invalid transfer operation from user 1 to user 2 of 2 tokens
     with fee of 1 token, but invalid signature *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 c >>= fun ctx ->
  (* After the append, the context is unchanged, as the transfer only modifies
       the RU's state *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We craft and perform the update for the credit op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We check that the state is unchanged, as the transfer should be invalid *)
  (* As we don't know if the user 1 actually sent this request, the fee isn't
     considered *)
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  Some ()

(* Invalid transfer: replay *)
let test_transfer_invalid3 () =
  let ( >>= ) = Option.bind in
  transfer_ctx_state >>= fun (ctx, state) ->
  (* We check the initial context *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* Whe check the initial state *)
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  let t = User1.transfer 2 1 1 in
  (* We append a transfer operation from user 1 to user 2 of 1 token with fee
     of 1 token *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 t >>= fun ctx ->
  (* After the append, the context is unchanged, as the transfer only modifies
       the RU's state *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We craft and perform the update for the transfer op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We check the state *)
  assert ((Circuit.Context.get state 0).el.balance = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 3) ;
  assert ((Circuit.Context.get state 2).el.balance = 1) ;
  (* Now we replay the transfer *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 t >>= fun ctx ->
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun _ ->
  (* We check that the state hasn't changed, as the transfer should have
     been invalidated *)
  assert ((Circuit.Context.get state 0).el.balance = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 3) ;
  assert ((Circuit.Context.get state 2).el.balance = 1) ;
  Some ()

(* Invalid transfer: dst doesn’t exist *)
let test_transfer_invalid4 () =
  let ( >>= ) = Option.bind in
  transfer_ctx_state >>= fun (ctx, state) ->
  (* We append a transfer operation from user 1 to user 3 of 1 token with
     fee of 1 token *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 (User1.transfer 3 1 1)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the transfer only
     modifies the RU's state *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  (* We craft and perform the update for the transfer op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We check the state: the validator should get the fee, but the
     transfer shouldn’t be performed *)
  assert ((Circuit.Context.get state 0).el.balance = 3) ;
  assert ((Circuit.Context.get state 1).el.balance = 4) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  Some ()

(* Invalid transfer: src doesn’t exist *)
let test_transfer_invalid5 () =
  let ( >>= ) = Option.bind in
  transfer_ctx_state >>= fun (ctx, state) ->
  (* We append a transfer operation from user 3 to user 2 of 1 token with
     fee of 1 token *)
  Protocol.append ctx pks.(3) ctx.(get_ru ctx).address 0 (User3.transfer 2 1 1)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the transfer only
     modifies the RU's state *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  (* We craft and perform the update for the transfer op *)
  let (pi, proof, state) = Operator.craft_update state ctx.rollup 0 in
  Protocol.update ctx pks.(0) ctx.(get_ru ctx).address pi proof >>= fun ctx ->
  (* We check that the context is unchanged *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  (* We check the state: everything is unchanged *)
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  Some ()

(* Invalid transfer: val doesn’t exist *)
let test_transfer_invalid6 () =
  let ( >>= ) = Option.bind in
  transfer_ctx_state >>= fun (ctx, state) ->
  (* We append a transfer operation from user 1 to user 2 of 1 token with
     fee of 1 token *)
  Protocol.append ctx pks.(1) ctx.(get_ru ctx).address 0 (User1.transfer 2 1 1)
  >>= fun ctx ->
  (* After the append, the context is unchanged, as the transfer only
     modifies the RU's state *)
  assert (Protocol.Accounts.find pks.(1) ctx.accounts = 4) ;
  assert (Protocol.Accounts.find pks.(2) ctx.accounts = 9) ;
  assert (Protocol.Accounts.find ctx.(get_ru ctx).address ctx.accounts = 7) ;
  assert ((Circuit.Context.get state 0).el.balance = 2) ;
  assert ((Circuit.Context.get state 1).el.balance = 5) ;
  assert ((Circuit.Context.get state 2).el.balance = 0) ;
  (* We craft and perform the update for the transfer op *)
  (* The validator now is user 3, who isn't in the RU *)
  let (pi, proof, _state) = Operator.craft_update state ctx.rollup 3 in
  Protocol.update ctx pks.(3) ctx.(get_ru ctx).address pi proof >>= fun _ctx ->
  (* This will actually fail, because the validator isn't in the RU*)
  failwith "The update should be None!"
 *)
let tests =
  [
    Alcotest.test_case "debit" `Slow (fun () -> Option.get (test_debit ()));
    Alcotest.test_case "credit" `Slow (fun () -> Option.get (test_credit ()));
    Alcotest.test_case "genesis" `Slow (fun () -> Option.get (test_genesis ()));
    Alcotest.test_case "create" `Slow (fun () -> Option.get (test_create ()));
    Alcotest.test_case "transfer" `Slow (fun () ->
        Option.get (test_transfer ()));
    Alcotest.test_case "inval_transfer_fees" `Slow (fun () ->
        Option.get (test_inval_transfer_fees ()));
    Alcotest.test_case "create_credit_transfer" `Slow (fun () ->
        Option.get (test_create_credit_transfer ()));
    Alcotest.test_case "ill_formed" `Slow (fun () ->
        Option.get (test_ill_formed ()));
    Alcotest.test_case "all_ticket" `Slow (fun () ->
        Option.get (test_all_ticket ()));
    Alcotest.test_case "credit_ticket" `Slow (fun () ->
        Option.get (test_credit_ticket ()));
    Alcotest.test_case "inner_batch" `Slow (fun () ->
        Option.get (test_inner_batch ()))
    (* Alcotest.test_case "Create valid" `Quick (fun () ->
               Option.get (test_create_valid ()));
           Alcotest.test_case "Create invalid: insufficient tokens" `Quick (fun () ->
               Option.get @@ should_fail (test_create_invalid1 ()));
           Alcotest.test_case "Create invalid: invalid signature" `Quick (fun () ->
               Option.get (test_create_invalid2 ()));
           Alcotest.test_case "Credit valid" `Quick (fun () ->
               Option.get (test_credit_valid ()));
           Alcotest.test_case "Credit invalid: insufficient tokens" `Quick (fun () ->
               Option.get @@ should_fail (test_credit_invalid1 ()));
           Alcotest.test_case "Credit invalid: replay" `Quick (fun () ->
               Option.get (test_credit_invalid2 ()));
           Alcotest.test_case "Credit valid: from other user" `Quick (fun () ->
               Option.get (test_credit_valid3 ()));
           Alcotest.test_case "Credit invalid: dst doesn't exist" `Quick (fun () ->
               Option.get (test_credit_invalid4 ()));
           Alcotest.test_case "Debit valid" `Quick (fun () ->
               Option.get (test_debit_valid ()));
           Alcotest.test_case "Debit invalid: insufficient tokens" `Quick (fun () ->
               Option.get (test_debit_invalid1 ()));
           Alcotest.test_case "Debit invalid: replay" `Quick (fun () ->
               Option.get (test_debit_invalid2 ()));
           Alcotest.test_case "Debit invalid: invalid signature" `Quick (fun () ->
               Option.get (test_debit_invalid3 ()));
           Alcotest.test_case "Debit invalid: src doesn't exist" `Quick (fun () ->
               Option.get (test_debit_invalid4 ()));
           Alcotest.test_case "Transfer valid" `Quick (fun () ->
               Option.get (test_transfer_valid ()));
           Alcotest.test_case "Transfer invalid: insufficient tokens" `Quick (fun () ->
               Option.get (test_transfer_invalid1 ()));
           Alcotest.test_case "Transfer invalid: invalid signature" `Quick (fun () ->
               Option.get (test_transfer_invalid2 ()));
           Alcotest.test_case "Transfer invalid: replay" `Quick (fun () ->
               Option.get (test_transfer_invalid3 ()));
           Alcotest.test_case "Transfer invalid: dst doesn't exist" `Quick (fun () ->
               Option.get (test_transfer_invalid4 ()));
           Alcotest.test_case "Transfer invalid: src doesn't exist" `Quick (fun () ->
               Option.get (test_transfer_invalid5 ()));
           Alcotest.test_case "Transfer invalid: val doesn't exist" `Quick (fun () ->
               Option.get @@ should_fail (test_transfer_invalid6 ())); *);
  ]
