
    A light API for Tezos to host ZK-Rollups

# TODOs

- Define circuit for batch of private ops
- Improve tests for Tx_rollup
- Add docs to this document (assertions/checks, encodings, etc)
- Rename tx by op everywhere?
- Check if public_input_size is really considered in verify
- Implement Miguel's idea to get an individual Tx proof from aggregated proof.
- Append several ops at the same time
- Data-availability flag

# Questions

- [CLOSED] We can't rely on the client to hash a published op. This leads to attack (impossible to reject an op if the hash is wrong). How do we solve this? We can use the entire op as public input for published ops, this might increase the verification cost a little. Another solution is to compute the hash on L1, in append. This means exposing Poseidon. Additionally, the hash scheme should be "uniform", so no more compression (as we did it before). MA proposes to use the compressed value as the op representation (allowing us to benefit from compression and hash in L1).

```
a < A,b < B, d < D

compress a b d = D × B × a + D × b + d

compress op = c
```

- [TODO] How do we deal with a Create on a full tree?

> MA & AL: by allowing L1 to ignore Create ops after next_pos reaches a certain value (passed at RU creation). We could also assert this in a circuit (bound check on next_pos). This would no longer work, as we allow arbitrary new next_positions.
> An alternative is to add an "available positions counter" to the rollup state. The circuits would increase/decrease it. This is fine.

- [CLOSED] What do we do with out-of-range op_codes? We need nb_ops separate circuits to prove that it is out of bounds, as we need to assert op_code to be the expected one. For now, we just use 2 bits for op_codes, avoiding this problem. We could also expose this to L1, given that op_codes are transparent, and never append an op with invalid op_code

> We expose the number of circuits, do the check in append

- [CLOSED] Should we check the operation hash in the ill-formed circuit?

> AL: I think we shouldn't, given that the hash is vulnerable to second pre-image attacks when the expected bounds are not respected (because of compress). Then, we need to check the integrity of the operation passed as an input to the circuit. WAIT, can we even do that?! The op is a private input, so clearly not. Attack:
> A valid operation (op) is published on chain,
> A malicious operator makes an ill-formed proof for a bad op', chosen such that H(op) = H(op')
> The verifier cannot reject this proof, and op is censored
> RT & AL: There are actually 2 solutions. One is simply to hash without packing (this is the safest, but it adds ~4 hashes). The other solution is to take the entire op as a public input in the ill-formed circuit, as it can be fed by the protocol. No, this breaks opacity.

- [CLOSED] How do we avoid having a Credit/Debit proof as a private op?

> Using the op_code, this should go into the RU's configuration
> We add a public input to every circuit to check if the op can be private.

- [CLOSED] What should be passed to update? Block? What can be hidden when we have the intermediate pi trick?

> Exit validities should always be passed (one bit per Tx).
> Fees don't, we can pass the sum
> Op hashes need to be an input, but can be computed by protocol (for published). For private,
> we can rely on Miguel's idea for deriving a single proof from an aggregated one.

- Do we want to optimize the private tx circuit now? Replacing checks with assertions, only valid ops. Is the gain significant?

# Remarks

- [CLOSED] IMPORTANT: all operations need to sign a rollup id to avoid replays. (pkh for now)

- [CLOSED] For every circuit, indicate if it is public/private. Check this in update.

- We need to keep track of the next available position in the state:
  If there are several (>1) operators, the ones that don't send the update need to know where in the tree the new accounts are created.

- The create operation needs to have a fee to discourage attackers from filling up the state tree.

- Non-unicity of tickets, can be spammed. We need to trust operator, economic argument for malicious user. We could add proof for the operator to invalidate spam (or discard private). We lose determinism?

- We shouldn't "overwrite" tickets with amount = 0 !

# Ticket integration

Tickets are represented as a pair (id, amount), where id is the hash that identifies a Tezos ticket.

The state tree now has the following shape:

```
                             root
                          /       \
                             ...
accounts level            /
 (h_accounts)        account={cnt; pk; tez_balance; tickets_root}
                      /       \
                         ...
leaves level            /
(h_tickets)       leaf={pos; ticket}
```

where the leaf's pos ∈ [0, 2^(h_accounts + h_tickets))

Internally, we will sometimes use an "account index" (∈ [0, 2^h_accounts)) to identify an account. This is not stored in the tree.

To prove that an `account` holds `ticket` at a given `pos` in a tree described by `root`, we require two Merkle proofs:

- `leaf_proof`: such that `prove (pos, ticket) leaf_proof` and `account.tickets_root = leaf_proof.root`

- `account_proof`: such that `prove account account_proof` and `account_proof.root = root`

To bundle these proofs, we use the types

``` ocaml
  type proof = {path : Merkle.path; root : S.t}

  type account_tree_el = {before : account; after : account; proof : proof}

  type leaf_tree_el = {before : leaf; after : leaf; path : Merkle.path}

  type tree_el = {account : account_tree_el; leaf : leaf_tree_el}
```

These encode a state transition where a leaf in an account is possibly modified. `leaf_tree_el` only includes the `path` because the corresponding `root` can be found in `tree_el.account.after.tickets_root`. When building the expected new account in circuit (which is checked to be equal to `tree_el.account.after`), it is safe to use the value `tree_el.account.after.tickets_root`, as it has already been checked in the Merkle proofs.

# ZKRU Tx format

## 4/3/22 discussion

We propose two safe ways to deal with inputs to the verifier.

### Configuration file

Each rollup is defined by a configuration file, which is set in the L1 create operation. This config describes each Tx format precisely (i.e. how many bits each field should have). As a result, the circuit doesn't need to validate upper bounds and the representation is storage-efficient.

### Total opacity

Let the protocol only parse scalars, which are directly fed to the verifier. This requires us to add bound checks on inputs. (Serialization will be less efficient than on the previous approach, either each scalar declares its length or we use Z.t.)

Such bound checks are implemented through a binary decomposition of the given scalar, but we must be careful:
(i) if we don't provide enough bits for the binary decomposition, a malicious client can provide a scalar as input that makes the circuit unsatisfiable; (ii) if we provide enough bits (255 for BLS) so that every scalar admits a valid binary decomposition, then some scalars have two representations (s and s + P, when s + P < 2^(numbits P)).
We suggest two alternatives for solving this issue:

- α-method: Let α = 2^(numbits P) - P. When using the binary decomp of a scalar s in a circuit in order to do a comparisson, we will instead use the (numbits P)-decomposition of (s + α). The latter is guaranteed to exist and it is unique.
- Comparing the binary decomp. provided by the prover with that of P. More expensive, but conceptually simpler.

Consequently, we suggest to perform the above well-formedness check on an independent circuit instead of the main one, to avoid the significant overhead that it would introduce.
That way, we would have an "ill-formed" circuit, to reject published ops that don't adhere to the format.
Finally, the main circuit will assert upper bounds taking only the number of bits required, without worrying about ill-formed txs.

## Tx Formats

NB: circuits should always take the Price as the first input (after state/fee/hash)

TODO: sign op-code and check it in circuit

---------------

op_code 1 byte
---------------

price : amount of tez transferred to the dst 8 bytes
---------------

length (in scalars) 2 bytes (could be derived from opcode)
---------------

scalars
---------------

- Create (creates a position in L2 rollup tree)
  - burn : scalar (cost of L2 storage)
  - pk : pk
  - signature : signature

NB: the fee must be asserted to be equal to a constant, perhaps half the price as Tezos.

----------------------------------------

CREATE | -BURN | 0 | 2 | BYTES(PK; SIGNATURE)
---------------------------------------

- Credit (transfers tokens from L1 to L2)
  - amount : scalar
  - cnt : scalar
  - dst : scalar

--------------------------------------

CREDIT | AMOUNT | 2 | BYTES(CNT; DST)
-------------------------------------

- Debit (transfers tokens from L2 to L1)
  - amount : scalar
  - cnt : scalar
  - src : scalar
  - signature : signature

NB: IMPORTANT we need to sign the L1 address

----------------------------------------

DEBIT | AMOUNT | 3 | BYTES(CNT; SRC; SIGNATURE)
---------------------------------------

- Transfer (transfers tokens from L2 to L2)
  - cnt : scalar
  - src : scalar
  - dst : scalar
  - amount : scalar
  - fee : scalar
  - signature : signature

----------------------------------------------------------

TRANSFER | 0 | FEE | 6 | BYTES(CNT; SRC; DST; AMOUNT; FEE; SIGNATURE)
-----------------------------------------------------------

NB: the transfer circuit has to take an additional input, which will always be 0. This is just done to make all the Txs' inputs uniform.

# Abstract

This document proposes a light API for the Tezos economic protocol
that would allow it to host a large class of ZK-Rollup designs.
The key advantages of ZK-Rollups for the Tezos chain are:

- scalability with very little overhead for the main chain
- modularity with minimal footprint on the code of the economic protocol
- privacy for rollup users with respect to the Tezos chain.

In this document we describe the API offered from the protocol and the
API that the rollup must provide.
We describe the validation of operations by a Tezos baker and by a
rollup validator and lastly we develop an example rollup with account
model and the usual operations: credit, debit and transfer.

# Introduction

In an Optimistic Rollup the host chain must understand the
internals of the rollup because it must be able to validate a rollup
block on its own in order to resolve disputes between rollup
validators.
In particular the host protocol needs the rollup definition of
supported operations, its state transition function (`apply` in Tezos)
and its state (`context` in Tezos). In other words the rollup protocol
is embedded in the host protocol.
Additionally, in order to resolve disputes, the host protocol needs
access to all transactions, making data-availability mandatory for all
transactions.
If we want to integrate several designs of Optimistic Rollups, the
impact on the host protocol becomes significant.
The main advantage of ORU is that they can run anything that the host
protocol can, including Michelson smart contracts.

On the other hand, in ZK-Rollups each update is proved to be correct by
the accompanying zero-knowledge proof, making each update final.
This significantly simplifies security assumptions and reduces latency
for the finality of a transaction.
The verification cost of the proof for the host chain is
independent from the number of transactions validated in the rollup,
thus providing great scalability.

An interesting consequence of the fact that a proof is all that it is
needed for validation is that the host protocol can be agnostic of
the details of the rollup.
Indeed for a ZK-Rollup, the equivalent of the ORU transition function
is a very compact representation of its circuit (as polynomial
commitments).
The state of the ZK-Rollup is also extremely compact, typically
amounting to a Merkle tree root.
Furthermore being operations of the rollup opaque values for the
protocol, data availability becomes optional, which means that the
rollup is extremely space efficient also for the transport layer of
the host chain.

In this proposal we further exploit the opaque nature of the ZK-Rollup
to design a minimal API that allows a large class of ZK-Rollups to be
hosted on the Tezos chain, with minimal impact on the code of economic
protocol: one new rollup account and two new operations, `append` and
`update`. This modularity allows rollup designers to deploy new solutions
independently from the protocol amendment cycle, encourages
innovation and customization of rollups for specific needs (e.g. a NFT
market, a local private currency, an exchange service, ...).

Our solution also subsumes many private chains use cases as it offers
flexibility, privacy with respect to the main chain and low cost with
the addition of ease of interoperability with a public chain.
Additionally, if the proving system allows for recursion, the rollup
can also offer transactions à la Zerocash which provide decentralized
privacy even inside the rollup (wrt to validators and other rollup
users).

The disadvantage of ZK-Rollup to this date are due to the limitations
of proving systems, notably it remains hard to have general
programmability. In the Tezos context this means no execution of
Michelson smart contracts inside the rollup.
It is however possible to provide a more limited programmability,
notably to create simple DeFi products such as AMM. This is however
left for future work.

# Validation cost

Executing Smart Contracts is a capability that will only be available
in the next generation of proving systems, in the immediate future we can
expect to execute only simple transfers of tokens in a circuit.

The main costs of validating a transfer on Tezos can be broken down in
a signature check and disk accesses to update balances.
Both these costs can be made constant with respect to the number of
operations, if the Tezos validator only needs to check a proof of
their validity.
The cost of validation is moved towards the prover, albeit with a
considerable multiplicative factor, and this shift allows the chain to
scale with the number of validators.

The verification of a proof is an operation constant with respect to
the number of operations, but it remains a complex cryptographic
computation. The size of a rollup block needs to be large enough to be
make verification competitive with respect to validating directly each
transaction.

When designing a zk-rollup one should also consider the economic
constraints of the cost of the host chain fees, the cost of the rollup
fees, the size of rollup blocks and the cost of renting hardware
capable enough to run the prover with an acceptable latency for users.

# Use cases

The API allows for a spectrum of designs, we illustrate here two very
different use cases. It should be noted that in any case the user must
trust the circuit that backs the zero-knowledge proofs.

## Permissioned rollup

A permissioned rollup allows a private company or institution to run a
service of which they control every aspect.
Clients of the service trust of the operator to run the service but
enjoy the ability to verify the integrity of their data and of the
system.

Running a permissioned rollup gives many of the advantages of private
blockchains, namely privacy with respect to the public chain and a
large degree of freedom in the functionalities supported by the
rollup.
However, being a rollup connected to the public chain, it leaves the
door open for users of the public chain to access the rollup.
Another advantage is that much of the technological development is
carried by the public blockchain, in particular the zero-knowledge
proving system.

In a permissioned rollup the state of the rollup is completely hidden
from the main chain and only a succinct representation of it is
published on chain.
Data availability is the responsability of the rollup validators and
users, which are probably communicating over a private network.
The operations are not published and even their description is not
known outside of the rollup.
Users should be able to access from the rollup private network the
blocks of the rollup, with all executed transactions.
Even without access to the transactions, users can be certain of the
correctness of the validation thanks to the proof.
Users should always be able to enter and exit the rollup, even without
the validators' consent, however they can execute operations inside
the rollup only with the approval of validators.

The only information available on chain is the configuration of the
rollup account, which consists in the function `price` and the circuit
description.
`price` does reveal that some operations might accept certain kinds of
tokens to enter and exit the rollup and this information is essential
to allow users of the main chain to join the rollup.
Lastly the circuit description is a succinct commitment to the code
that governs the rollup and anybody can verify it given the complete
description of the circuit.

## Permissionless rollup

If the goal of the rollup is to offload work from the main chain
(except for smart contract calls), a permissionless setup allows
anyone to submit operations and any validator to provide proofs.

If there is a worry that the proving power of the rollup is
concentrated in the hand of a cartel that could re-order or censor
transactions, any user can submit an `append` operation to publish a
block of transactions and force the provers to validate them.

# Creation

Any user can create a rollup account which, as any other account, has an
address and a balance of xtz.
The rollup however is not allowed to spend its balance (e.g. with a
transfer), only the protocol can credit its balance with the
application of `append` and debit it with `update`, the two new
protocol operations.
Additionally the rollup has a compact `state` - e.g. the root of a
Merkle tree - which can only be changed by `update`.
The two lists, `pending_ops` and `pending_exits`, handle the
asynchrony between the protocol and the rollup validation and are
modified by `append` and `update`.
For simplicity we present the case where balances consist of xtz but
everything can be generalized to handle multiple tokens (such as xtz
and tickets).

```ocaml
type rollup_account = {
  address : tezos_address;
  balance : nat;
  state : bytes;
  pending_ops : bytes list;
  pending_exits : (nat * tezos_address) list;
  config : {
    circuit : circuit;
    price : bytes -> int option;
    data-availability : bool;
  }
}
```

At creation the user configures its rollup using Michelson to define
the field `config`. We could also
define an access control policy like a multisig to change some parts
of the configuration of the rollup during its life.

The field `circuit` contains the (compact) information needed by the
proving system to validate the proofs of the rollup.
This field should be flexible enough to accommodate for several proving
systems (e.g. plonk and it's variants, halo2) and several proofs per
rollup. More details are discussed later.

Each rollup defines a set of operations which are opaque for the
protocol. The rollup however must provide the protocol with a `price`
function, which returns the amount of protocol tokens that the
operation claims to cost - e.g. a rollup mint operation of 1 ru-xtz
is priced 1 xtz.
The price of an operation is used by the protocol to enforce a
coherence between its own monetary mass and the one inside the rollup,
during the validation of `append` operations.
Note that `price` can also be used to realize more complex conversion
policies - e.g. for every 5 xtz the rollup creates 1 special token.

In the case of zk-rollups, data-availability is not strictly necessary
for validation but it is convenient for some use-cases. Each rollup can
impose it with the flag `data-availability` which will make invalid
any `update` operation which does not contain its list of transactions.

## TODO

- we could have another ACL for users, which would limit append
- add hash of more than circuit e.g. validator_code/operation_encodings ? add the full circuit ?
- what can be changed during the life of a rollup?
  can we let the rollup change its price function during its life?
- price could take more inputs, e.g. level, balance of the rollup etc
- code `price` in michelson
  - slicing bytes is ok
  - interpreting zarith is ok
  - parsing integers can be gas expensive
  So should probably change the type of price to `bytes -> bytes list
  option` so that it returns a list of bytes that has been properly
  sliced for a bigger transaction. Then the deserialization to integer
  can be done by the protocol in OCaml. The return type could also
  specify if it's a variable/fixed size integer and its size.
- use VIEWS to code the smart contract

# Append

The `append` operation is a way for the host protocol to force the
inclusion of a rollup operation, which will have to be deferred
to the next `update` operation.
Indeed each `update` guarantees that a certain number of pending
operations are executed by the rollup in the order they appear.
The main use for `append` is to enter and exit the rollup but it can
also be used to force any number of rollup operations at the cost of a
higher fee since those will be partially validated by the main chain.

```ocaml
type append_operation = {
  source : tezos_address;
  amount : int;
  rollup_ops : bytes list;
}
```

The first part of the validation for the protocol is similar to a
regular transfer of tokens from the source address to the address of
the rollup: check signature, check source balance, update balances of
source and rollup address, credit baker with fees.

The protocol then checks the value returned by `price` and
compares it to the amount that was tranferred.
If price is negative and amount is zero or price is the same as amount:

- the rollup operation is appended to the pending list
- the couple `(source, |price|)` is appended to the list of pending
  exits.
In all the other cases the validation fails.

Note that the pending exits always have a positive value, however they
are treated differently if they were appended by a positive or
negative price operation.
In particular when the rollup validates a positive price operation, if
it is valid the corresponding exit is invalidated.
Conversely if a negative price operation is invalid, then its
corresponding exit is invalidated as well.

Concretely in the case of enters we want to avoid a user loosing its
tokens by mistake by sending an invalid rollup operation.
In that case the tokens will be temporarly locked between the `append`
and `update` operation, but then they will be returned.

On the other hand when a users demands an exit, the rollup must first
check that there is sufficient balance inside the rollup, otherwise
the exit is canceled.

A permissioned rollup that wants to control the internal operations
can invalidate any zero-priced operation submitted by `append`, in
this way zero-priced operations can only be submitted by an `update`.
It is important to allow operations with a price different than zero
in `append` so that users can enter and exit.

## More general use for append

`append` has been described to execute enters and exits however it can
be used to force the rollup to execute a block of its operations.
Using `append` in this way is more expensive than including the
operation directly in an `update`, however it guarantees its
execution, which may be worth the cost in some case.
In particular submitting a block through `append` allows to separate
the privilege to choose which operations are validated and in which
order, from the proving power that is needed to produce the
zero-knowledge proofs to validate them.
This separation reduces the influence of the proving cartels.

# Update crafted by a validator

After entering a rollup a user can start submitting rollup operations
to a validator. Even the simplest rollup should at least support some
form of transfer between rollup addresses.
These operations can use any transport layer to reach a validator,
they can be gossiped over the Tezos p2p protocol or they can be
transferred privately directly to a validator.

A validator aggregates a number of rollup operations, validates them
and computes the new `state` of the rollup, together with a proof of
correctness.
It then posts an `update` operation with these data to the host chain,
for which it needs to pay fees.
The update for the rollup is the equivalent to a block for the main
chain.

The `update` contains also a hash of the included transactions.
In a permissioned rollup for example, it can be used by users to verify
the blocks published by their validators on their private service.
It can also be used as a Merkle tree root so that a validator can
provide its users with a proof of inclusion of their operations
without revealing the rest of the block.
The hash is passed to the circuit that verifies it corresponds to the
hash of the verified operations.

If `data-availability` was set at creation, an `update` must also
contain the operations themselves (which will increase its size and
probably its fees) and the protocol will verify that the hash of the
concatenation of the operations corresponds to the hash included in
the `update`.

Note that all operations included in an update must have a `price`
of zero. Indeed all non zero balance operations have to be known by
the main chain and so they must pass through an `append`.

```ocaml
type update_operation = {
  rollup_address : tezos_address;
  state : bytes ;
  proof : proof ;
  exits_validity : bool list;
  operations : { hash : bytes ; content: bytes list option };
}
```

Importantly the validator must include, as first operations in its
update, the pending operations of the rollup (in the same order).
Given that some of the pending enters or exits could be invalid - e.g.
because the address didn't really have the funds inside the rollup -
the validator also indicates their validity in a `exits_validity`
list.
This information will be used by the protocol do execute or
cancel the pending exits.

## Malicious validators

It is the prerogative of a validator to choose the order of inclusion
of the operations, just as it is for a Tezos baker, and it incurs in
the same problem of front-running. A possible solution, would be a
privacy preserving transaction format that doesn't leak any
exploitable information.

Censoring is another privilege of a validator and, if the rollup is
permissioned, a user could be excluded from the rollup by a cartel of
validators. Such a user would always have the option to exit the
rollup or force its transactions using an `append`.

Finally a validator could stop updating the rollup and, if the rollup is
permissioned, this would effectively freeze the funds of all users.
For this reason in a permissioned rollup, users should either trust
the set of validators or expect them to have a stake in the rollup so
that their tokens would be frozen too.

## TODO

- include hash of the old state?
  it allows the protocol to discard an outdated update operation with
  a simple check of hashes rather than validating the zkp
- because the pending list can be too large, we need to allow the
  validator to only do a prefix of it, so that it can also append some
  transactions of his choosing. We'll need to instantiate several
  circuits to do e.g. 50 pending + 50 transactions, 500 pending + 500
  transactions etc.

# Update validated by a baker

A baker that validates an update needs to check the ability of
the validator to pay fees as usual (signature check and balance check)
and the validity of the proof.
If the rollup imposes data availability, the hash of the concatenation
of the operations should correspond to the advertised one.

The baker passes a number of inputs into the proof verification function.

```ocaml
val verify
    ~old_state:bytes
    ~new_state:bytes
    ~pending_operations:hash
    ~operations:hash
    ~exits_validity
    ~proof
    : bool
```

If `verify` passes then:

- a prefix of the pending operations list is removed
- a prefix of the pending exits are executed if their validity flag is true.

If the rollup imposes data-availability then the baker needs also to
pass the hash of the the list of operations included in the `update`
so that the circuit can check it.

# Simple Rollup instance and its circuits

In this section we develop an example instance of rollup.

## Context

The state of the rollup is a simple account model stored as a Merkle
tree which contains in each leaf the public key and balance of a user
alongside a counter used to avoid replays. Additionally, the state stores
the next position to be used in the tree.
Each user is identified by its position in the tree. The tree has
a fixed size and returns a default dummy pk and a balance on 0 for any
empty position.

## Operations

TODO

- add check that position is inside the max size of the tree

There are 4 operations:

- `Create`: to enter the rollup creating an account,
- `Credit`: to send tokens from the chain to the rollup,
- `Debit`: to move tokens from the rollup to the chain and
- `Transfer`: to move tokens them between accounts.

Once aggregated in a block, the validator also adds its own account to
the block to reclaim fees.

```ocaml
type operation =
  | Transfer : {
      cnt : nat;
      src : pos;
      dst : pos;
      amount : nat;
      fee : nat;
      signature : signature;
    }
  | Debit : {
      cnt : nat;
      src : pos;
      amount : nat;
      signature : signature;
    }
  | Credit : {
      cnt : nat;
      dst : pos;
      amount : nat;
      signature : signature;
    }
  | Create : {
      pk : public_key;
      fee : nat;
    }


type block = {
  validator : nat;
  pending_operations : operation list;
  operations : operation list;
}
```

The `price` function defines a one-to-one conversion between the Tez
and the rollup native token.

```ocaml
let price = function
  | Debit {amount; _} -> - amount
  | Credit {amount; _} -> amount
  | Create {fee; _} -> fee
  | Transaction _ -> zero
```

## Snark-friendly Primitives

It should be noted that the proof system greatly influences the choice
of cryptographic primitives that can be used in a rollup protocol.
In this instance it is necessary to have a hash function and a
signature scheme.
Instances of this primitives that are efficient in a circuit (also
called Snark-friendly) are Pedersen hash and RedJubjub, popularized by
ZCash.
Note however that these instances are considerably slower outside the
circuit if compared with more established alternatives such as Blake2b
and Ed25519.
This consideration is important if the Tezos baker needs to pass to
the circuit verification the hash of some inputs, because in that case
the computation of the hash for n transactions should be faster than
checking n signatures.
Newer proof systems seems capable of dealing with Blake2b and Ed25519
in circuit, in that case interoperability with Tezos would be greatly
simplified.

## Circuits

We now define several circuits which can be combined to verify a set
of transactions.
Given that circuits require fixed size inputs and that proving time
increases with circuit size, each circuit will be instantiated for
several sizes (e.g. 50, 250, 500 transactions).
Additionally the prover needs to fill the list of transactions with
dummy transactions in order to reach the circuit size.

### Inputs

Each circuit takes a number of private and public inputs. In the
interest of verification time, the public inputs should be reduced to
a minimum.

We consider public inputs:

- `old_state`: the root of the last Merkle tree validated by a Baker alongside the next position to be used
- `new_state`: the root of the Merkle tree proposed by the rollup validator in this update operation alongside the next position to be used
- `operations_hash`: the hash of the list of operations
- `pending_operations_hash`: the hash of the pending operation list
- `exits_validity`: the list of each operation's exit validity

Note that a validator will first validate each transaction outside of
the circuit to establish if it is valid or not. Only valid transaction
are included in a block.
Invalid operations however can appear in the pending operations list.

We consider the private input to be a `block`.

In order to avoid traversing the Merkle tree inside the circuit,
every time an operation contains a position, the prover and verifier
pass as input in the circuit the content of the leaf and a Merkle
proof that it belongs to the tree.

Additionally for every edit in the tree an intermediate root needs to be
passed.
For `Create`, `Debit` and `Credit`, no intermediate roots are necessary
because they all cause a single edit in the tree.
`Transfer` requires an intermediate root, as it needs to modify the source
and destination. It's important to note that a block's operations fees are
collected and credited to the validator all at once.

### Tree insertions

For the following circuits we use the fact that for two roots
root_{1,2}, check_merkle_path (path, pos, content_1, root_1) &&
check_merkle_path (path, pos, content_2, root_2) shows that the
difference between the first and second tree is exactly pos changing
from content_1 to content_2.
Using this and creating intermediary roots when necessary allows to
make controlled modifications of the the rollup state, as shown in the
next part.

### Check integrity

- pending_{in,}valid_hash = hash pending_{in,}valid_ops

### Check Create

For an operation `Create { pk; fee; signature }`, the circuit
receives additionally:

- next_position
- root_{prev, next}, the roots before and after the operation

Check conjunction of:

- verify_signature(pk, signature, (Create, pk, fee))

### Check Credit

For an operation `Credit { cnt; pos ; amount ; signature }`, the circuit
receives additionally:

- (pk, balance, path, dst_cnt) corresponding to pos
- root_{prev, next}, the roots before and after the operation

Check conjunction of:

- check_merkle_path (path, pos, (pk, balance), root_prev)
- check_merkle_path (path, pos, (pk, balance_next), root_next)
- check cnt > dst_cnt
- check pk <> dummy_pk (the destination position existed)

### Check Debit

For an operation `Debit { cnt; pos ; amount ; signature }`, the circuit
receives additionally:

- (pk, balance, path, src_cnt) corresponding to pos
- root_{prev, next}, the roots before and after the operation

Check conjunction of:

- check_merkle_path (path, pos, (pk, balance), root_prev)
- check_merkle_path (path, pos, (pk, balance_next), root_next)
- check balance_next >= 0
- check cnt > src_cnt
- verify_signature(pk, signature, (Debit, pos, amount))

### Check Transfer

For an operation `Transfer { cnt ; src ; dst ; amount ; fee; signature }`,
the circuit receives additionally:

- (pk, balance, path, pos_cnt) corresponding to src and dst
- balance_next, the balance expected after the edit of src, dst
- root_prev, the root before the transfer
- root_src, the root after the edit for src
- root_next, the root after the edit for dst

Check conjunction of:

- check_merkle_path (path_src, pos_src, (pk_src, balance_src), root)
- check_merkle_path (path_src, pos_src, (pk_src, balance_src_next), root_src)
- check_merkle_path (path_dst, pos_dst, (pk_dst, balance_dst), root_src)
- check_merkle_path (path_dst, pos_dst, (pk_dst, balance_dst_next), root_next)
- check balance_src - amount - fee >= 0
- check dst_pk <> dummy_pk
- check cnt > src_cnt
- verify_signature(pk_src, signature, (pos_src, pos_dst, amount, fee))

### Check invalid Debit

For an operation `Debit { pos ; amount ; signature }`, the circuit
receives additionally:

- (pk, balance, path) corresponding to pos
- balance_next, the balance expected after the operation
- root_{prev, next}, the roots before and after the operation

Check disjunction of:

- check_merkle_path (path, pos, (pk, balance), root_prev)
- either
  - verify_signature(pk, signature, (Debit, pos, amount)) = false
  - amount > balance

### Check invalid Credit

For an operation `Credit { pos ; amount ; signature }`, the circuit
receives additionally:

- (pk, balance, path) corresponding to pos
- balance_next, the balance expected after the operation
- root, the root after the operation

Check disjunction of:

- check_merkle_path (path, pos, (pk, balance), root) = false
- verify_signature(pk, signature, (Credit, pos, amount)) = false

### TODO

- the circuit should not accept an operation with cost different than zero except in the pending ops
- check again invalid Credit / Debit, they seem wrong
- we could merge Debit and Credit in circuit. it's important to check the type of operation in the signature otherwise.
- circuit must forbid mints outside the pending list otherwise I can create fake tokens and exit with them before the others
- liveness of rollup operations

# TODO

- if we fix the position, endianess, size, unsignedness of price we can do w/o michelson
  we wanted price to be able to do more complex convertions but this can be done by a smart contract

- what's the use of permissioning if users can pass through append?
  you know who is getting the fees, you still have some control of what to include
  can you permission in another way? like inside the rollup? the fees are distributed inside the rollup after all
  a permissioned rollup could simply invalidate any zero-priced operation in the pending list
  it's useful to limit update to avoid:
  - provers war
  - a user submitting enter/exits and then a proof for a single one of them
  the best way is to hide your circuit

- versioning of the `circuit` commitment

- rename price into convert? deposit-withdraw? exchange?

- conservation of value is a good property for all the users that do not take part in the rollup
properties of the circuit:
- the property that pending operations are executed first
- the property that if a enter is valid, a corresponding exit is valid too

- there are no forks in the zkr, because they are resolved on the main chain, so there is no need for consensus
- in principle anybody can submit an update
- validators can choose order and censor, maybe the baking right are important after all
- they can also block updates so they should have some stake locked in the rollup

- economics of xtz fee vs ru fees vs update size
- use cases with simple sapling

- liveness of update operations

- add swap operation that takes two signatures
- add batch transfers from single src to execute atomically
- extend instance to more tokens

- upgrade/migrate a rollup?

- data-availability probably imposes blake

the size of pending lists must be bounded and if exceed append operations fail
