module Sig = struct
  type pk = int
  type pkh = int
  type sk = int
  type t = string

  let pk_encoding = Data_encoding.int31
  let pkh_encoding = pk_encoding
  let encoding = Data_encoding.string
  let hash_pk pk = pk
  let dummy_pk = 0

  let gen () =
    (* 2^30 - 1 is the max bound of Random.int *)
    let i = Random.int 1073741823 in
    (i, i, i)

  let sign sk ~msg = string_of_int sk ^ msg
  let verify pk ~msg signature = string_of_int pk ^ msg = signature
end

(* Model of L1 accounts *)
module Accounts = Map.Make (struct
  type t = Sig.pk

  let compare = compare
end)

(* Representation of the L2 state
   [next_position] should be UInt[hight_of_tree] *)
type state = { root : Bls12_381.Fr.t; next_position : int }

(* TODO: check if we actually want to go through Z *)
let scalar_data_encoding : Bls12_381.Fr.t Data_encoding.t =
  Data_encoding.(conv Bls12_381.Fr.to_z Bls12_381.Fr.of_z z)

let state_data_encoding : state Data_encoding.t =
  Data_encoding.(
    conv
      (fun { root; next_position } -> (root, next_position))
      (fun (root, next_position) -> { root; next_position })
      (obj2 (req "root" scalar_data_encoding) (req "next_position" int31)))

let state_to_scalar_array s = [| s.root; Bls12_381.Fr.of_int s.next_position |]

let tez_id =
  Data_encoding.Binary.to_bytes_exn scalar_data_encoding Bls12_381.Fr.zero

(* TODO: use real hash type *)
type ticket = { id : bytes; amount : Int32.t }

let ticket_data_encoding : ticket Data_encoding.t =
  Data_encoding.(
    conv
      (fun { id; amount } -> (id, amount))
      (fun (id, amount) -> { id; amount })
      (obj2 (req "id" bytes) (req "amount" int32)))

let ticket_to_scalar_array t =
  [|
    Data_encoding.Binary.of_bytes_exn scalar_data_encoding t.id;
    Bls12_381.Fr.of_z @@ Z.of_int32 @@ Int32.abs t.amount;
  |]

(* L1 view of an L2 operation. The transparent fields correspond to
   the L2 header, while the L2 payload is opaque *)
type zk_op = {
  op_code : int;
  price : ticket;
  l1_dst : Sig.pkh;
  rollup_id : Sig.pkh;
  payload : Bls12_381.Fr.t array;
}

let zk_op_to_scalar_array { op_code; price; l1_dst; rollup_id; payload } =
  let op_code = Bls12_381.Fr.of_int op_code in
  let price = ticket_to_scalar_array price in
  (* TODO: fix this v *)
  let l1_dst =
    Tx_rollup.Utils.tezos_pkh_to_s
      (Data_encoding.Binary.to_bytes_exn Sig.pkh_encoding l1_dst)
  in
  let rollup_id =
    Tx_rollup.Utils.tezos_pkh_to_s
      (Data_encoding.Binary.to_bytes_exn Sig.pkh_encoding rollup_id)
  in
  Array.concat [ [| op_code |]; price; [| l1_dst; rollup_id |]; payload ]

let zk_op_data_encoding : zk_op Data_encoding.t =
  Data_encoding.(
    conv
      (fun { op_code; price; l1_dst; rollup_id; payload } ->
        (op_code, price, l1_dst, rollup_id, payload))
      (fun (op_code, price, l1_dst, rollup_id, payload) ->
        { op_code; price; l1_dst; rollup_id; payload })
      (obj5 (req "op_code" uint8)
         (req "price" ticket_data_encoding)
         (req "l1_dst" Sig.pkh_encoding)
         (req "rollup_id" Sig.pkh_encoding)
         (req "payload" (array scalar_data_encoding))))

module SMap = Plonk.Main_protocol.SMap

type public_parameters =
  Plonk.Main_protocol.verifier_public_parameters
  * Plonk.Main_protocol.transcript

type rollup = {
  address : Sig.pk;
  (* Byte representation of L2 state *)
  state : bytes;
  pending_ops : zk_op list;
  pending_exits : (Sig.pk * ticket) list;
  public_parameters : public_parameters;
  (* Tracking the circuit names, alongside a boolean flag indicating
     if they can be used for private ops.
  *)
  circuits : bool SMap.t;
  (* Valid op_codes must be in [0, nb_ops) *)
  nb_ops : int;
}

module BMap = Map.Make (Bytes)

type account = Z.t * Z.t BMap.t

let empty_account : account = (Z.zero, BMap.empty)

(* Representation of L1 Context, for now we have only one rollup *)
type ctx = { accounts : account Accounts.t; rollup : rollup option }

let get_balance ?id pk ctx =
  match id with
  | None ->
      Option.value ~default:Z.zero
      @@ Option.map fst (Accounts.find_opt pk ctx.accounts)
  | Some id ->
      Option.value ~default:Z.zero
      @@ Option.bind (Accounts.find_opt pk ctx.accounts) (fun (tez_bal, bals) ->
             if id = tez_id then Some tez_bal else BMap.find_opt id bals)

(* Deposit/withdraw to/from an L1 account *)
let credit ?id pk amount ctx =
  let tez, bals =
    Option.value ~default:empty_account @@ Accounts.find_opt pk ctx.accounts
  in
  let accounts =
    match id with
    | None -> Accounts.add pk Z.(tez + amount, bals) ctx.accounts
    | Some id when id = tez_id ->
        Accounts.add pk Z.(tez + amount, bals) ctx.accounts
    | Some id ->
        let bal = get_balance ~id pk ctx in
        let bals = BMap.add id Z.(bal + amount) bals in
        Accounts.add pk (tez, bals) ctx.accounts
  in
  { ctx with accounts }

(* Create a new rollup *)
let create :
    ctx ->
    public_parameters ->
    circuits:bool SMap.t ->
    init_state:bytes ->
    fee:Z.t ->
    nb_ops:int ->
    Accounts.key ->
    ctx option =
 fun ctx public_parameters ~circuits ~init_state ~fee ~nb_ops operator ->
  let operator_balance = get_balance operator ctx in
  if operator_balance >= fee then
    let rollup =
      Some
        {
          address =
            (let _, pk, _ = Sig.gen () in
             pk);
          state = init_state;
          pending_ops = [];
          pending_exits = [];
          public_parameters;
          circuits;
          nb_ops;
        }
    in
    Some { ctx with rollup }
  else None

(* Publish an L2 operation on L1 *)
let append ~ctx ~src ~ru ~(ticket : ticket) ~(op : zk_op) ~fee : ctx option =
  (* Op_code must be in [0, rollup.nb_ops) *)
  Option.bind ctx.rollup (fun rollup ->
      if op.op_code >= 0 && op.op_code < rollup.nb_ops then
        let src_tez_balance = get_balance src ctx in
        let src_ticket_balance = get_balance ~id:ticket.id src ctx in
        let src_tez_balance_next = Z.(src_tez_balance - fee) in
        let src_ticket_balance_next =
          Z.sub
            (if ticket.id = tez_id then src_tez_balance_next
            else src_ticket_balance)
            (Z.of_int32 ticket.amount)
        in
        if Z.(src_tez_balance_next >= zero && src_ticket_balance_next >= zero)
        then
          let ctx = credit src Z.(-fee) ctx in
          let ctx = credit ~id:ticket.id src Z.(-of_int32 ticket.amount) ctx in
          if
            (op.price.amount > 0l && ticket = op.price)
            || (op.price.amount <= 0l && ticket.amount = 0l)
          then
            let ctx = credit ~id:ticket.id ru (Z.of_int32 ticket.amount) ctx in
            let pending_ops = rollup.pending_ops @ [ op ] in
            let pending_exits =
              rollup.pending_exits
              @ [ (src, { op.price with amount = Int32.abs op.price.amount }) ]
            in
            let rollup = Some { rollup with pending_ops; pending_exits } in
            Some { ctx with rollup }
          else None
        else None
      else None)

type op_pi = { new_state : state; fee : Bls12_381.Fr.t; exit_validity : bool }
type private_inner_pi = { new_state : state; fees : Bls12_381.Fr.t }
type fee_pi = { new_state : state }

(* Data sent to an update operation *)
type update_data = {
  pending_pis : (string * op_pi) array;
  private_pis : (string * private_inner_pi) array;
  fee_pi : fee_pi;
  proof : Plonk.Main_protocol.proof;
  operator : Accounts.key;
      (* operations_hash ? Perhaps we don't need it due to Miguel's proposal
         of extracting single Tx proofs from the aggregated proof.
      *)
}

let print_pi_map m =
  Printf.printf "{\n";
  SMap.iter
    (fun n pis ->
      Printf.printf "\t%s: [%s]\n" n
      @@ String.concat "; "
           (List.map
              (fun a ->
                "[|"
                ^ String.concat ";"
                    (Array.to_list @@ Array.map Bls12_381.Fr.to_string a)
                ^ "|]")
              pis))
    m;
  Printf.printf "}\n"

let rec drop n l = match n with 0 -> l | _ -> drop (n - 1) (List.tl l)

let take n l =
  let rec aux n l acc =
    match n with 0 -> acc | _ -> aux (n - 1) (List.tl l) (List.hd l :: acc)
  in
  List.rev @@ aux n l []

let insert s x m =
  match SMap.find_opt s m with
  | None -> SMap.add s [ x ] m
  | Some l -> SMap.add s (x :: l) m

(* Update the rollup's state, after verifying the correctness proof *)
let update ctx (data : update_data) ~fee : ctx option =
  Option.bind ctx.rollup (fun rollup ->
      let operator_balance = get_balance data.operator ctx in
      if operator_balance >= fee then
        let ctx = credit data.operator Z.(-fee) ctx in
        let open Bls12_381 in
        let nb_pending_processed = Array.length data.pending_pis in
        let pending_ops_and_pis =
          let pending_ops = Array.of_list rollup.pending_ops in
          Array.init nb_pending_processed (fun i ->
              (pending_ops.(i), data.pending_pis.(i)))
        in
        let pi_map = SMap.empty in
        let pi_map, new_state_bytes, fees, exit_validities =
          (* Check published ops *)
          Array.fold_left
            (fun (pi_map, old_state_bytes, fees, exit_validities)
                 (op, (name, sent_pi)) ->
              let old_state =
                Data_encoding.Binary.of_bytes_exn state_data_encoding
                  old_state_bytes
              in
              let op_scalars = zk_op_to_scalar_array op in
              let exit_validity_s =
                if sent_pi.exit_validity then Bls12_381.Fr.one
                else Bls12_381.Fr.zero
              in
              let pi =
                Array.(
                  concat
                    [
                      state_to_scalar_array old_state;
                      state_to_scalar_array sent_pi.new_state;
                      [| sent_pi.fee; exit_validity_s |];
                      [|
                        Tx_rollup.Utils.tezos_pkh_to_s
                          (Data_encoding.Binary.to_bytes_exn Sig.pkh_encoding
                          @@ Sig.hash_pk rollup.address);
                      |];
                      op_scalars;
                    ])
              in
              let pi_map = insert name pi pi_map in
              let new_state_bytes =
                Data_encoding.Binary.to_bytes_exn state_data_encoding
                  sent_pi.new_state
              in
              ( pi_map,
                new_state_bytes,
                Fr.add fees sent_pi.fee,
                sent_pi.exit_validity :: exit_validities ))
            (pi_map, rollup.state, Fr.zero, [])
            pending_ops_and_pis
        in
        let exit_validities = List.rev exit_validities in
        (* Check private ops *)
        let circ_validity, pi_map, new_state_bytes, fees =
          Array.fold_left
            (fun (circ_validity, pi_map, old_state_bytes, fees)
                 ((name, sent_pi) : string * private_inner_pi) ->
              let old_state =
                Data_encoding.Binary.of_bytes_exn state_data_encoding
                  old_state_bytes
              in
              let pi =
                Array.(
                  concat
                    [
                      state_to_scalar_array old_state;
                      state_to_scalar_array sent_pi.new_state;
                      [| sent_pi.fees |];
                      [|
                        Tx_rollup.Utils.tezos_pkh_to_s
                          (Data_encoding.Binary.to_bytes_exn Sig.pkh_encoding
                          @@ Sig.hash_pk rollup.address);
                      |];
                    ])
              in
              let pi_map = insert name pi pi_map in
              let new_state_bytes =
                Data_encoding.Binary.to_bytes_exn state_data_encoding
                  sent_pi.new_state
              in
              ( circ_validity && SMap.find name rollup.circuits,
                pi_map,
                new_state_bytes,
                Fr.add fees sent_pi.fees ))
            (true, pi_map, new_state_bytes, fees)
            data.private_pis
        in

        let pi_map, new_state_bytes =
          let old_state_bytes = new_state_bytes in

          (* Checking fee circuit *)
          let old_state =
            Data_encoding.Binary.of_bytes_exn state_data_encoding
              old_state_bytes
          in
          let new_state_bytes =
            Data_encoding.Binary.to_bytes_exn state_data_encoding
              data.fee_pi.new_state
          in
          let pi =
            Array.(
              concat
                [
                  state_to_scalar_array old_state;
                  state_to_scalar_array data.fee_pi.new_state;
                  [| fees |];
                ])
          in
          let pi_map = insert "fee" pi pi_map in
          (pi_map, new_state_bytes)
        in

        let checks =
          fst
          @@ Plonk.Main_protocol.verify_multi_circuits rollup.public_parameters
               ~public_inputs:pi_map data.proof
        in

        if circ_validity && checks then
          (* perform exits *)
          let ctx =
            List.fold_left
              (fun ctx ((address, ticket), b) ->
                if b then
                  let ctx =
                    credit ~id:ticket.id rollup.address
                      Z.(-of_int32 ticket.amount)
                      ctx
                  in
                  let ctx =
                    credit ~id:ticket.id address Z.(of_int32 ticket.amount) ctx
                  in
                  ctx
                else ctx)
              ctx
              (List.combine
                 (take nb_pending_processed rollup.pending_exits)
                 exit_validities)
          in
          Some
            {
              ctx with
              rollup =
                Some
                  {
                    rollup with
                    state = new_state_bytes;
                    pending_ops = drop nb_pending_processed rollup.pending_ops;
                    pending_exits =
                      drop nb_pending_processed rollup.pending_exits;
                  };
            }
        else None
      else None)
