module Hash = Plompiler.Poseidon128
module MerklePV = Plompiler.Merkle (Hash)
module Merkle = MerklePV.P
open Tx_rollup.Types.P
open Tx_rollup.Constants.Bound
module SchnorrPV = Plompiler.Schnorr (Hash)
module Schnorr = SchnorrPV.P
module TV = Tx_rollup.V (Plompiler.LibCircuit)
include Tx_rollup.P

type nonrec state = state

let cache_dir = "/tmp/rollup"
let add_path r n = r ^ "/" ^ n

let recompute =
  let s = Option.value (Sys.getenv_opt "RECOMPUTE") ~default:"1" in
  let b = 1 = int_of_string s in
  if b then (
    if Sys.file_exists cache_dir && Sys.is_directory cache_dir then (
      let files = Sys.readdir cache_dir in
      Array.iter (fun n -> Sys.remove (add_path cache_dir n)) files;
      Sys.rmdir cache_dir);
    Sys.mkdir cache_dir 0o777);
  b

let lazy_circuits =
  let predicate_op =
    TV.predicate_op ~old_root:Dummy.root ~old_next_pos:Dummy.pos
      ~new_root:Dummy.root ~new_next_pos:Dummy.pos ~fee:Dummy.fee
      ~exit_validity:true ~rollup_id:Dummy.tezos_pkh
  in
  let predicate_if =
    TV.predicate_ill_formed ~old_root:Dummy.root ~old_next_pos:Dummy.pos
      ~new_root:Dummy.root ~new_next_pos:Dummy.pos ~fee:Dummy.fee
      ~exit_validity:true ~rollup_id:Dummy.tezos_pkh
  in
  let get_circuit name c () =
    let r = Plompiler.LibCircuit.get_cs ~optimize:true c in
    let _initial, public_input_size = Plompiler.LibCircuit.get_inputs c in
    if recompute then (
      let outc = open_out (add_path cache_dir @@ name ^ "-solver") in
      Printf.fprintf outc "%s"
        (Bytes.to_string
        @@ Data_encoding.Binary.to_bytes_exn Plompiler.Solver.solver_encoding
             r.solver);
      close_out outc;
      let outc = open_out (add_path cache_dir @@ name ^ "-cs") in
      Printf.fprintf outc "%s"
        (Bytes.to_string
        @@ Data_encoding.Binary.to_bytes_exn
             Plompiler.Csir.CS.cs_pub_size_encoding (r.cs, public_input_size));
      close_out outc);
    (Plonk.Circuit.to_plonk ~public_input_size r.cs, public_input_size)
  in
  Protocol.SMap.of_list
  @@ List.map
       (fun (n, c) -> (n, Lazy.from_fun (get_circuit n c)))
       [
         ( "tx",
           predicate_op ~public:true (Transfer Dummy.transfer)
             (Transfer Dummy.transfer_storage) );
         ( "create",
           predicate_op ~public:true (Create Dummy.create)
             (Create Dummy.create_storage) );
         ( "credit",
           predicate_op ~public:true (Credit Dummy.credit)
             (Credit Dummy.credit_storage) );
         ( "debit",
           predicate_op ~public:true (Debit Dummy.debit)
             (Debit Dummy.debit_storage) );
         ( "private-batch-1",
           TV.predicate_private_batch ~old_root:Dummy.root
             ~old_next_pos:Dummy.pos ~new_root:Dummy.root
             ~new_next_pos:Dummy.pos ~fees:Dummy.amount
             ~rollup_id:Dummy.tezos_pkh
             (List.init 1 @@ Fun.const Dummy.transfer)
             (List.init 1 @@ Fun.const Dummy.transfer_storage) );
         ("if-tx", predicate_if (Transfer Dummy.transfer));
         ("if-create", predicate_if (Create Dummy.create));
         ("if-credit", predicate_if (Credit Dummy.credit));
         ("if-debit", predicate_if (Debit Dummy.debit));
         ( "fee",
           TV.predicate_fees ~old_root:Dummy.root ~old_next_pos:Dummy.pos
             ~new_root:Dummy.root ~new_next_pos:Dummy.pos ~fees:Dummy.amount
             Dummy.account_tree_el );
       ]

let circuit_names = List.map fst @@ Protocol.SMap.bindings lazy_circuits

let time description f =
  let start = Unix.gettimeofday () in
  let res = f () in
  let stop = Unix.gettimeofday () in
  let delta = stop -. start in
  if delta > 120. then
    Printf.printf "Time %s: %.3f min\n" description (delta /. 60.)
  else if delta > 1. then Printf.printf "Time %s: %.3f sec\n" description delta
  else Printf.printf "Time %s: %.3f ms\n" description (delta *. 1000.);
  res

(* Actual empty state *)
let init = empty_state ()

(* Bytes of initial state *)
let init_state =
  let module Hash = Plompiler.Poseidon128 in
  let module MerklePV = Plompiler.Merkle (Hash) in
  let module Merkle = MerklePV.P in
  let s = init in
  let root = Merkle.root s.accounts_tree in
  let state = Protocol.{ root; next_position = 0 } in
  Data_encoding.Binary.to_bytes_exn Protocol.state_data_encoding state

let load name enc =
  let bytes =
    let inc = open_in @@ add_path cache_dir name in
    let s = really_input_string inc (in_channel_length inc) in
    close_in inc;
    Bytes.of_string s
  in
  Data_encoding.Binary.of_bytes_exn enc bytes

let save name enc obj =
  let outc = open_out (add_path cache_dir @@ name) in
  let bytes =
    match Data_encoding.Binary.to_bytes enc obj with
    | Ok x -> x
    | Error e ->
        Data_encoding.Binary.pp_write_error Format.std_formatter e;
        failwith "error"
  in
  Printf.fprintf outc "%s" (Bytes.to_string @@ bytes);
  close_out outc

let n_proofs = 1000

let circuits =
  List.fold_left
    (fun acc name ->
      let priv =
        let prefix = "private" in
        let len_pre = String.length prefix in
        String.length name >= len_pre && String.sub name 0 len_pre = prefix
      in
      Protocol.SMap.add name priv acc)
    Protocol.SMap.empty circuit_names

(* Setup of circ map *)
let public_parameters, prover_pp =
  let circuit_map =
    if recompute then Protocol.SMap.map Lazy.force lazy_circuits
    else
      List.fold_left
        (fun acc name ->
          let cs, public_input_size =
            load (name ^ "-cs") Plompiler.Csir.CS.cs_pub_size_encoding
          in
          let circuit = Plonk.Circuit.to_plonk ~public_input_size cs in
          Protocol.SMap.add name (circuit, n_proofs) acc)
        Protocol.SMap.empty circuit_names
  in
  let (ppp, vpp), t =
    time "setup" @@ fun () ->
    Plonk.Main_protocol.setup_multi_circuits ~zero_knowledge:false circuit_map
      ~srsfiles:Plonk_test.Helpers.srsfiles
  in
  if recompute then (
    save "verifier_public_parameters"
      Plonk.Main_protocol.verifier_public_parameters_encoding vpp;
    save "transcript" Plonk.Main_protocol.transcript_encoding t);
  ((vpp, t), ppp)

let pk_data_encoding : Schnorr.pk Data_encoding.t =
  let open Data_encoding in
  let module Curve = Mec.Curve.Jubjub.AffineEdwards in
  conv
    (fun p ->
      Curve.(Base.to_z (get_u_coordinate p), Base.to_z (get_v_coordinate p)))
    (fun (u, v) ->
      Curve.(from_coordinates_exn ~u:(Base.of_z u) ~v:(Base.of_z v)))
    (tup2 z z)

let signature_data_encoding : Schnorr.signature Data_encoding.t =
  (* TODO: fix this? *)
  let open Data_encoding in
  let open Schnorr in
  conv
    (fun { sig_u_bytes; sig_r; c_bytes } -> (sig_u_bytes, sig_r, c_bytes))
    (fun (sig_u_bytes, sig_r, c_bytes) -> { sig_u_bytes; sig_r; c_bytes })
    (obj3
       (req "sig_u_bytes" (list bool))
       (req "sig_r" pk_data_encoding)
       (req "c_bytes" (list bool)))

let to_protocol_ticket : ?neg:bool -> 'a ticket -> Protocol.ticket =
 fun ?(neg = false) { id; amount } ->
  {
    id = Data_encoding.Binary.to_bytes_exn Protocol.scalar_data_encoding id;
    amount =
      (if neg then Int32.neg else Fun.id) @@ Z.to_int32 @@ Bounded.v amount;
  }

let of_protocol_ticket :
    bound:'a Tx_rollup.Constants.Bound.t -> Protocol.ticket -> 'a ticket =
 fun ~bound { id; amount } ->
  {
    id = Data_encoding.Binary.of_bytes_exn Protocol.scalar_data_encoding id;
    amount = Bounded.make ~unsafe:true ~bound (Z.of_int32 @@ Int32.abs amount);
  }

(* Op-codes:
    0 - Transfer
    1 - Create
    2 - Credit
    3 - Debit *)

(* Conversion between the L1 view of L2 operations and the Tx_rollup types *)
let to_protocol_tx : tx -> Protocol.zk_op =
  let open Tx_rollup.Types.Encodings (Plompiler.LibCircuit) in
  function
  | Transfer tx ->
      let i = (transfer_payload_encoding ~safety:NoCheck).input tx.payload in
      let payload = Plompiler.LibCircuit.serialize i in
      assert (Bounded.v tx.header.op_code = Z.zero);
      assert (Bounded.v tx.header.price.amount = Z.zero);
      Protocol.
        {
          op_code = 0;
          price = Protocol.{ id = tez_id; amount = 0l };
          l1_dst =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding tx.header.l1_dst;
          rollup_id =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding
              tx.header.rollup_id;
          payload;
        }
  | Create tx ->
      let i = (create_payload_encoding ~safety:NoCheck).input tx.payload in
      let payload = Plompiler.LibCircuit.serialize i in
      assert (Bounded.v tx.header.op_code = Z.one);
      assert (Bounded.v tx.header.price.amount = Bounded.v tx.payload.msg.fee);
      Protocol.
        {
          op_code = 1;
          price = to_protocol_ticket tx.header.price;
          l1_dst =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding tx.header.l1_dst;
          rollup_id =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding
              tx.header.rollup_id;
          payload;
        }
  | Credit tx ->
      let i = (credit_payload_encoding ~safety:NoCheck).input tx.payload in
      let payload = Plompiler.LibCircuit.serialize i in
      assert (Bounded.v tx.header.op_code = Z.of_int 2);
      assert (
        Bounded.v tx.header.price.amount = Bounded.v tx.payload.amount.amount);
      Protocol.
        {
          op_code = 2;
          price = to_protocol_ticket tx.header.price;
          l1_dst =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding tx.header.l1_dst;
          rollup_id =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding
              tx.header.rollup_id;
          payload;
        }
  | Debit tx ->
      let i = (debit_payload_encoding ~safety:NoCheck).input tx.payload in
      let payload = Plompiler.LibCircuit.serialize i in
      assert (Bounded.v tx.header.op_code = Z.of_int 3);
      assert (
        Bounded.v tx.header.price.amount
        = Bounded.v tx.payload.msg.amount.amount);
      Protocol.
        {
          op_code = 3;
          price = to_protocol_ticket ~neg:true tx.header.price;
          l1_dst =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding tx.header.l1_dst;
          rollup_id =
            Data_encoding.Binary.of_bytes_exn Sig.pkh_encoding
              tx.header.rollup_id;
          payload;
        }

let of_protocol_tx : Protocol.zk_op -> tx =
  let open Plompiler.Encodings (Plompiler.LibCircuit) in
  let open Tx_rollup.Types.Encodings (Plompiler.LibCircuit) in
  let module Sig = Protocol.Sig in
  fun Protocol.{ op_code; price; l1_dst; rollup_id; payload } ->
    let header =
      {
        op_code = Bounded.make ~bound:max_op_code (Z.of_int op_code);
        price = of_protocol_ticket ~bound:max_balance price;
        l1_dst = Data_encoding.Binary.to_bytes_exn Sig.pkh_encoding l1_dst;
        rollup_id = Data_encoding.Binary.to_bytes_exn Sig.pkh_encoding rollup_id;
      }
    in
    let make_payload :
        type a b c.
        (safety:Bounded_e.bound_check_safety -> (a, b, c) encoding) -> a -> a =
     fun enc dummy ->
      let enc = enc ~safety:NoCheck in
      let dummy_input = enc.input dummy in
      let i = Plompiler.LibCircuit.deserialize payload dummy_input in
      enc.of_input i
    in
    match op_code with
    | 0 ->
        let payload =
          make_payload transfer_payload_encoding Dummy.transfer_payload
        in
        Transfer { header; payload }
    | 1 ->
        let payload =
          make_payload create_payload_encoding Dummy.create_payload
        in
        Create { header; payload }
    | 2 ->
        let payload =
          make_payload credit_payload_encoding Dummy.credit_payload
        in
        Credit { header; payload }
    | 3 ->
        let payload = make_payload debit_payload_encoding Dummy.debit_payload in
        Debit { header; payload }
    | _ -> failwith "of_protocol_tx: op_code not supported"

(* Create an update for a given number of published operations and a list
   of private operations *)
let craft_update :
    state ->
    operator_index:int ->
    operator_l1pk:Protocol.Sig.pk ->
    ?private_ops:Protocol.zk_op list list ->
    ?nb_pending:int ->
    Protocol.rollup ->
    Protocol.update_data * state =
 fun s ~operator_index ~operator_l1pk ?(private_ops = []) ?nb_pending rollup ->
  (* Read a solver from disk *)
  let load_solver name = load name Plompiler.Solver.solver_encoding in

  (* Given an op and some optional storage, compute the triplet:
      - common_public_inputs: scalar array representing
         <old_state, new_state, fee, exit_validity; rollup_id>
      - op_scalars: scalar array represeting the op
      - pi_bytes: array of the bytes for the public inputs that
        the operator has to send to the protocol. See Protocol.

     The first two are separated because the second is only part of
     the public inputs for public ops.
  *)
  let tx_public_inputs ~old_state ~new_state =
    let module TxE = Tx_rollup.Types.Encodings (Plompiler.LibCircuit) in
    let module H = Plompiler.Poseidon128 in
    let module SPV = Plompiler.Schnorr (H) in
    let module S = SPV.V (Plompiler.LibCircuit) in
    let common_pi fee exit_validity rollup_id =
      Array.concat
      @@ [
           Protocol.state_to_scalar_array old_state;
           Protocol.state_to_scalar_array new_state;
           [| fee; exit_validity; rollup_id |];
         ]
    in
    (* Inputs actually sent, they don't include the op *)
    let pis_to_send fee exit_validity : Protocol.op_pi =
      { new_state; fee; exit_validity = Bls12_381.Fr.is_one exit_validity }
    in
    fun (tx : tx) tx_s ->
      match (tx, get_validity tx_s) with
      | Transfer tx, valid ->
          let fee =
            if valid then Bls12_381.Fr.of_z @@ Bounded.v tx.payload.msg.fee
            else Bls12_381.Fr.zero
          in
          let exit_validity = Bls12_381.Fr.zero in
          let rollup_id = Tx_rollup.Utils.tezos_pkh_to_s tx.header.rollup_id in
          let i_tx = TxE.(transfer_encoding ~safety:NoCheck).input tx in
          let w_tx = Plompiler.LibCircuit.serialize i_tx in
          ( common_pi fee exit_validity rollup_id,
            w_tx,
            pis_to_send fee exit_validity )
      | Create tx, valid ->
          let fee =
            if valid then Bls12_381.Fr.of_z @@ Bounded.v tx.payload.msg.fee
            else Bls12_381.Fr.zero
          in
          let exit_validity =
            if valid then Bls12_381.Fr.zero else Bls12_381.Fr.one
          in
          let rollup_id = Tx_rollup.Utils.tezos_pkh_to_s tx.header.rollup_id in
          let i_tx = TxE.(create_encoding ~safety:NoCheck).input tx in
          let w_tx = Plompiler.LibCircuit.serialize i_tx in
          ( common_pi fee exit_validity rollup_id,
            w_tx,
            pis_to_send fee exit_validity )
      | Credit tx, valid ->
          let fee = Bls12_381.Fr.zero in
          let exit_validity =
            if valid then Bls12_381.Fr.zero else Bls12_381.Fr.one
          in
          let rollup_id = Tx_rollup.Utils.tezos_pkh_to_s tx.header.rollup_id in
          let i_tx = TxE.(credit_encoding ~safety:NoCheck).input tx in
          let w_tx = Plompiler.LibCircuit.serialize i_tx in
          ( common_pi fee exit_validity rollup_id,
            w_tx,
            pis_to_send fee exit_validity )
      | Debit tx, valid ->
          let fee = Bls12_381.Fr.zero in
          let exit_validity =
            if valid then Bls12_381.Fr.one else Bls12_381.Fr.zero
          in
          let rollup_id = Tx_rollup.Utils.tezos_pkh_to_s tx.header.rollup_id in
          let i_tx = TxE.(debit_encoding ~safety:NoCheck).input tx in
          let w_tx = Plompiler.LibCircuit.serialize i_tx in
          ( common_pi fee exit_validity rollup_id,
            w_tx,
            pis_to_send fee exit_validity )
  in

  let inner_batch_public_inputs ~old_state ~new_state ~fees
      (ops : transfer list) =
    let rollup_id =
      let op = List.hd ops in
      Tx_rollup.Utils.tezos_pkh_to_s op.header.rollup_id
    in
    let pi =
      Array.concat
      @@ [
           Protocol.state_to_scalar_array old_state;
           Protocol.state_to_scalar_array new_state;
           [| fees; rollup_id |];
         ]
    in
    let pi_to_send : Protocol.private_inner_pi = { new_state; fees } in
    (pi, pi_to_send)
  in

  (* Compute the initial private inputs for a given operation storage.
     This does not include the operation itself, which is part
     of the private inputs for private ops, as it is already
     provided by [tx_public_inputs].
  *)
  let tx_private_inputs op_s =
    let open Plompiler in
    let module TxE = Tx_rollup.Types.Encodings (LibCircuit) in
    let module H = Poseidon128 in
    let module SPV = Schnorr (H) in
    let module S = SPV.V (LibCircuit) in
    let i_g = S.pk_encoding.input SPV.P.g in
    let w_g = LibCircuit.serialize i_g in
    match op_s with
    | Some (Transfer tx_s) ->
        let i_tx = TxE.transfer_storage_encoding.input tx_s in
        let w_tx = LibCircuit.serialize i_tx in
        Array.append w_tx w_g
    | Some (Create tx_s) ->
        let i_tx = TxE.create_storage_encoding.input tx_s in
        let w_tx = LibCircuit.serialize i_tx in
        Array.append w_tx w_g
    | Some (Credit tx_s) ->
        let i_tx = TxE.credit_storage_encoding.input tx_s in
        let w_tx = LibCircuit.serialize i_tx in
        Array.append w_tx w_g
    | Some (Debit tx_s) ->
        let i_tx = TxE.debit_storage_encoding.input tx_s in
        let w_tx = LibCircuit.serialize i_tx in
        Array.append w_tx w_g
    | None -> [||]
  in

  let inner_batch_private_inputs (ops : transfer list)
      (ops_s : transfer_storage list) =
    let open Plompiler in
    let module TxE = Tx_rollup.Types.Encodings (LibCircuit) in
    let module H = Poseidon128 in
    let module SPV = Schnorr (H) in
    let module S = SPV.V (LibCircuit) in
    let ops_w =
      Array.concat
      @@ List.map
           (fun op ->
             LibCircuit.serialize
             @@ TxE.(transfer_encoding ~safety:NoCheck).input op)
           ops
    in
    let ops_s_w =
      Array.concat
      @@ List.map
           (fun op_s ->
             LibCircuit.serialize @@ TxE.transfer_storage_encoding.input op_s)
           ops_s
    in
    let i_w = LibCircuit.serialize @@ S.pk_encoding.input SPV.P.g in
    Array.concat [ ops_w; ops_s_w; i_w ]
  in
  time "crafting update" @@ fun () ->
  let to_proto_state s : Protocol.state =
    let Tx_rollup.Types.P.{ accounts_tree; next_position; _ } = s in
    let root = Merkle.root accounts_tree in
    Protocol.{ root; next_position }
  in
  let nb_pending =
    Option.value ~default:(List.length rollup.pending_ops) nb_pending
  in
  (* Pending ops that will be processed by this update *)
  let pending = List.init nb_pending (List.nth rollup.pending_ops) in

  (* Process public ops *)
  let rollup_id =
    Protocol.Sig.(
      Data_encoding.Binary.to_bytes_exn pkh_encoding @@ hash_pk rollup.address)
  in
  let s, pending_pis, inputs, fees =
    List.fold_left
      (fun (s, acc, inputs, fees) tx ->
        let old_state = to_proto_state s in
        let tx = of_protocol_tx tx in
        let s, op, op_s = preprocess_operation s tx rollup_id in
        let new_state = to_proto_state s in
        let name =
          let prefix = if Option.is_none op_s then "if-" else "" in
          prefix
          ^
          match op with
          | Transfer _ -> "tx"
          | Create _ -> "create"
          | Credit _ -> "credit"
          | Debit _ -> "debit"
        in

        let solver = load_solver (name ^ "-solver") in
        let common_public_inputs, op_scalars, pi_to_send =
          tx_public_inputs ~old_state ~new_state op op_s
        in
        (* Initial private inputs *)
        let w_private_inputs = tx_private_inputs op_s in
        let public_inputs, w_private_inputs =
          (* if public then *)
          (Array.append common_public_inputs op_scalars, w_private_inputs)
          (* else (common_public_inputs, Array.append op_scalars w_private_inputs) *)
        in
        let initial = Array.append public_inputs w_private_inputs in
        let private_inputs = Plompiler.Solver.solve solver initial in

        (* assert (Plonk.Circuit.sat cs [] private_inputs) ; *)
        ( s,
          (name, pi_to_send) :: acc,
          Protocol.insert name
            Plonk.Main_protocol.
              { public = public_inputs; witness = private_inputs }
            inputs,
          Z.add (Bounded.v @@ tx_fee op op_s) fees ))
      (s, [], Protocol.SMap.empty, Z.zero)
      pending
  in
  let pending_pis = Array.of_list @@ List.rev pending_pis in

  (* Process inner batches of private ops *)
  let s, private_pis, inputs, fees =
    List.fold_left
      (fun (s, acc, inputs, fees) inner_batch ->
        let old_state = to_proto_state s in

        let s, ops, ops_s, batch_fees =
          preprocess_private_batch s
            (List.map of_protocol_tx inner_batch)
            rollup_id
        in
        let new_state = to_proto_state s in
        let name = "private-batch-" ^ string_of_int (List.length inner_batch) in

        let solver = load_solver (name ^ "-solver") in
        let public_inputs, pi_to_send =
          inner_batch_public_inputs ~old_state ~new_state
            ~fees:(Bls12_381.Fr.of_z @@ Bounded.v batch_fees)
            ops
        in
        (* Initial private inputs *)
        let w_private_inputs = inner_batch_private_inputs ops ops_s in

        let initial = Array.append public_inputs w_private_inputs in
        let private_inputs = Plompiler.Solver.solve solver initial in

        (* assert (Plonk.Circuit.sat cs [] private_inputs) ; *)
        ( s,
          (name, pi_to_send) :: acc,
          Protocol.insert name
            Plonk.Main_protocol.
              { public = public_inputs; witness = private_inputs }
            inputs,
          Z.add (Bounded.v @@ batch_fees) fees ))
      (s, [], inputs, fees) private_ops
  in

  let private_pis = Array.of_list @@ List.rev private_pis in

  let operator_account, operator_leaves, operator_tree =
    get_account operator_index s.accounts
  in
  let old_state = to_proto_state s in
  let fees = Bounded.make fees ~bound:max_amount in
  (* Credit the operator with the fees, update state, and get the
     storage for the operator.
  *)
  let operator, s =
    let _, path = Merkle.proof_path operator_index s.accounts_tree in
    let new_tez_bl_operator =
      Bounded.add_left ~unsafe:true operator_account.tez_balance fees
    in
    let new_acc_operator =
      { operator_account with tez_balance = new_tez_bl_operator }
    in
    let s =
      let accounts =
        IMap.add operator_index
          (new_acc_operator, operator_leaves, operator_tree)
          s.accounts
      in
      let accounts_tree =
        Merkle.update_tree ~input_length:2 s.accounts_tree operator_index
          (scalar_of_account new_acc_operator)
      in
      { s with accounts_tree; accounts }
    in
    let root = Merkle.root s.accounts_tree in
    ( {
        before = operator_account;
        after = new_acc_operator;
        proof = { path; root };
      },
      s )
  in
  let new_state = to_proto_state s in

  let solver = load_solver "fee-solver" in
  let public_inputs =
    Array.concat
      [
        Protocol.state_to_scalar_array old_state;
        Protocol.state_to_scalar_array new_state;
        [| Bls12_381.Fr.of_z @@ Bounded.v fees |];
      ]
  in
  let w_private_inputs =
    let open Plompiler in
    let module TxE = Tx_rollup.Types.Encodings (LibCircuit) in
    let module H = Poseidon128 in
    let module SPV = Schnorr (H) in
    let module S = SPV.V (LibCircuit) in
    let i_tx = TxE.account_tree_el_encoding.input operator in
    let w_tx = LibCircuit.serialize i_tx in
    let i_g = S.pk_encoding.input SPV.P.g in
    let w_g = LibCircuit.serialize i_g in
    Array.append w_tx w_g
  in
  let initial = Array.append public_inputs w_private_inputs in
  let private_inputs = Plompiler.Solver.solve solver initial in

  (* assert (Plonk.Circuit.sat cs [] private_inputs) ; *)
  let inputs =
    Protocol.insert "fee"
      Plonk.Main_protocol.{ public = public_inputs; witness = private_inputs }
      inputs
  in

  let fee_pi = Protocol.{ new_state } in

  let (_, t), pp = (public_parameters, prover_pp) in

  let proof, _ =
    time "prove" @@ fun () ->
    Plonk.Main_protocol.prove_multi_circuits ~zero_knowledge:false (pp, t)
      ~inputs
  in
  let public_inputs =
    Plonk.Main_protocol.SMap.map
      (List.map (fun Plonk.Main_protocol.{ public; _ } -> public))
      inputs
  in
  assert (
    time "verify" @@ fun () ->
    fst
    @@ Plonk.Main_protocol.verify_multi_circuits public_parameters
         ~public_inputs proof);
  let update_data =
    Protocol.
      { pending_pis; private_pis; fee_pi; proof; operator = operator_l1pk }
  in
  (update_data, s)
