(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plompiler
module CS = Plonk.Circuit

let srs_root = Option.value (Sys.getenv_opt "SRS_DIR") ~default:"."
let srs_path srs = srs_root ^ "/" ^ srs

let srsfiles =
  ( (srs_path "srs_zcash_g1", srs_path "srs_zcash_g2"),
    (srs_path "srs_filecoin_g1", srs_path "srs_filecoin_g2") )

let output_buffer = ref stdout
let open_output_buffer () = output_buffer := open_out "test.output"
let close_output_buffer () = close_out !output_buffer
let set_seed s = Random.init s

let time description f =
  let start = Unix.gettimeofday () in
  let res = f () in
  let stop = Unix.gettimeofday () in
  let delta = stop -. start in
  if delta > 120. then
    Printf.printf "Time %s: %.3f min\n" description (delta /. 60.)
  else if delta > 1. then Printf.printf "Time %s: %.3f sec\n" description delta
  else Printf.printf "Time %s: %.3f ms\n" description (delta *. 1000.);
  res

type test_info = { valid : bool; name : string; flamegraph : bool }

module type Test = functor (L : LIB) -> sig
  open L

  val tests : ((unit -> unit repr t) * test_info) list
end

let string_of_bytes bytes =
  if bytes <= 1024. then Printf.sprintf "%3.2f B " bytes
  else
    let kilobytes = bytes /. 1024. in
    if kilobytes <= 1024. then Printf.sprintf "%3.2f KB" kilobytes
    else
      let megabytes = kilobytes /. 1024. in
      if megabytes <= 1024. then Printf.sprintf "%3.2f MB" megabytes
      else
        let gigabytes = megabytes /. 1024. in
        Printf.sprintf "%.2f GB" gigabytes

let run_plonk ?(name = "") cs used_vars tables private_inputs public_input_size
    =
  let module Main = Plonk.Main_protocol in
  (* Run PlonK on the circuit three times:
     1. Valid input
     2. Invalid inputs to the prover, [prove] should fail
     3. Invalid inputs to the verifier, [verify] should fail
  *)
  let circuit = CS.to_plonk ~public_input_size ~tables cs in
  let modify_inputs a =
    (* Modify a random element of [a] *)
    let len = Array.length a in
    let used_vars = List.filter (fun v -> v < len) used_vars in
    assert (List.length used_vars > 0);
    let i = Stdlib.Random.int (List.length used_vars) in
    let a' = Array.copy a in
    a'.(List.nth used_vars i) <- S.random ();
    a'
  in
  let public_inputs = Array.sub private_inputs 0 public_input_size in
  let inputs = Main.{ public = public_inputs; witness = private_inputs } in
  let (prv, vrf), transcript = Main.setup circuit ~srsfiles ~nb_proofs:1 in
  (* Valid run *)
  let proof, _transcript =
    try Main.prove (prv, transcript) ~inputs with
    | Main.Rest_not_null _ ->
        raise (Invalid_argument "Proving error: incorrect witness")
    | Main.Entry_not_in_table _ ->
        raise (Invalid_argument "Proving error: incorrect lookup")
    | _ -> raise (Invalid_argument "Proving error: unknown error")
  in
  let proof_size =
    Data_encoding.Binary.length Main.proof_encoding proof |> Float.of_int
  in
  Printf.fprintf !output_buffer "%s:\nProof size: %s\n\n" name
  @@ string_of_bytes proof_size;
  assert (fst @@ Main.verify (vrf, transcript) ~public_inputs proof);
  (* Invalid (private) inputs to the prover *)
  (try
     let invalid_inputs =
       Main.{ inputs with witness = modify_inputs inputs.witness }
     in
     let _ = Main.prove (prv, transcript) ~inputs:invalid_inputs in
     failwith "Prover succeeded with invalid inputs"
   with _ -> ());
  (* Invalid (public) inputs to the verifier *)
  if Array.length public_inputs > 0 then
    let invalid_public_inputs = modify_inputs inputs.public in
    let b, _ =
      Main.verify (vrf, transcript) ~public_inputs:invalid_public_inputs proof
    in
    if b then failwith "Verifier succeeded with invalid inputs"

let test ~plonk ?(optimize = false) test =
  let module Test = (val test : Test) in
  let circuits =
    let module E1 = Test (LibCircuit) in
    E1.tests
  in
  let results =
    let module E2 = Test (LibResult) in
    E2.tests
  in
  let run_one_test (circuit, info) (result, _) =
    let LibCircuit.{ used_vars; cs; tables; solver; _ } =
      LibCircuit.(get_cs (circuit ()))
    in

    let initial, public_input_size = LibCircuit.(get_inputs (circuit ())) in
    if info.flamegraph then
      Plompiler.Utils.dump_label_traces (info.name ^ "_flamegraph") cs;
    let pi =
      try Solver.solve solver initial |> fun x -> Some x with _ -> None
    in
    match pi with
    | None -> assert (not info.valid)
    | Some private_inputs ->
        (* Printf.printf
             "Trace:\n%s\n%s\n"
             (String.concat
                ","
                (List.init (Array.length private_inputs) string_of_int))
             (String.concat
                ","
                (List.map S.string_of_scalar (Array.to_list private_inputs))) ;
           Printf.printf "CS:\n%s\n" (CS.to_string cs) ; *)
        if not info.valid then assert (not @@ CS.sat cs tables private_inputs)
        else (
          Printf.fprintf !output_buffer "%s:\nConstraints: %d\n\n" info.name
            Array.(concat cs |> length);

          assert (CS.sat cs tables private_inputs);
          let cs, private_inputs =
            if optimize then (
              let LibCircuit.{ cs; solver; _ } =
                LibCircuit.(get_cs ~optimize (circuit ()))
              in
              Printf.fprintf !output_buffer "%s_optimized:\nConstraints: %d\n\n"
                info.name
                Array.(concat cs |> length);
              let private_inputs = Solver.solve solver initial in
              assert (CS.sat cs tables private_inputs);

              (cs, private_inputs))
            else (cs, private_inputs)
          in
          if info.flamegraph then
            Plompiler.Utils.dump_label_traces (info.name ^ "_opt_flamegraph") cs;
          let res = LibResult.get_result (result ()) in
          let serialized_res = LibResult.serialize res in
          let out_size = Array.length serialized_res in
          let trace_out =
            Array.sub private_inputs
              (Array.length private_inputs - out_size)
              out_size
          in
          (* Compare values obtained from Result and Circuit interpreters *)
          assert (Array.for_all2 S.( = ) serialized_res trace_out);

          if plonk then
            run_plonk ~name:info.name cs used_vars tables private_inputs
              public_input_size)
  in
  List.iter2 run_one_test circuits results

module Utils (L : LIB) = struct
  open L

  (* We test equality for Num, Bool, lists, tuples *)
  let test_equal x z () =
    let* x = input ~public:true x in
    let* z = input z in
    assert_equal x z

  let si x = Input.scalar @@ S.of_string @@ string_of_int x

  let test ~valid ?(name = "test") ?(flamegraph = false) x =
    (x, { valid; name; flamegraph })
end

let to_test ?(plonk = false) ?(optimize = true) m () = test ~plonk ~optimize m
