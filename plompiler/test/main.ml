(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plompiler_test

let () =
  Helpers.open_output_buffer ();
  (* Seed for deterministic pseudo-randomness:
      If the environment variable RANDOM_SEED is set, then its value is used as
      as seed. Otherwise, a random seed is used.
     WARNING: using [Random.self_init] elsewhere in the tests breaks thedeterminism.
  *)
  let seed =
    match Sys.getenv_opt "RANDOM_SEED" with
    | None ->
        Random.self_init ();
        Random.int 1073741823
    | Some v -> (
        try int_of_string v
        with _ ->
          failwith
            (Format.sprintf
               "Invalid random seed '%s'. Maybe you need to run '$ unset \
                RANDOM_SEED' in your terminal?"
               v))
  in
  Printf.printf "Random seed: %d\n" seed;
  Helpers.set_seed seed;
  Alcotest.run "Plompiler"
    [
      ("Core", Test_core.tests);
      (*       ("Bytes", Test_bytes.tests); *)
      ("Blake", Test_blake.tests);
      (*       ("Point", Test_plompiler.tests_point); *)
      ("Poseidon", Test_poseidon.tests);
      ("Enum", Test_enum.tests);
      ("Schnorr", Test_schnorr.tests);
      (*       ("Helpers", Test_helpers.tests); *)
      ("Merkle", Test_merkle.tests);
      ("Merkle N-arity: Plonk integration", Test_merkle_narity.tests);
      (*       ( "Merkle N-arity: circuit properties", *)
      (*         Test_merkle_narity.CircuitProperties.tests ); *)
      ("Edwards", Test_edwards.tests);
      ("Weierstrass", Test_weierstrass.tests);
      ("Serialization", Test_serialization.tests);
      ("Lookups", Test_lookup.tests);
      ("Linear algebra", Test_linear_algebra.tests);
      ("Bench", Benchmark.bench);
      ("Optimizer", Test_optimizer.tests);
      ("Encoding", Test_encoding.tests);
    ];
  Helpers.close_output_buffer ()
