(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Lang_core
module CS = Csir.CS
open Linear_algebra.Make_VectorSpace(S)
module Tables = Csir.Tables

type wire = A | B | C
type 'a tagged = Input of 'a | Output of 'a

val untag : 'a tagged -> 'a

type arith_desc = {
  a : int;
  b : int;
  c : int;
  ql : S.t;
  qr : S.t;
  qm : S.t;
  qc : S.t;
  qo : S.t;
  to_solve : wire;
}

type pow5_desc = { a : int; c : int }
type wires_desc = { a : int; b : int; c : int }
type add5_desc = { o : int; x1 : int; x2 : int; x3 : int; x4 : int; x5 : int }

type lookup_desc = {
  a : int tagged;
  b : int tagged;
  c : int tagged;
  table : string;
}

type ws_desc = { x1 : int; y1 : int; x2 : int; y2 : int; x3 : int; y3 : int }

type ed_desc = {
  a : S.t;
  d : S.t;
  x1 : int;
  y1 : int;
  x2 : int;
  y2 : int;
  x3 : int;
  y3 : int;
}

type bits_desc = { nb_bits : int; shift : Z.t; l : int; bits : int list }

type pos128full_desc = {
  x0 : int;
  y0 : int;
  x1 : int;
  y1 : int;
  x2 : int;
  y2 : int;
  k : S.t array;
  variant : Variants.t;
}

type pos128partial_desc = {
  a : int;
  b : int;
  c : int;
  a_5 : int;
  b_5 : int;
  c_5 : int;
  x0 : int;
  y0 : int;
  x1 : int;
  y1 : int;
  x2 : int;
  y2 : int;
  k_cols : matrix array;
  variant : Variants.t;
}

type solver_desc =
  | Arith of arith_desc
  | Pow5 of pow5_desc
  | IsZero of wires_desc
  | IsNotZero of wires_desc
  | Add5 of add5_desc
  | Lookup of lookup_desc
  | Ecc_Ws of ws_desc
  | Ecc_Ed of ed_desc
  | Skip
  | BitsOfS of bits_desc
  | Poseidon128Full of pos128full_desc
  | Poseidon128Partial of pos128partial_desc
  | Updater of Optimizer.trace_info

type solvers
type t = { solvers : solvers; initial_size : int; final_size : int }

val empty_solver : t
val append_solver : solver_desc -> t -> t
val solve : t -> S.t array -> S.t array
val solver_desc_encoding : solver_desc Data_encoding.t
val solver_encoding : t Data_encoding.encoding
