(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let build_array init next len =
  let xi = ref init in
  Array.init len (fun _ ->
      let i = !xi in
      xi := next !xi;
      i)

(* The second vector w may be shorter than v *)
let inner_product ~add ~mul v w =
  let rec aux acc = function
    | _, [] -> acc
    | a :: v', b :: w' -> aux (add acc (mul a b)) (v', w')
    | _ -> failwith "inner_product: first is shorter than second"
  in
  aux List.(mul (hd v) (hd w)) List.(tl v, tl w)

let read_vector a_size a_of_bytes len file =
  let read_element ic bytes_buf =
    Stdlib.really_input ic bytes_buf 0 a_size;
    a_of_bytes bytes_buf
  in
  let ic = open_in file in
  try
    if in_channel_length ic < len * a_size then
      failwith "Buffer is smaller than requested vector length";

    let bytes_buf = Bytes.create a_size in
    let vector = List.init len (fun _ -> read_element ic bytes_buf) in
    close_in ic;
    vector
  with error ->
    close_in ic;
    raise error

let export_vector a_to_bytes vector file =
  let oc = open_out_bin file in
  List.iter (fun a -> output_bytes oc (a_to_bytes a)) vector;
  close_out oc

(* TODO : do this in C *)
let import_srs_g2 d srsfile =
  let g2_size_compressed = Bls12_381.G2.size_in_bytes / 2 in
  let buf = Bytes.create g2_size_compressed in
  let read ic =
    Stdlib.really_input ic buf 0 g2_size_compressed;
    Bls12_381.G2.of_compressed_bytes_exn buf
  in
  let ic = open_in srsfile in
  try
    if in_channel_length ic < d * g2_size_compressed then
      failwith (Printf.sprintf "SRS asked (%d) too big for %s" d srsfile);
    let res = Array.init d (fun _ -> read ic) in
    close_in ic;
    res
  with e ->
    close_in ic;
    raise e

module Hash : sig
  type state

  val init : unit -> state
  val update : state -> bytes -> unit
  val finish : state -> bytes
  val hash_bytes : bytes list -> bytes
  val bytes_to_seed : bytes -> int array * bytes
end = struct
  type state = Hacl_star.EverCrypt.Hash.t

  let init () =
    Hacl_star.EverCrypt.Hash.init ~alg:Hacl_star.SharedDefs.HashDefs.BLAKE2b

  let update st msg = Hacl_star.EverCrypt.Hash.update ~st ~msg
  let finish st = Hacl_star.EverCrypt.Hash.finish ~st

  let hash_bytes bytes =
    (* select the appropriate BLAKE2b function depending on platform and
     * always produce a 32 byte digest *)
    let blake2b msg =
      let digest_size = 32 in
      let open Hacl_star in
      if AutoConfig2.(has_feature VEC256) then
        Hacl.Blake2b_256.hash msg digest_size
      else Hacl.Blake2b_32.hash msg digest_size
    in
    blake2b (Bytes.concat Bytes.empty bytes)

  (* generate a seed for Random.full_init from hash of b bytes
     Also returns the hash of the bytes*)
  let bytes_to_seed b =
    let hashed_b = hash_bytes [ b ] in
    assert (Bytes.length hashed_b = 32);
    let sys_int_size = Sys.int_size - 1 in
    let modulo = Z.pow (Z.of_int 2) sys_int_size in
    (* seed generation based on four int, computed from hashed_b sub_byte ;
       each ni is Bytes.sub hashed_b i 8 modulo 2**sys.int_size, in order to avoid
       Z.Overflow when ni is converted to int *)
    let n0_raw = Z.of_bits (Bytes.sub_string hashed_b 0 8) in
    let n0 = Z.to_int (Z.erem n0_raw modulo) in
    let n1_raw = Z.of_bits (Bytes.sub_string hashed_b 8 8) in
    let n1 = Z.to_int (Z.erem n1_raw modulo) in
    let n2_raw = Z.of_bits (Bytes.sub_string hashed_b 16 8) in
    let n2 = Z.to_int (Z.erem n2_raw modulo) in
    let n3_raw = Z.of_bits (Bytes.sub_string hashed_b 24 8) in
    let n3 = Z.to_int (Z.erem n3_raw modulo) in
    ([| n0; n1; n2; n3 |], hashed_b)
end

(* expand a transcript with the elements of a list *)
let list_expand_transcript ~to_bytes transcript list =
  let open Hash in
  let st = init () in
  update st transcript;
  List.iter (fun a -> update st (to_bytes a)) list;
  finish st

let expand_transcript : to_bytes:('a -> bytes) -> bytes -> 'a -> bytes =
 fun ~to_bytes transcript x -> list_expand_transcript ~to_bytes transcript [ x ]

(* Pad array to given size with the last element of the array *)
let pad array final_size =
  let size = Array.length array in
  Array.init final_size (fun i ->
      if i < size then array.(i) else array.(size - 1))

(* Resize array: return the array, subarray or pad it with its last element *)
let array_resize array final_size =
  let size = Array.length array in
  if size = final_size then array
  else if size > final_size then Array.sub array 0 final_size
  else pad array final_size
