(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Plonk.List
open Plonk.Utils
module Helpers = Plonk_test.Helpers

let rng6 = [ 1; 2; 3; 4; 5; 6 ]

let list_tests () =
  let left, right = split_n 2 rng6 in
  assert (equal_lists ~equal:( = ) [ 1; 2 ] left);
  assert (equal_lists ~equal:( = ) [ 3; 4; 5; 6 ] right);

  let left, right = split_in_half rng6 in
  assert (equal_lists ~equal:( = ) [ 1; 2; 3 ] left);
  assert (equal_lists ~equal:( = ) [ 4; 5; 6 ] right);

  assert (
    21 * 21 = fold_left3 (fun acc a b c -> acc + (a * b * c)) 0 rng6 rng6 rng6)

let utils_tests () = assert (91 = inner_product ~add:( + ) ~mul:( * ) rng6 rng6)

module Multicore = struct
  module Srs = Polynomial.Srs
  module Polynomial = Polynomial.Polynomial

  let pippenger_bls_copy ~start ~len aa bb =
    let aa = Array.sub aa start len in
    let bb = Array.sub bb start len in
    let pippenger_ctxt = Srs.of_array aa in
    let poly = Polynomial.of_dense bb in
    Srs.pippenger pippenger_ctxt poly

  let pippenger_bls ~start ~len aa bb = Srs.pippenger ~start ~len aa bb

  let test () =
    Plonk.Multicore.with_pool ~num_additional_domains:3 (fun () ->
        let l = 4_000_001 in
        let open Bls12_381 in
        (* always using copy to create a fresh value *)
        let a1 = Array.init l (fun _ -> G1.(copy one)) in
        (* IMPROVEME: copy is not in Ff_sig.0.6.1 *)
        let a2 = Array.init l (fun _ -> G1.Scalar.(of_string "1")) in
        let pippenger_ctxt = Srs.of_array a1 in
        let poly = Polynomial.of_dense a2 in
        let res =
          Helpers.time "one big pippengeer" (fun () ->
              pippenger_bls ~start:0 ~len:l pippenger_ctxt poly)
        in
        assert (G1.(eq res (mul one (Scalar.of_z @@ Z.of_int l))));
        let res =
          let res =
            Helpers.time "one_chunk_per_core with copy" (fun () ->
                Plonk.Multicore.map2_one_chunk_per_core pippenger_bls_copy a1 a2
                  l)
          in
          List.fold_left G1.add G1.zero res
        in
        assert (G1.(eq res (mul one (Scalar.of_z @@ Z.of_int l))));
        let res =
          let res =
            Helpers.time "one_chunk_per_core no copying" (fun () ->
                Plonk.Multicore.map2_one_chunk_per_core pippenger_bls
                  pippenger_ctxt poly l)
          in
          List.fold_left G1.add G1.zero res
        in
        assert (G1.(eq res (mul one (Scalar.of_z @@ Z.of_int l)))))
end

let tests =
  Alcotest.
    [
      test_case "List.ml" `Quick list_tests;
      test_case "Utils.ml" `Quick utils_tests;
      test_case "pippenger" `Quick Multicore.test;
    ]
