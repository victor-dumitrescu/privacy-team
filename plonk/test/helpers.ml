(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let srs_root = Option.value (Sys.getenv_opt "SRS_DIR") ~default:"."

let srsfiles =
  ( (srs_root ^ "/" ^ "srs_zcash_g1", srs_root ^ "/" ^ "srs_zcash_g2"),
    (srs_root ^ "/" ^ "srs_filecoin_g1", srs_root ^ "/" ^ "srs_filecoin_g2") )

let rec repeat n f =
  if n < 0 then ()
  else (
    f ();
    repeat (n - 1) f)

let must_fail f =
  let exception Local in
  try
    (try f () with _ -> raise Local);
    assert false
  with
  | Local -> ()
  | _ -> assert false

let string_of_words words =
  let bytes = words *. 8. in
  if bytes <= 1024. then Printf.sprintf "%3.2f B " bytes
  else
    let kilobytes = bytes /. 1024. in
    if kilobytes <= 1024. then Printf.sprintf "%3.2f KB" kilobytes
    else
      let megabytes = kilobytes /. 1024. in
      if megabytes <= 1024. then Printf.sprintf "%3.2f MB" megabytes
      else
        let gigabytes = megabytes /. 1024. in
        Printf.sprintf "%.2f GB" gigabytes

let time description f =
  Gc.full_major ();
  let st1 = Gc.stat () in
  let start = Unix.gettimeofday () in
  let res = f () in
  let stop = Unix.gettimeofday () in
  let time =
    let d = stop -. start in
    if d > 60. then Printf.sprintf "%3.2f m " (d /. 60.)
    else if d > 1. then Printf.sprintf "%3.2f s " d
    else Printf.sprintf "%3.2f ms" (d *. 1_000.)
  in
  let st2 = Gc.stat () in
  let allocations =
    st2.minor_words +. st2.major_words -. st2.promoted_words
    -. (st1.minor_words +. st1.major_words -. st1.promoted_words)
  in
  let top_heap_words =
    if st2.top_heap_words > st1.top_heap_words then
      st2.top_heap_words |> float_of_int |> string_of_words
    else "?"
  in
  Printf.printf "%-6s: Time: %8s Allocations %6s MaxHeap: %s\n%!" description
    time
    (string_of_words allocations)
    top_heap_words;
  res

module Make (Main : Plonk.Main_protocol.Main_protocol_sig) = struct
  open Plonk.Circuit

  (* generator must be n-th root of unity
     n must be in the form 2^i
     for k number of gates
     a_c, b_c, c_c, ql, qr, qo, qm, qc must be lists of length k
     x is an array of length m = 3+2(k-1)
     l between 0 and m-1, l first parameters will be taken as public inputs
     n = k+l
     valid_proof is true if the proof is expected valid, false if it must fail
     if verbose print run times when valid_proof is true
  *)
  let test_circuit ?(zero_knowledge = true) ?(valid_proof = true)
      ?(proof_exception = false) ?(lookup_exception = false) ?(verbose = false)
      ~nb_proofs circuit private_inputs =
    let time_if_verbose verbose description f =
      if verbose then time description f else f ()
    in
    let public_inputs = Array.sub private_inputs 0 circuit.public_input_size in
    let inputs = Main.{ witness = private_inputs; public = public_inputs } in

    let (pp_prover, pp_verifier), transcript =
      time_if_verbose verbose "setup" (fun () ->
          Main.setup ~zero_knowledge circuit ~srsfiles ~nb_proofs)
    in
    if valid_proof then
      let proof, _transcript =
        time_if_verbose verbose "prove" (fun () ->
            Main.prove ~zero_knowledge (pp_prover, transcript) ~inputs)
      in
      let v, _ =
        time_if_verbose verbose "verify" (fun () ->
            Main.verify (pp_verifier, transcript) ~public_inputs proof)
      in
      assert v
    else
      assert (
        try
          let proof, _ =
            Main.prove (pp_prover, transcript) ~zero_knowledge ~inputs
          in
          not (fst @@ Main.verify (pp_verifier, transcript) ~public_inputs proof)
        with
        | Main.Rest_not_null _ ->
            if proof_exception then true
            else raise (Invalid_argument "Proving error: incorrect witness")
        | Main.Entry_not_in_table _ ->
            if lookup_exception then true
            else raise (Invalid_argument "Proving error: incorrect lookup")
        | _ -> raise (Invalid_argument "Proving error: unknown error"))

  let test_circuits ?(zero_knowledge = true) ?(num_additional_domains = 0)
      ?(valid_proof = true) ?(proof_exception = false)
      ?(lookup_exception = false) ?(verbose = false) circuit_map private_inputs
      =
    let time_if_verbose verbose description f =
      if verbose then time description f else f ()
    in
    let inputs =
      let get_pi pi_size =
        List.map (fun witness ->
            let public = Array.sub witness 0 pi_size in
            Main.{ witness; public })
      in
      try
        Main.SMap.mapi
          (fun c_name witness ->
            get_pi (fst (Main.SMap.find c_name circuit_map)).public_input_size
              witness)
          private_inputs
      with _ ->
        failwith
          "Helpers.test_circuits : circuit_map must contain private_inputs' \
           keys."
    in
    let public_inputs =
      Main.SMap.map (List.map (fun i -> Main.(i.public))) inputs
    in
    let (pp_prover, pp_verifier), transcript =
      time_if_verbose verbose "setup" (fun () ->
          Main.setup_multi_circuits ~zero_knowledge ~num_additional_domains
            circuit_map ~srsfiles)
    in
    if valid_proof then
      let proof, _transcript =
        time_if_verbose verbose "prove" (fun () ->
            Main.prove_multi_circuits ~zero_knowledge ~num_additional_domains
              (pp_prover, transcript) ~inputs)
      in

      let v, _ =
        time_if_verbose verbose "verify" (fun () ->
            Main.verify_multi_circuits (pp_verifier, transcript) ~public_inputs
              proof)
      in
      assert v
    else
      assert (
        try
          let proof, _ =
            Main.prove_multi_circuits (pp_prover, transcript) ~zero_knowledge
              ~num_additional_domains ~inputs
          in
          not
            (fst
            @@ Main.verify_multi_circuits (pp_verifier, transcript)
                 ~public_inputs proof)
        with
        | Main.Rest_not_null _ ->
            if proof_exception then true
            else raise (Invalid_argument "Proving error: incorrect witness")
        | Main.Entry_not_in_table _ ->
            if lookup_exception then true
            else raise (Invalid_argument "Proving error: incorrect lookup")
        | _ -> raise (Invalid_argument "Proving error: unknown error"))
end
