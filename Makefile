coverage:
	dune test --instrument-with bisect_ppx --force
	bisect-ppx-report html
	bisect-ppx-report summary
	@echo "full report in _coverage/index.html"

are-we-fast-yet:
	dune exec nuplompiler/test/main.exe -- test Ben -v
