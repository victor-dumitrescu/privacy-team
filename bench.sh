#!/bin/bash

set -e

logname () {
    echo ${1}-$(git branch --show-current)-$(git rev-parse --short HEAD).log
}
name=$(logname "bench")
dune build plonk/test/main.exe

if [ ! -f "$1" ]
then (OCAMLRUNPARAM=v=0x400 /usr/bin/time -v ./_build/default/plonk/test/main.exe test Benc -v 2>&1 ) > $name
else name="$1"
fi

cat $name | grep 'Time'

allocated=$(($(cat $name | grep 'allocated_words' | cut -d' ' -f2) * 8 / 1024 / 1024))
max_heap=$(($(cat $name | grep 'top_heap_words' | cut -d' ' -f2) * 8 / 1024 / 1024))
max_process=$(($(cat $name | grep 'Maximum resident set size' | cut -d' ' -f6) / 1024))

printf "GC allocated %5s MB\n" $allocated
printf "GC max       %5s MB\n" $max_heap
printf "process max  %5s MB\n" $max_process
