(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tx_rollup
open Plompiler
module H = Helpers.V (LibCircuit)

let benchmark ~nb_batches ~batch_size () =
  Format.printf "nb_batches %i batch_size %i\n%!" nb_batches batch_size;
  let circ_map, private_inputs =
    Plonk_test.Helpers.time "prepare" (fun () ->
        let init_state =
          P.make_state
            [ (Z.of_int 1_000_000, [||]); (Z.of_int 1_000_000, [||]) ]
        in
        (* Batches and intermediate states *)
        let rs =
          P.generate_transactions ~nb_batches ~batch_size ~src_pos:Z.zero
            ~dst_pos:(Z.of_int Constants.max_nb_tickets)
            ~amount:
              {
                id = Constants.tez_id;
                amount =
                  Types.P.Bounded.make ~bound:Constants.Bound.max_amount Z.one;
              }
            ~fee:Z.zero init_state
        in
        let circ, private_inputs =
          let batch, s = List.hd rs in
          let c = H.inner_batch batch init_state s in
          let initial, public_input_size = LibCircuit.get_inputs c in
          let r = LibCircuit.get_cs ~optimize:true c in
          let private_inputs = Solver.solve r.solver initial in
          let circ = Plonk.Circuit.to_plonk ~public_input_size r.cs in
          let solver = Solver.solve r.solver in
          let hd = private_inputs in
          let cs, _ =
            List.fold_left
              (fun (acc, acc_s) (batch, s) ->
                let c = H.inner_batch batch acc_s s in
                let initial, _ = LibCircuit.get_inputs c in
                let private_inputs = solver initial in
                (private_inputs :: acc, s))
              ([], s) (List.tl rs)
          in
          (circ, hd :: List.rev cs)
        in
        let circ_map =
          Plonk.Main_protocol.SMap.(add "batch" (circ, nb_batches) empty)
        in
        let private_inputs =
          Plonk.Main_protocol.SMap.(add "batch" private_inputs empty)
        in
        (circ_map, private_inputs))
  in
  Gc.full_major ();
  let module H = Plonk_test.Helpers.Make (Plonk.Main_protocol) in
  H.test_circuits ~verbose:true ~num_additional_domains:(nb_batches - 1)
    circ_map private_inputs

let tests =
  [
    Alcotest.test_case "Private batch benchmark" `Quick (fun () ->
        ignore
        @@ List.map
             (fun (nb_batches, batch_size) ->
               benchmark ~nb_batches ~batch_size ())
             [ (4, 1) ]);
  ]
